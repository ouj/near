#ifndef RENDER_PHOTON_H
#define RENDER_PHOTON_H
#include "render.h"
#include <integration/integration.h>
#include "photon_kdtree.h"

struct PhotonRenderOpts : RenderOpts {
    PhotonRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    TraceDirectMode     directmode = TDM_ALL;
    bool                diffusemultibounce = true;
    int                 maxdepth = 16;
    int                 photonrays = 200000;
    int                 photonbatchsize = 2500;
    int                 gatherrays = 1;
    int                 nlookup = 16;
    float               maxlookupdist = 5.0f;
    int                 generatortype = PixelSobolSampleGenerator::TYPEID;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
};

struct PhotonRender : Render {
    const static int TYPEID = 800602;
    PhotonRender();
    PhotonRender(const PhotonRenderOpts &opts);
    virtual ~PhotonRender();

    // operations --------------------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    void                        constructPhotonMaps(bool parallel);
    vec3f                       evalRadiance(const ray3f& ray, SampleSequence& seq);
    ImageSamplingState          _imageSampling;
    PhotonRenderOpts            _opts;
    KdTree                      _indirect;
    KdTree                      _direct;
    vec3f                       _sceneCenter;
    float                       _sceneRadius;
    int                         _nPath;
    int                         _photonBatches;

};

#endif
