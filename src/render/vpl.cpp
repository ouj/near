#include "vpl.h"
#include <integration/options.h>
#include <integration/sequence.h>
#include <integration/integration.h>
#include <accelerator/accelerator.h>
#include <scene/transform.h>
#include <scene/light.h>
#include <integration/sampling.h>
#include <tbb/tbb.h>

void allocateDirectSamples(std::vector<PointVpLight> &ptLights,
                           std::vector<DirectVpLight> &dirLights,
                           std::vector<OrientedVpLight> &ortLights,
                           const std::vector<Light*> &lights,
                           int areaLightSamples, int envLightSamples) {
    int ort = 0;
    int dir = 0;
    int pt = 0;
    for (int l = 0; l < lights.size(); l++) {
        Light* light = lights[l];
        switch (light->type) {
        case AreaLight::TYPEID: ort += areaLightSamples; break;
        case PointLight::TYPEID: pt += 1;
        case DirectionalLight::TYPEID: dir += 1;
        case EnvironmentLight::TYPEID: dir += envLightSamples; break;
        default: break;
        }
    }
    ortLights.resize(ort);
    dirLights.resize(dir);
    ptLights.resize(pt);
}

void sampleDirectVpLights(std::vector<PointVpLight> &ptLights,
                          std::vector<DirectVpLight> &dirLights,
                          std::vector<OrientedVpLight> &ortLights,
                          const std::vector<Light*> lights,
                          SampleGenerator *generator,
                          int areaLightSamples, int envLightSamples) {
    allocateDirectSamples(ptLights, dirLights, ortLights, lights,
                          areaLightSamples, envLightSamples);
    int ort = 0;
    int dir = 0;
    int pt = 0;
    SampleSequence seq;
    Rng rng(0);
    int samples = 0;
    float invAreaLightSamples = 1.0f / areaLightSamples;
    float invEnvLightSamples = 1.0f / envLightSamples;
    for (int l = 0; l < lights.size(); l++) {
        Light* light = lights[l];
        switch (light->type) {
        case AreaLight::TYPEID: {
            AreaLight *area = static_cast<AreaLight*>(light);
            initSequence(seq, generator, rng, 0, 0, samples, areaLightSamples);
            for (int s = 0; s < areaLightSamples; s++) {
                vec2f ss = next2f(seq);
                ShapeSample ssl = sampleShape(area->shape,ss);
                EmissionSample es = sampleEmission(area->material, ss);
                vec3f irr = es.le * ssl.area * invAreaLightSamples;
                vec3f pos = transformPoint(light->transform, ssl.frame.o);
                vec3f norm = transformVector(light->transform, ssl.frame.z);
                ortLights[ort++] = OrientedVpLight(irr, pos, norm);
                nextSample(seq);
                samples++;
            }
        } break;
        case PointLight::TYPEID: {
            PointLight *point = static_cast<PointLight*>(light);
            vec3f intensity = point->intensity;
            vec3f position = transformPoint(light->transform, zero3f);
            ptLights[pt++] = PointVpLight(intensity, position);
            samples++;
        } break;
        case DirectionalLight::TYPEID: {
            DirectionalLight *direct = static_cast<DirectionalLight*>(light);
            vec3f radiance = direct->radiance;
            vec3f direction = transformVector(light->transform, z3f);
            dirLights[dir++] = DirectVpLight(radiance, direction);
            samples++;
        } break;
        case EnvironmentLight::TYPEID: {
            initSequence(seq, generator, rng, 0, 0, samples, envLightSamples);
            EnvironmentLight *env = static_cast<EnvironmentLight*>(light);
            for (int s = 0; s < envLightSamples; s++) {
                vec2f ss = next2f(seq);
                if(options::trace::envmapsampling && env->leTxt && env->traceAux) {
                    EnvironmentLightAux* aux = (EnvironmentLightAux*)env->traceAux;
                    float mapPdf;
                    vec2f uv = aux->dist.sample(ss, &mapPdf);
                    float sinTheta = sin(uv.y * consts<float>::pi);
                    float pdf = mapPdf / (consts<float>::twopi * consts<float>::pi * sinTheta);
                    vec3f direction = transformVector(light->transform, -latlongToDirection(uv));
                    vec3f le = (env->leTxt) ? env->le * sampleImage(env->leTxt->image, uv) : env->le;
                    vec3f radiance = le / pdf * invEnvLightSamples;
                    dirLights[dir++] = DirectVpLight(radiance, direction);
                } else {
                    vec3f w = sampleSpherical(ss);
                    vec3f direction = transformVector(light->transform, -w);
                    float pdf = sampleSphericalPdf(w);
                    vec2f uv = directionToLatlong(w);
                    vec3f le = (env->leTxt) ? env->le * sampleImage(env->leTxt->image, uv) : env->le;
                    vec3f radiance = le / pdf * invEnvLightSamples;
                    dirLights[dir++] = DirectVpLight(radiance, direction);
                }
                nextSample(seq);
                samples++;
            }
        } break;
        default: break;
        }
    }
}

struct VpLightBatch {
    int                     batchId;
    int                     batchSize;
    std::vector<OrientedVpLight> lights;
};


void sampleIndirectLightBatch(VpLightBatch &batch, Rng &rng,
                              const Accelerator *accelerator,
                              const std::vector<Light*> &lights,
                              SampleGenerator *generator,
                              const vec3f &sceneCenter, float sceneRadius,
                              int batchSize, int maxdepth,
                              int indirectsamples) {
    int batchsize = batch.batchSize;
    if (maxdepth == 0) return;

    SampleSequence seq;
    initSequence(seq, generator, rng, 0, 0, batch.batchId * batchSize, batchSize);

    float invLocalLights = 1.0f / indirectsamples;
    while (batchsize--) {
        LightSample ls = sampleLight(lights, next1f(seq));
        PhotonSample ps = samplePhoton(ls.light, next2f(seq), next2f(seq),
                                       sceneCenter, sceneRadius);

        if (ps.le.iszero() || ps.pdf == 0) continue;
        vec3f alpha = ps.le * abs(dot(ps.normal, ps.ray.d)) / (ps.pdf * ls.pdf) * invLocalLights;
        ray3f ray = ps.ray;
        if (alpha.iszero()) break;

        intersection3f photonIsect;
        int nIntersections = 0;

        while (accelerator->intersectFirst(ray, photonIsect)) {
            ++nIntersections;
            ReflectanceSample rs = sampleReflectance(photonIsect.m, photonIsect.st);
            if (!isvalid(rs)) break;
            vec3f wo = -ray.d;

            vec3f irradiance = alpha * rs.rhod * consts<float>::inv_pi;
            batch.lights.push_back(OrientedVpLight(irradiance,
                                                   photonIsect.f.o,
                                                   photonIsect.f.z));

            if (nIntersections >= maxdepth) break;
            ScatteringSample ss = sampleScattering(rs, photonIsect.f, wo,
                                                   next1f(seq), next2f(seq));

            if (ss.bsdf.iszero() || ss.pdf == 0) break;
            vec3f scale = ss.bsdf * abs(dot(ss.ray.d, photonIsect.f.z)) / ss.pdf;
            if (options::vpl::russianroulette) {
                float continueProb = min(1.0f, mean(scale));
                if (next1f(seq) > continueProb)
                    break;
                alpha *= scale / continueProb;
            } else
                alpha *= scale;
            ray = ss.ray;
        }
        nextSample(seq);
    }
}

void sampleIndirectVpLights(std::vector<OrientedVpLight> &indirectLights,
                            const Accelerator *accelerator,
                            const std::vector<Light*> &lights,
                            SampleGenerator *generator,
                            const vec3f &center, float radius,
                            int indirectsamples, int batchsize, int maxdepth,
                            bool diffusemultibounce, bool parallel) {
    int nbatch = static_cast<int>(ceil((double)indirectsamples / batchsize));
    int nray = indirectsamples;
    std::vector<VpLightBatch> batches(nbatch);
    std::vector<Rng>          rngs = initRngs(nbatch);
    for (int i = 0; i < nbatch; i++) {
        batches[i].batchSize = min(nray, batchsize);
        nray -= batchsize;
        batches[i].batchId = i;
        batches[i].lights.reserve(batchsize * maxdepth);
    }

    if (!parallel) {
        for (int b = 0; b < batches.size(); b++) {
            sampleIndirectLightBatch(batches[b], rngs[b], accelerator, lights,
                                     generator, center, radius, batchsize,
                                     maxdepth, indirectsamples);
        }
    } else {
        auto func = [&batches, &rngs, accelerator, &lights,
                     generator, &center, radius, batchsize,
                     maxdepth, indirectsamples]
        (const tbb::blocked_range<int> &r)->void {
            for (int b = r.begin(); b != r.end(); b++) {
                sampleIndirectLightBatch(batches[b], rngs[b], accelerator, lights,
                                         generator, center, radius, batchsize,
                                         maxdepth, indirectsamples);
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0, nbatch), func);
    }

    int nvpl = 0;
    for (int b = 0; b < nbatch; b++) {
        nvpl += (int)batches[b].lights.size();
    }

    // normalize indirect lighting
    indirectLights.resize(nvpl);
    OrientedVpLight *p = indirectLights.data();

    for (int b = 0; b < batches.size(); b++) {
        const VpLightBatch &batch = batches[b];
        for (unsigned int i = 0; i < batch.lights.size(); i++) {
            const OrientedVpLight &l = batch.lights[i];
            *p = OrientedVpLight(l.irradiance, l.position, l.normal);
            p++;
        }
    }
}


VpLightEval evalVpLight(const VpLight* light, const vec3f &pl, const vec3f &wo,
                        float r2min) {
    VpLightEval eval;
    switch (light->type) {
    case PointVpLight::TYPEID: {
        const PointVpLight *l = static_cast<const PointVpLight*>(light);
        eval.le = l->intensity / (pl - l->position).lengthSqr();
        eval.ray = makeraysegment3f(pl,l->position);
                               } break;
    case DirectVpLight::TYPEID: {
        const DirectVpLight *l = static_cast<const DirectVpLight*>(light);
        eval.le = l->radiance;
        eval.ray = ray3f(pl,-l->direction);
                                } break;
    case OrientedVpLight::TYPEID: {
        const OrientedVpLight *l = static_cast<const OrientedVpLight*>(light);
        vec3f wi = normalize(l->position - pl);
        float r2 = (l->position - pl).lengthSqr();
        float g = max(0.0f, dot(l->normal, -wi)) / max(r2min, r2);
        eval.le = l->irradiance * g;
        eval.ray = makeraysegment3(pl, l->position);
                                  } break;
    default: error("unknown virtual light type");
        break;
    }
    return eval;
}

