#include "erpt.h"
#include <integration/opts.h>
#include <integration/utils.h>
#include <common/pam.h>
#include "options.h"
#include <tbb/parallel_for.h>

ErptRenderOpts::ErptRenderOpts() : RenderOpts(ErptRender::TYPEID) {}

jsonObject ErptRenderOpts::print() const {
    jsonObject json;
    json["type"] = "erptrenderopts";
    json["maxdepth"] = maxdepth;
    json["general"] = printGeneralOpts(general);
    json["bootstrapsamples"] = bootstrapsamples;
    json["mcsamples"] = mcsamples;
    json["batchsamples"] = batchsamples;
    json["mutations"] = mutations;
    json["generatortype"] = printSampleGenerator(generatortype);
    json["mis"] = mis;
    json["blocksize"] = blocksize;
    json["diffusemultibounce"] = diffusemultibounce;
    return json;
}

void ErptRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("erptrenderopts");
    maxdepth = jsonGet(json, "maxdepth");
    generatortype = parseSampleGenerator(jsonGet(json, "generatortype"));
    general = parseGeneralOpts(jsonGet(json,"general"));
    mcsamples = jsonGet(json, "mcsamples");
    batchsamples = jsonGet(json, "batchsamples");
    mutations = jsonGet(json, "mutations");
    mis = jsonGet(json, "mis");
    bootstrapsamples = jsonGet(json, "bootstrapsamples");
    blocksize = jsonGet(json, "blocksize");
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
}

ErptRender::ErptRender() : Render(TYPEID) {}

ErptRender::ErptRender(const ErptRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

ErptRender::~ErptRender() { clearState(); }

bool ErptRender::initState(const Scene* scene, const Accelerator* accelerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    int width = _opts.general.width;
    int height = _opts.general.height;
    _image.resize(width, height);
    _accImage.assign(width, height, makespixel(zero3f));
    _rngs = initRngs(width, height);
    initLightSampling(scene->lights);

    _generator = initGenerator(_opts.generatortype, width, height, _opts.batchsamples);
    bootstrap();
    _passes = 0;
    return true;
}

void ErptRender::clearState() {}

static void mutateSample(Rng &rng, const ErptSample &cs, ErptSample &ps, int maxDepth,
                         int width, int height) {
    ps.cameraSample.image.x = mutate(rng, cs.cameraSample.image.x, 0, width);
    ps.cameraSample.image.y = mutate(rng, cs.cameraSample.image.y, 0, height);
    ps.cameraSample.lens = mutate2(rng, cs.cameraSample.lens);
    // Apply small step mutation to camera, lighting, and light samples
    for (int i = 0; i < maxDepth; ++i) {
        const PathSample &ces = cs.pathSamples[i];
        PathSample &pes = ps.pathSamples[i];
        pes.ar = mutate2(rng, ces.ar);
        pes.lr = mutate(rng, ces.lr);
        pes.rrSample = mutate(rng, ces.rrSample);

        const LightingSample &cls = cs.lightingSamples[i];
        LightingSample &pls = ps.lightingSamples[i];
        pls.lr = mutate(rng, cls.lr);
        pls.ar = mutate2(rng, cls.ar);
        pls.ls = mutate(rng, cls.ls);
        pls.ss = mutate2(rng, cls.ss);
    }
}

static void createSample(SampleSequence seq, ErptSample *sample, int maxdepth,
                         float i, float j) {
    sample->cameraSample.image = vec2f(i, j) + next2f(seq);
    sample->cameraSample.lens = next2f(seq);

    for (int i = 0; i < maxdepth; ++i) {
        PathSample &cps = sample->pathSamples[i];
        cps.ar = next2f(seq);
        cps.lr = next1f(seq);
        cps.rrSample = seq.rng->next1f();

        LightingSample &ls = sample->lightingSamples[i];
        //ls.lightNum = next1f(seq);
        ls.ss = next2f(seq);
        ls.ls = next1f(seq);
    }
}

static vec3f evalDirect(const ReflectanceSample& rs, const frame3f& f,
                 const vec3f& wo, const LightingSample& sample,
				 const std::vector<Light*>& lights, const Accelerator* accelerator,
                 bool mis) {
    LightSample ls = sampleLight(lights, sample.ls);

    if (!mis || isDelta(ls.light))
        return evalDirect(rs, f, wo, sample.ss, ls.light, accelerator) / ls.pdf;
    else {
        return evalDirectMis(rs, f, wo, sample.ss, sample.lr, sample.ar, ls.light, accelerator) / ls.pdf;
    }
}

static vec3f evalRadiance(const ray3f& ray, int depth, const ErptSample &sample,
                          const std::vector<Light*> &lights, const Accelerator *accelerator,
                          const ErptRenderOpts& opts) {
    // trace ray
    vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, depth == 0, true, lights, accelerator, f, wo, le, rs)) return le;
    if(!isvalid(rs)) return le;

    // convert reflectance to diffuse if needed
    ReflectanceSample brs = rs;
    if(opts.diffusemultibounce && depth > 0) brs = reflectanceToLambert(brs);

    // direct
    vec3f ld = evalDirect(brs, f, wo, sample.lightingSamples[depth],
                          lights, accelerator, opts.mis);

    // indirect
    vec3f li = zero3f;
    if(depth < opts.maxdepth-1) {
        ScatteringSample ss = sampleScattering(rs,f,wo,
                                               sample.pathSamples[depth].lr,
                                               sample.pathSamples[depth].ar);
        vec3f lr;
        if(!ss.bsdf.iszero()) {
            vec3f scale = ss.bsdf * dot(ss.ray.d,f.z) / ss.pdf;
            if (options::erpt::russianroulette) {
                float continueProb = min(1.0f, mean(scale));
                if (sample.pathSamples[depth].rrSample > continueProb) {
                    lr = zero3f;
                } else {
                    lr = evalRadiance(ss.ray,depth+1,sample,lights,accelerator,opts);
                    lr /= continueProb;
                }
            } else {
                lr = evalRadiance(ss.ray,depth+1,sample,lights,accelerator,opts);
            }
            li = scale * lr;
        }
    }
    // done
    return le+ld+li;
}

static vec3f evalRadiance(const ErptSample &erptSample, const Scene *scene,
                          const Accelerator *accelerator, const ErptRenderOpts& opts) {
    vec2f is = vec2f(erptSample.cameraSample.image.x/opts.general.width,
                     erptSample.cameraSample.image.y/opts.general.height);
    ray3f r = sampleCamera(scene->cameras[0], is, erptSample.cameraSample.lens);
    return evalRadiance(r, 0, erptSample, scene->lights, accelerator, opts);
}

void ErptRender::bootstrap() {
    _npixels = _opts.general.width * _opts.general.height;
    Rng rng;
    RandomSampleGenerator generator;
    SampleSequence seq;
    initSequence(seq, &generator, rng, 0, 0, 0, 0);
    float energy = 0.0f;
    ErptSample sample(_opts.maxdepth);
    for (int i = 0; i < _opts.bootstrapsamples; i++) {
        float x = lerp(0u, _opts.general.width, rng.next1f());
        float y = lerp(0u, _opts.general.height, rng.next1f());
        createSample(seq, &sample, _opts.maxdepth, x, y);
        vec3f L = evalRadiance(sample, scene, accelerator, _opts);
        float I = L.luminance();
        energy += I;
    }
    _ed = energy / (_opts.bootstrapsamples * _opts.mutations);
    message_va("bootstraps samples %d", _opts.bootstrapsamples);
    message_va("deposit energy Ed = %f", _ed);
}

void ErptRender::equalDepositionFlow(Rng &rng, const ErptSample &x, const vec3f &l) {
    ErptSample samples[2];
    vec3f L[2];
    float I[2];
    float e = l.luminance();
    int nc = rng.next1f() + e / (_opts.mutations * _ed);
    for (int i = 0; i < nc; i++) {
        int proposed = 1;
        int current = 0;
        samples[current] = x;
        L[current] = l;
        I[current] = e;
        for (int j = 0; j < _opts.mutations; j++) {
            mutateSample(rng, samples[current], samples[proposed], _opts.maxdepth,
                         _opts.general.width, _opts.general.height);
            L[proposed] = evalRadiance(samples[proposed], scene, accelerator, _opts);
            I[proposed] = L[proposed].luminance();
            float q = min(1.0f, I[proposed] / I[current]);
            if (rng.next1f() < q) {
                current ^= 1;
                proposed ^= 1;
            }
            int px = samples[current].cameraSample.image.x;
            int py = samples[current].cameraSample.image.y;
            vec3f contrib =  L[current] / I[current] * _ed;
            _accImage.at(px, py) += contrib;
        }
    }
}

void ErptRender::samplePixel(int i, int j) {
    SampleSequence seq;
    initSequence(seq, _generator, _rngs.at(i, j), i, j, _passes, _opts.batchsamples);
    for(int s = 0; s < _opts.batchsamples; s ++) {
        ErptSample erptSample(_opts.maxdepth);
        createSample(seq, &erptSample, _opts.maxdepth, i, j);
        vec3f l = evalRadiance(erptSample, scene, accelerator, _opts);
        if (!l.iszero()) {
            equalDepositionFlow(*seq.rng, erptSample, l);
        }
        nextSample(seq);
    }
}

bool ErptRender::tracePass() {
    const int width = _opts.general.width;
    const int height = _opts.general.height;
    if (_opts.general.parallel) {
        auto func = [this] (const tbb::blocked_range2d<int>& r) {
            for(int j = r.cols().begin(); j < r.cols().end(); j++) {
                for(int i = r.rows().begin(); i < r.rows().end(); i++) {
                    samplePixel(i, j);
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range2d<int>(0, width, _opts.blocksize,
                                                    0, height, _opts.blocksize),
                                                    func,
                                                    tbb::simple_partitioner());
    } else {
        for(int j = 0; j < _opts.general.height; j ++) {
            for(int i = 0; i < _opts.general.width; i ++) {
                samplePixel(i, j);
            }
        }
    }

    float weight = _passes + 1;
    for(int j = 0; j < _opts.general.height; j ++) {
        for(int i = 0; i < _opts.general.width; i ++) {
            _image.at(i, j) = _accImage.at(i, j) / weight;
        }
    }
    _passes++;
    return false;
}

Image<vec3f> ErptRender::traceImage() {
    for (int s = 0; s < _opts.mcsamples; s++) {
        tracePass();
    }
    return _image;
}

vec2i ErptRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* ErptRender::getImage() const {
    return &(_image);
}
