#ifndef RENDER_H
#define RENDER_H
#include <common/vec.h>
#include <common/json.h>
#include <common/array.h>

struct RenderOpts {
    int type;
    RenderOpts(int t) : type(t) {}
    virtual ~RenderOpts() {}
    virtual jsonObject print() const = 0;
    virtual void parse(const jsonObject &opts) = 0;
};

struct Accelerator;
struct Scene;
class Render {
public:
    Render(int t) : type(t) {}
    virtual ~Render() {};
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual Image<vec3f>        traceImage() = 0;
    virtual bool                tracePass() = 0;
    virtual void                clearState() = 0;
    virtual vec2i               getImageSize() const = 0;
    virtual const Image<vec3f>* getImage() const = 0;
protected:
    int                         type;
    const Scene                 *scene;
    const Accelerator           *accelerator;
};

#endif //RENDER_H
