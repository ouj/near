#include "photon.h"
#include "photon_kdtree.h"
#include "options.h"
#include <integration/options.h>
#include <integration/opts.h>
#include <integration/utils.h>
#include <accelerator/accelerator.h>

PhotonRenderOpts::PhotonRenderOpts() : RenderOpts(PhotonRender::TYPEID) {}

jsonObject PhotonRenderOpts::print() const {
    jsonObject json;
    json["type"] = "photonrenderopts";
    json["photonrays"] = photonrays;
    json["photonbatchsize"] = photonbatchsize;
    json["maxdepth"] = maxdepth;
    json["directmode"] = printDirectMode(directmode);
    json["gatherrays"] = gatherrays;
    json["nlookup"] = nlookup;
    json["maxlookupdist"] = maxlookupdist;
    json["diffusemultibounce"] = diffusemultibounce;
    json["generatortype"] = printSampleGenerator(generatortype);
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    return json;
}

void PhotonRenderOpts::parse(const jsonObject &json) {
    error_if_not(jsonGet(json, "type").as_string() == "photonrenderopts", "incorrect type");
    photonrays = jsonGet(json, "photonrays");
    photonbatchsize = jsonGet(json, "photonbatchsize");
    maxdepth = jsonGet(json, "maxdepth");
    directmode = parseDirectMode(jsonGet(json, "directmode"));
    gatherrays = jsonGet(json, "gatherrays");
    nlookup = jsonGet(json, "nlookup");
    maxlookupdist = jsonGet(json, "maxlookupdist");
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
    generatortype = parseSampleGenerator(jsonGet(json, "generatortype"));
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json, "general"));
}

PhotonRender::PhotonRender() : Render(TYPEID) {}

PhotonRender::PhotonRender(const PhotonRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

PhotonRender::~PhotonRender() { clearState(); }

bool PhotonRender::initState(const Scene* scene, const Accelerator *accelerator) {
    warning_if_not(_opts.gatherrays < 4,
                   "use a small number of indirectsamples samples!");
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    range3f sbbox = accelerator->computeWorldBound();
    _sceneCenter = sbbox.center();
    _sceneRadius = sbbox.diagonal() / 2;
    _photonBatches = 0;
    constructPhotonMaps(_opts.general.parallel);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    return true;
}

void PhotonRender::clearState() {}

struct PhotonBatch {
    int                 batchId;
    int                 batchSize;
    std::vector<Photon> directPhoton;
    std::vector<Photon> indirectPhoton;
};

void samplePhotonBatch(PhotonBatch &batch, Rng& rng,
                       const Accelerator *accelerator, SampleGenerator *generator,
                       const std::vector<Light*> &lights, const vec3f &sceneCenter,
                       float sceneRadius, const PhotonRenderOpts& opts) {
    int batchsize = batch.batchSize;
    int nshot = 0;
    int maxphotondepth = opts.maxdepth - 1;
    if (maxphotondepth == 0) return;

    SampleSequence seq;
    initSequence(seq, generator, rng, 0, 0, batch.batchId * opts.photonbatchsize, opts.photonbatchsize);
    while(batchsize--) {
        ++nshot;
        LightSample ls = sampleLight(lights, next1f(seq));
        PhotonSample ps = samplePhoton(ls.light, next2f(seq), next2f(seq), sceneCenter, sceneRadius);

        if (ps.le.iszero() || ps.pdf == 0) continue;
        ray3f ray = ps.ray;
        vec3f alpha = ps.le * abs(dot(ps.normal, ps.ray.d)) / (ps.pdf * ls.pdf);
        if (alpha.iszero()) continue;

        intersection3f photonIsect;
        int nIntersections = 0;

        while (accelerator->intersectFirst(ray, photonIsect)) {
            ++nIntersections;
            ReflectanceSample rs = sampleReflectance(photonIsect.m, photonIsect.st);
            if (!isvalid(rs)) break;

            Photon photon(photonIsect.f.o, photonIsect.f.z, alpha, -ray.d);
            if (nIntersections == 1) {
                batch.directPhoton.push_back(photon);
            }
            if (nIntersections > 1) {
                batch.indirectPhoton.push_back(photon);
            }


            if (opts.diffusemultibounce) {
                rs = reflectanceToLambert(rs);
            }

            if (nIntersections >= maxphotondepth) break;
            ScatteringSample ss = sampleScattering(rs, photonIsect.f, -ray.d, next1f(seq), next2f(seq));

            if (ss.bsdf.iszero() || ss.pdf == 0) break;
            vec3f scale = ss.bsdf * abs(dot(ss.ray.d,photonIsect.f.z)) / ss.pdf;
            if (options::photon::russianroulette) {
                float continueProb = min(1.0f, mean(scale));
                if (next1f(seq) > continueProb)
                    break;
                alpha *= scale / continueProb;
            } else
                alpha *= scale;
            ray = ss.ray;
        }
        nextSample(seq);
    }
}


inline float _kernel(const Photon *photon, const vec3f &p, float maxDist2) {
        float s = (1.f - distanceSqr(photon->position, p) / maxDist2);
        return 3.f * consts<float>::inv_pi * s * s;
}

void PhotonRender::constructPhotonMaps(bool parallel) {
    int nbatch = static_cast<int>(ceil((double)_opts.photonrays / _opts.photonbatchsize));
    int nray = _opts.photonrays;
    int batchsize = _opts.photonbatchsize;
    std::vector<PhotonBatch> batches(nbatch);
    std::vector<Rng> rngs = initRngs(nbatch);

    for (int i = 0; i < nbatch; i++) {
        if (nray > batchsize) {
            batches[i].batchSize = batchsize;
            nray -= batchsize;
        } else {
            batches[i].batchSize = nray;
        }
        batches[i].batchId = _photonBatches + i;
        batches[i].directPhoton.reserve(_opts.photonbatchsize);
        batches[i].indirectPhoton.reserve(_opts.photonbatchsize);
    }
    _photonBatches += nbatch;

    warning_if(_opts.generatortype == ParametricFastSobolSampleGenerator::TYPEID,
               "parametric sequence is not supported in generating vpls");
    SampleGenerator *generator = initGenerator(_opts.generatortype, 1, 1, 0);

    // shoot photons
    if (parallel) {
        tbb::parallel_for(tbb::blocked_range<int>(0, nbatch),
                          [&,this](const tbb::blocked_range<int> &r) {
                              for (int i = r.begin(); i != r.end(); i++) {
                                  samplePhotonBatch(batches[i], rngs[i], accelerator, generator, scene->lights,
                                                    _sceneCenter, _sceneRadius, _opts);
                              }
                          });
    } else {
        for (int b = 0; b < batches.size(); b++)
            samplePhotonBatch(batches[b], rngs[b], accelerator, generator,
                              scene->lights, _sceneCenter, _sceneRadius, _opts);
    }
    _nPath = _opts.photonbatchsize * nbatch;

    int ndirect = 0;
    int nindirect = 0;
    int nradiance = 0;
    for (int b = 0; b < batches.size(); b++) {
        const PhotonBatch &batch = batches[b];
        ndirect += (int)batch.directPhoton.size();
        nindirect += (int)batch.indirectPhoton.size();
    }

    std::vector<Photon*> directPhotons(ndirect);
    std::vector<Photon*> indirectPhotons(nindirect);
    std::vector<ReflectanceSample*> reflectanceSamples(nradiance);
    // merge photons
    int d = 0, i = 0;
    for (int b = 0; b < batches.size(); b++) {
        PhotonBatch &batch = batches[b];
        for (unsigned int k = 0; k < batch.directPhoton.size(); k++) directPhotons[d++] = &(batch.directPhoton[k]);
        for (unsigned int k = 0; k < batch.indirectPhoton.size(); k++) indirectPhotons[i++] = &(batch.indirectPhoton[k]);
    }

    // build kbtree
    if (parallel) {
        _indirect = parallelBuildKdTree(indirectPhotons.data(), indirectPhotons.size());
        _direct = parallelBuildKdTree(directPhotons.data(), directPhotons.size());
    } else {
        _indirect = buildKdTree(indirectPhotons.data(), indirectPhotons.size());
        _direct = buildKdTree(directPhotons.data(), directPhotons.size());
    }

    delete generator;
}

vec3f PhotonRender::evalRadiance(const ray3f& ray, SampleSequence& seq) {
        // trace ray
        vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
        if(!traceRay(ray, true, true, scene->lights, accelerator, f, wo, le, rs)) return le;
        if(!isvalid(rs)) return le;

        // convert reflectance to diffuse if needed
        ReflectanceSample brs = rs;

        // direct
        vec3f ld = evalDirect(brs, f, wo, seq, _opts.directmode, scene->lights, accelerator);

        // indirect
        vec3f li = zero3f;
        ClosePhoton lookupBuf[options::photon::photo_lookup_buf_size];
        for (int g = 0; g < _opts.gatherrays; g++) {
            ScatteringSample ss = sampleScattering(rs,f,wo,next1f(seq),next2f(seq));
            if(ss.bsdf.iszero() || ss.pdf == 0) continue;

            intersection3f gatherisect;
            vec3f lr = zero3f;
            if (accelerator->intersectFirst(ss.ray, gatherisect)) {
                PhotonProcess<Photon> process(_opts.nlookup, lookupBuf);
                float maxdist2 = min(gatherisect.t, _opts.maxlookupdist);
                lookup(_indirect, gatherisect.f.o, process, maxdist2);
                lookup(_direct, gatherisect.f.o, process, maxdist2);
                ClosePhoton *photons = process.photons;
                int nfound = process.nfound;
                ReflectanceSample grs = sampleReflectance(gatherisect.m, gatherisect.st);
                if(isvalid(grs)) {
                    grs = _opts.diffusemultibounce ? reflectanceToLambert(grs) : grs;
                    if (grs.type == LambertMaterial::TYPEID) {
                        for(int i = 0; i < nfound; i++) {
                            const Photon *p = photons[i].photon;
                            float k = (options::photon::const_kernel) ? consts<float>::inv_pi : _kernel(p, gatherisect.f.o, maxdist2);
                            if(dot(p->wi, gatherisect.f.z) > 0 && dot(p->normal, gatherisect.f.z) > 0)
                                lr += (p->alpha * (k / (_nPath * maxdist2)));
                        }
                        vec3f brdf = grs.rhod * consts<float>::inv_pi; //fast path for lambert.
                        lr *= brdf;
                    } else {
                        for(int i = 0; i < nfound; i++) {
                            const Photon *p = photons[i].photon;

                            float k = (options::photon::const_kernel) ? consts<float>::inv_pi : _kernel(p, gatherisect.f.o, maxdist2);
                            if(dot(p->wi, gatherisect.f.z) > 0 && dot(p->normal, gatherisect.f.z) > 0) {
                                vec3f brdf = evalReflectance(grs, gatherisect.f, p->wi, -ss.ray.d);
                                lr += (p->alpha * brdf * (k / (_nPath * maxdist2)));
                            }
                        }
                    }
                }
            }
            float proj = max(dot(ss.ray.d,f.z), 0.0f);
            li += lr * ss.bsdf * proj / (ss.pdf * _opts.gatherrays);
        }
        // done
        return le + ld + li;
}

bool PhotonRender::tracePass() {
    auto func = [this](const ray3f& ray, SampleSequence& seq) {
        return evalRadiance(ray, seq);
    };
    sampleImage(scene, func, _imageSampling, _opts.imageSampling, _opts.general);
    return false;
}

Image<vec3f> PhotonRender::traceImage() {
    renderImageFunc<PhotonRender>(this, _opts.imageSampling);
    return _imageSampling.image;
}

vec2i PhotonRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* PhotonRender::getImage() const {
    return &(_imageSampling.image);
}
