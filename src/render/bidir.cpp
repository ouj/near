#include "bidir.h"
#include <integration/opts.h>
#include <integration/utils.h>

BidirRenderOpts::BidirRenderOpts() : RenderOpts(BidirRender::TYPEID) {}

jsonObject BidirRenderOpts::print() const {
    jsonObject json;
    json["type"] = "bidirrenderopts";
    json["directmode"] = printDirectMode(directmode);
    json["bounceddirectmode"] = printDirectMode(bounceddirectmode);
    json["diffusemultibounce"] = diffusemultibounce;
    json["maxdepth"] = maxdepth;
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    return json;
}

void BidirRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("bidirrenderopts");
    directmode = parseDirectMode(jsonGet(json, "directmode"));
    bounceddirectmode = parseDirectMode(jsonGet(json, "bounceddirectmode"));
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
    maxdepth = jsonGet(json, "maxdepth");
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json,"general"));
}

BidirRender::BidirRender() : Render(TYPEID) {}

BidirRender::BidirRender(const BidirRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

BidirRender::~BidirRender() { clearState(); }

bool BidirRender::initState(const Scene* scene, const Accelerator *accelerator) {
    if(!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    range3f sbbox = accelerator->computeWorldBound();
    _sceneCenter = sbbox.center();
    _sceneRadius = sbbox.diagonal() / 2;
    return true;
}

void BidirRender::clearState() {}

vec3f BidirRender::LPath(SampleSequence &seq,
                         const PathBuffer &path,
                         int pLength, const ray3f &escRay, const vec3f &escAlpha) {
    vec3f l = zero3f;
    vec3f ld = zero3f;
    for (int i = 0; i < pLength; i++) {
        const PathVertex &vc = path[i];
        if (i == 0 && isvalid(vc.es)) // add emitted light from vertex if needed
            l += vc.alpha * evalEmission(vc.es, vc.f, vc.wPrev);

        if (isvalid(vc.rs))
            ld = vc.alpha * evalDirect(vc.rs, vc.f, vc.wPrev, seq, _opts.directmode,
                                       scene->lights, accelerator);
        l += ld;
    }
    if (!escAlpha.iszero())
        l += escAlpha * evalEnvironment(escRay, scene->lights);
    return l;
}

void uniformWeights(float pathweight[options::bidir::maxpathlength+1][options::bidir::maxpathlength+1],
                    int cameraPathLength, int lightPathLength) {
    float sum[options::bidir::maxpathlength*2+2];
    for(int i = 0; i < cameraPathLength+lightPathLength+2; i ++) sum[i] = 0;
    for(int i = 0; i < cameraPathLength; i ++) {
        pathweight[i+1][0] = 1;
        sum[i+1] += pathweight[i+1][0];
        for(int j = 0; j < lightPathLength; j ++) {
            pathweight[i+1][j+1] = 1;
            sum[i+1+j+1] += pathweight[i+1][j+1];
        }
    }
    for(int i = 0; i < cameraPathLength; i ++) {
        pathweight[i+1][0] /= sum[i+1];
        for(int j = 0; j < lightPathLength; j ++) {
            pathweight[i+1][j+1] /= sum[i+1+j+1];
        }
    }
}

void misWeights(float pathweight[options::bidir::maxpathlength+1][options::bidir::maxpathlength+1],
                const PathBuffer &cameraPath, int cameraPathLength,
                const PathBuffer &lightPath, int lightPathLength) {
    float sum[options::bidir::maxpathlength*2+2];
    for(int i = 0; i < cameraPathLength+lightPathLength+2; i ++) sum[i] = 0;
    for(int i = 0; i < cameraPathLength; i ++) {
        pathweight[i+1][0] = cameraPath[i].pdf * cameraPath[i].pdf;
        sum[i+1] += pathweight[i+1][0];
        for(int j = 0; j < lightPathLength; j ++) {
            pathweight[i+1][j+1] = cameraPath[i].pdf * cameraPath[i].pdf *
            lightPath[i].pdf * lightPath[i].pdf;
            sum[i+1+j+1] += pathweight[i+1][j+1];
        }
    }
    for(int i = 0; i < cameraPathLength; i ++) {
        pathweight[i+1][0] /= sum[i+1];
        error_if(isnan(pathweight[i+1][0]), "nan");
        for(int j = 0; j < lightPathLength; j ++) {
            pathweight[i+1][j+1] /= sum[i+1+j+1];
            error_if(isnan(pathweight[i+1][j+1]), "nan");
        }
    }
}

vec3f BidirRender::LBidir(SampleSequence &seq,
                          const PathBuffer &camPath, int camLength,
                          const PathBuffer &ltPath, int ltLength,
                          const ray3f &escRay, const vec3f &escAlpha) {
    vec3f l = zero3f;

    //float pathweight[options::bidir::maxpathlength+1][options::bidir::maxpathlength+1];
    //uniformWeights(pathweight, camLength, ltLength);
    //misWeights(pathweight, camPath, camLength, ltPath, ltLength);

    for (int i = 0; i < camLength; i++) {
        const PathVertex &vc = camPath[i];
        if (i == 0 && isvalid(vc.es))
            l += vc.alpha * evalEmission(vc.es, vc.f, vc.wPrev);

        if (isvalid(vc.rs)) {
            float weight = 1.0f / (i + 1);
            //float weight = pathweight[i+1][0];
            vec3f ld = vc.alpha * evalDirect(vc.rs, vc.f, vc.wPrev, seq, _opts.directmode,
                                             scene->lights, accelerator);
            l += ld * weight;
        }
        // Loop over light path vertices and connect to camera vertex
        for (int j = 0; j < ltLength; ++j) {
            const PathVertex &vl = ltPath[j];
            const vec3f &pc = vc.f.o;
            const vec3f &nc = vc.f.z;
            const vec3f &pl = vl.f.o;
            const vec3f &nl = vl.f.z;
            vec3f w = normalize(pl - pc);
            if (!isvalid(vc.rs) || !isvalid(vl.rs)) continue;
            vec3f fc = evalReflectance(vc.rs, vc.f, vc.wPrev, w);
            vec3f fl = evalReflectance(vl.rs, vl.f, -w, vl.wPrev);
            if (fc.iszero() || fl.iszero()) continue;
            ray3f r = makeraysegment3(pc, pl);
            if (!accelerator->intersectAny(r)) {
                // for path length i+j+2
                float weight = 1.0f / (i + j + 2);
                // float weight = pathweight[i+1][j+1];
                float G = abs(dot(nc, w)) * abs(dot(nl, -w)) / (pl-pc).lengthSqr();
                l += (vc.alpha * fc * G * fl * vl.alpha) * weight;
            }
        }
    }
    if (!escAlpha.iszero())
        l += escAlpha * evalEnvironment(escRay, scene->lights);
    return l;
}

vec3f BidirRender::evalRadiance(const ray3f& ray, SampleSequence& seq) {
    PathBuffer lightPath, cameraPath;
    vec3f escapedAlpha; ray3f escapedRay;
    constexpr int mlen = options::bidir::maxpathlength;
    constexpr bool rr = options::bidir::russianroulette;

    int cameraLength = generatePath<mlen, rr>(cameraPath, accelerator,
                                              seq, ray, one3f, 1.0f,
                                              _opts.maxdepth, &escapedRay, &escapedAlpha);

    if (!options::bidir::bidirectional) {
        return LPath(seq, cameraPath, cameraLength, escapedRay, escapedAlpha);
    } else {
        LightSample ls = sampleLight(scene->lights, next1f(seq));
        PhotonSample ps = samplePhoton(ls.light, next2f(seq), next2f(seq),
                                       _sceneCenter, _sceneRadius);
        if(ps.le.iszero() || ps.pdf == 0) {
            return LPath(seq, cameraPath, cameraLength, escapedRay, escapedAlpha);
        } else {
            vec3f alpha = ps.le * abs(dot(ps.normal, ps.ray.d)) / (ps.pdf * ls.pdf);
            int lightLength = generatePath<mlen, rr>(lightPath, accelerator,
                                                     seq, ps.ray, alpha,
                                                     (isDelta(ls.light)?1:ps.pdf) * ls.pdf,
                                                     _opts.maxdepth, 0, 0);

            return LBidir(seq, cameraPath, cameraLength,
                          lightPath, lightLength, escapedRay, escapedAlpha);
        }
    }
    return zero3f;
}

bool BidirRender::tracePass() {
    auto func = [this](const ray3f& ray, SampleSequence& seq) {
        return evalRadiance(ray, seq);
    };
    sampleImage(scene, func, _imageSampling, _opts.imageSampling, _opts.general);
    return false;
}

Image<vec3f> BidirRender::traceImage() {
    renderImageFunc<BidirRender>(this, _opts.imageSampling);
    return _imageSampling.image;
}

vec2i BidirRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* BidirRender::getImage() const {
    return &(_imageSampling.image);
}
