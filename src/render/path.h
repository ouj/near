#ifndef RENDER_PATH_H
#define RENDER_PATH_H

#include "render.h"
#include <integration/integration.h>

struct PathRenderOpts : RenderOpts {
    PathRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    TraceDirectMode     directmode = TDM_ALL_MIS;
    int                 maxdepth = 3;
    bool                diffusemultibounce = true;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
};

struct PathRender : Render {
    const static int TYPEID = 800601;
    PathRender();
    PathRender(const PathRenderOpts &opts);
    virtual ~PathRender();
    // operations ------------------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    PathRenderOpts              _opts;
    ImageSamplingState          _imageSampling;
};

#endif
