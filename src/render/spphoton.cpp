#include "photon.h"
#include "options.h"
#include "spphoton.h"
#include "photon_kdtree.h"
#include <integration/opts.h>
#include <integration/utils.h>
#include <integration/options.h>
#include <accelerator/accelerator.h>
#include <common/geom.h>

SpPhotonRenderOpts::SpPhotonRenderOpts() : RenderOpts(SpPhotonRender::TYPEID) {}

jsonObject SpPhotonRenderOpts::print() const {
    jsonObject json;
    json["type"] = "spphotonrenderopts";
    json["photonrays"] = photonrays;
    json["photonbatchsize"] = photonbatchsize;
    json["maxdepth"] = maxdepth;
    json["r1"] = r1;
    json["generatortype"] = printSampleGenerator(generatortype);
    json["alpha"] = alpha;
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    return json;
}

void SpPhotonRenderOpts::parse(const jsonObject &json) {
    error_if_not(jsonGet(json, "type").as_string() == "spphotonrenderopts", "incorrect type");
    photonrays = jsonGet(json, "photonrays").as_int();
    photonbatchsize = jsonGet(json, "photonbatchsize").as_int();
    maxdepth = jsonGet(json, "maxdepth").as_int();
    r1 = jsonGet(json, "r1").as_float();
    generatortype = parseSampleGenerator(jsonGet(json, "generatortype").as_string());
    alpha = jsonGet(json, "alpha").as_float();
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json, "general"));
}

struct SpPhotonBatch {
    int             batchId;
    int             batchSize;
    std::vector<Photon>  photons;
};

SpPhotonRender::SpPhotonRender() : Render(TYPEID) {}

SpPhotonRender::SpPhotonRender(const SpPhotonRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

SpPhotonRender::~SpPhotonRender() { clearState(); };

bool SpPhotonRender::initState(const Scene* scene, const Accelerator *accelerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    _photonbatches = 0;
    _rr = pow2(_opts.r1);
    range3f sbbox = accelerator->computeWorldBound();
    _sceneCenter = sbbox.center();
    _sceneRadius = sbbox.diagonal() / 2;
    return true;
}

void SpPhotonRender::clearState() {}

void samplePhotonBatch(SpPhotonBatch &batch, Rng &rng, const Accelerator *accelerator,
                       SampleGenerator *generator, const std::vector<Light*> &lights,
                       const vec3f &sceneCenter, float sceneRadius,
                       const SpPhotonRenderOpts& opts) {
    int batchsize = batch.batchSize;
    int nshot = 0;
    int maxphotondepth = opts.maxdepth;
    if (maxphotondepth == 0) return;

    SampleSequence seq;
    initSequence(seq, generator, rng, 0, 0, batch.batchId * opts.photonbatchsize, opts.photonbatchsize);
    while(batchsize--) {
        ++nshot;
        LightSample ls = sampleLight(lights, next1f(seq));
        PhotonSample ps = samplePhoton(ls.light, next2f(seq), next2f(seq), sceneCenter, sceneRadius);

        if (ps.le.iszero() || ps.pdf == 0) continue;
        ray3f ray = ps.ray;
        vec3f alpha = ps.le * abs(dot(ps.normal, ps.ray.d)) / (ps.pdf * ls.pdf);
        if (alpha.iszero()) continue;

        intersection3f photonIsect;
        int nIntersections = 0;

        while (accelerator->intersectFirst(ray, photonIsect)) {
            ++nIntersections;
            ReflectanceSample rs = sampleReflectance(photonIsect.m, photonIsect.st);
            if (!isvalid(rs)) break;

            Photon photon(photonIsect.f.o, photonIsect.f.z, alpha, -ray.d);
            batch.photons.push_back(photon);

            if (nIntersections >= maxphotondepth) break;

            ScatteringSample ss = sampleScattering(rs, photonIsect.f, -ray.d, next1f(seq), next2f(seq));

            if (ss.bsdf.iszero() || ss.pdf == 0) break;
            vec3f scale = ss.bsdf * abs(dot(ss.ray.d, photonIsect.f.z)) / ss.pdf;
            if (options::spphoton::russianroulette) {
                float continueProb = min(1.0f, mean(scale));
                if (next1f(seq) > continueProb)
                    break;
                alpha *= scale / continueProb;
            } else
                alpha *= scale;
            ray = ss.ray;
        }
        nextSample(seq);
    }
}

void SpPhotonRender::buildPhotonmap(bool parallel) {
    warning_if(_opts.generatortype == ParametricFastSobolSampleGenerator::TYPEID,
               "parametric sequence is not supported in generating vpls");
    SampleGenerator *generator = initGenerator(_opts.generatortype, 1, 1, 0);
    int nbatch = static_cast<int>(ceil((double)_opts.photonrays / _opts.photonbatchsize));
    std::vector<SpPhotonBatch> batches(nbatch);
    std::vector<Rng> rngs = initRngs(nbatch);

    int nray = _opts.photonrays;
    int batchsize = _opts.photonbatchsize;
    for (int i = 0; i < nbatch; i++) {
        if (nray > batchsize) {
            batches[i].batchSize = batchsize;
            nray -= batchsize;
        } else {
            batches[i].batchSize = nray;
        }
        batches[i].batchId = _photonbatches + i;
        batches[i].photons.reserve(_opts.photonbatchsize);
        batches[i].photons.clear();
    }
    _photonbatches += nbatch;

    // shoot photons
    if (parallel) {
        auto func = [&batches, &rngs, generator, this]
        (const tbb::blocked_range<int> &r) {
            for (int b = r.begin(); b != r.end(); b++) {
                samplePhotonBatch(batches[b], rngs[b], accelerator, generator,
                                  scene->lights, _sceneCenter, _sceneRadius, _opts);
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0, batches.size()), func);
    } else {
        for (int b = 0; b < batches.size(); b++)
            samplePhotonBatch(batches[b], rngs[b], accelerator, generator,
                              scene->lights, _sceneCenter, _sceneRadius, _opts);
    }

    int nphoton = 0;
    for (int b = 0; b < batches.size(); b++) {
        const SpPhotonBatch &batch = batches[b];
        nphoton += (int)batch.photons.size();
    }

    std::vector<Photon*> photons(nphoton);

    // merge photons
    int d = 0;
    for (int b = 0; b < batches.size(); b++) {
        SpPhotonBatch &batch = batches[b];
        for (int k = 0; k < batch.photons.size(); k++) photons[d++] = &(batch.photons[k]);
    }

    // build kbtree
    if (photons.size() > 0) {
        if (parallel)
            _photonmap = parallelBuildKdTree(photons.data(), photons.size());
        else
            _photonmap = buildKdTree(photons.data(), photons.size());
    }

    delete generator;

}

bool SpPhotonRender::tracePass() {
    buildPhotonmap(_opts.general.parallel);
    auto func = [this](const ray3f& ray, SampleSequence& seq) {
        return evalRadiance(ray, seq, 0);
    };
    sampleImage(scene, func, _imageSampling, _opts.imageSampling, _opts.general);
    _passes++;
    _rr = _rr * (_passes + _opts.alpha) / (_passes + 1);
    return false;
}

Image<vec3f> SpPhotonRender::traceImage() {
    renderImageFunc<SpPhotonRender>(this, _opts.imageSampling);
    return _imageSampling.image;
}

vec3f SpPhotonRender::evalRadiance(const ray3f& ray,
                                   SampleSequence& seq, int depth) {
    vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, true, true, scene->lights, accelerator, f, wo, le, rs)
       || !isvalid(rs)) {
        return le;
    }

//    if (!isLambert(rs)) {
//        ScatteringSample ss = sampleScattering(rs,f,wo,next1f(seq),next2f(seq));
//        vec3f li = zero3f;
//        if(!ss.bsdf.iszero()) {
//            vec3f lr;
//            vec3f scale = ss.bsdf * dot(ss.ray.d,f.z) / ss.pdf;
//            if (options::path::russianroulette) {
//                float continueProb = min(1.0f, mean(scale));
//                if (seq.rng->next1f() > continueProb) {
//                    lr = zero3f;
//                } else {
//                    lr = evalRadiance(ss.ray, seq, depth+1);
//                    lr /= continueProb;
//                }
//            } else {
//                lr = evalRadiance(ss.ray, seq, depth+1);
//            }
//            li = scale * lr;
//        }
//        return li;
//    } else {
        ClosePhoton lookupBuf[options::spphoton::photo_lookup_buf_size];
        PhotonProcess<Photon> process(options::spphoton::photo_lookup_buf_size, lookupBuf);

        vec3f ld = zero3f;
        lookup(_photonmap, f.o, process, _rr);
        ClosePhoton *photons = process.photons;
        int nfound = process.nfound;
        if (nfound > 0) {
            if (rs.type == LambertMaterial::TYPEID) {
                vec3f e = zero3f;
                for(int i = 0; i < nfound; i++) {
                    const Photon *p = photons[i].photon;
                    if(dot(f.z, p->wi) > 0.0f && dot(f.z, p->normal) > 0.0f)
                        e += p->alpha;
                }
                vec3f brdf = rs.rhod * consts<float>::inv_pi;
                ld = e * brdf * (consts<float>::inv_pi / _rr);
            } else {
                for(int i = 0; i < nfound; i++) {
                    const Photon *p = photons[i].photon;
                    vec3f brdf = evalReflectance(rs, f, p->wi, wo);
                    ld += p->alpha * brdf * (consts<float>::inv_pi / _rr);
                }
            }
        }
        return ld / _opts.photonrays;
//    }
}


vec2i SpPhotonRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* SpPhotonRender::getImage() const {
    return &(_imageSampling.image);
}
