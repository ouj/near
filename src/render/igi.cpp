#include "igi.h"
#include <scene/transform.h>
#include "options.h"
#include <common/geom.h>
#include "vpl.h"
#include <integration/opts.h>
#include <integration/utils.h>
#include <accelerator/accelerator.h>

IgiRenderOpts::IgiRenderOpts() : RenderOpts(IgiRender::TYPEID) {}

jsonObject IgiRenderOpts::print() const {
    jsonObject json;
    json["type"] = "igirenderopts";
    json["directmode"] = printDirectMode(directmode);
    json["arealightsamples"] = arealightsamples;
    json["envlightsamples"] = envlightsamples;
    json["indirectlightsamples"] = indirectlightsamples;
    json["indirectlightbatchsize"] = indirectlightbatchsize;
    json["maxdepth"] = maxdepth;
    json["directvplight"] = directvplight;
    json["geoclamp"] = geoclamp;
    json["generatortype"] = printSampleGenerator(generatortype);
    json["diffusemultibounce"] = diffusemultibounce;
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    return json;
}

void IgiRenderOpts::parse(const jsonObject &json) {
    error_if_not(jsonGet(json, "type").as_string() == "igirenderopts",
                 "incorrect type");
    directmode = parseDirectMode(jsonGet(json, "directmode"));
    arealightsamples = jsonGet(json, "arealightsamples");
    envlightsamples = jsonGet(json, "envlightsamples");
    indirectlightsamples = jsonGet(json, "indirectlightsamples");
    indirectlightbatchsize = jsonGet(json, "indirectlightbatchsize");
    maxdepth = jsonGet(json, "maxdepth");
    directvplight = jsonGet(json, "directvplight");
    geoclamp = jsonGet(json, "geoclamp");
    generatortype = parseSampleGenerator(jsonGet(json, "generatortype"));
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json, "general"));
}

IgiRender::IgiRender() : Render(TYPEID) {}

IgiRender::IgiRender(const IgiRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

IgiRender::~IgiRender() { clearState(); }

bool IgiRender::initState(const Scene* scene, const Accelerator *accelerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    generateLights();
    return true;
}

void IgiRender::clearState() {
    _ortLight.clear();
    _dirLight.clear();
    _ptLight.clear();
    _indirectLights.clear();
}

void IgiRender::generateLights() {
    range3f sceneBound = accelerator->computeWorldBound();
    vec3f sceneCenter = sceneBound.center();
    float sceneRadius = sceneBound.diagonal() * 0.5f;
    float rmin = sceneRadius * _opts.geoclamp;

    _r2min = rmin * rmin;
    warning_if(_opts.generatortype == ParametricFastSobolSampleGenerator::TYPEID,
               "parametric sequence is not supported in generating vpls");
    _generator = initGenerator(_opts.generatortype, 1, 1, 0);

    if (_opts.directvplight) {
        sampleDirectVpLights(_ptLight, _dirLight, _ortLight, scene->lights,
                             _generator, _opts.arealightsamples, _opts.envlightsamples);

        int nTotal = _ptLight.size() + _dirLight.size() + _ortLight.size();
        _directLights.reserve(nTotal);
        for (int i = 0; i < _ptLight.size(); i++)
            _directLights.push_back(&_ptLight[i]);
        for (int i = 0; i < _dirLight.size(); i++)
            _directLights.push_back(&_dirLight[i]);
        for (int i = 0; i < _ortLight.size(); i++)
            _directLights.push_back(&_ortLight[i]);
        message_va("direct light number: %d", _directLights.size());
    }

    int maxvpldepth = _opts.maxdepth - 1;
    sampleIndirectVpLights(_indirectLights, accelerator, scene->lights,
                           _generator, sceneCenter, sceneRadius,
                           _opts.indirectlightsamples, _opts.indirectlightbatchsize,
                           maxvpldepth, _opts.diffusemultibounce, _opts.general.parallel);
    message_va("indirect light number %d", _indirectLights.size());
}

static vec3f evalVpLight(const VpLight* light, const float pdf,
                         const ReflectanceSample &rs, const frame3f &f,
                         const vec3f &wo, const Accelerator *accelerator,
                         const float r2min) {
    if(pdf != 0) {
        VpLightEval ve = evalVpLight(light, f.o, wo, r2min);
        vec3f vl = ve.le * evalReflectance(rs,f,ve.ray.d,wo) * dot(ve.ray.d,f.z) / pdf;
        if (!vl.iszero()) {
            if(!accelerator->intersectAny(ve.ray)) return vl;
        }
    }
    return zero3f;
}

vec3f IgiRender::evalRadiance(const ray3f& ray, SampleSequence& seq) {
    // trace ray
    vec3f le = zero3f; vec3f ld = zero3f;
    vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, true, true, scene->lights, accelerator, f, wo, le, rs)) return le;
    if(!isvalid(rs)) return le;

    // direct
    if (!_opts.directvplight) {
        ld = evalDirect(rs, f, wo, seq, _opts.directmode, scene->lights,
                        accelerator);
    }

    // indirect
    vec3f li = zero3f;
    if (_opts.directvplight) {
        int index = seq.s % _directLights.size();
        const VpLight *direct = _directLights[index];
        ld += evalVpLight(direct, 1.0f / _directLights.size(), rs, f, wo,
                          accelerator, _r2min);
    }

    const OrientedVpLight &indirect = _indirectLights[seq.s % _indirectLights.size()];
    li += evalVpLight(&indirect, 1.0f / _indirectLights.size(), rs, f, wo,
                      accelerator, _r2min);

    // done
    return le+ld+li;
}

bool IgiRender::tracePass() {
    auto func = [this] (const ray3f& ray, SampleSequence& seq) {
        return evalRadiance(ray, seq);
    };
    sampleImage(scene, func, _imageSampling, _opts.imageSampling, _opts.general);
    return false;
}

Image<vec3f> IgiRender::traceImage() {
    renderImageFunc<IgiRender>(this, _opts.imageSampling);
    return _imageSampling.image;
}

vec2i IgiRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* IgiRender::getImage() const {
    return &(_imageSampling.image);
}
