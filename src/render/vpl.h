#ifndef _RENDER_VPL_H_
#define _RENDER_VPL_H_
#include <scene/scene.h>

struct Accelerator;
struct SampleGenerator;

struct VpLight {
    int type;
};

struct OrientedVpLight : VpLight {
    static const int TYPEID = 301;
    OrientedVpLight() { type = TYPEID; }
    OrientedVpLight(const vec3f &irr, const vec3f &pos, const vec3f &norm)
        : irradiance(irr), position(pos), normal(norm) { type = TYPEID; }
    vec3f irradiance;
    vec3f position;
    vec3f normal;
};

struct DirectVpLight : VpLight {
    static const int TYPEID = 302;
    DirectVpLight() { type = TYPEID; }
    DirectVpLight(const vec3f &le, const vec3f &dir)
        : radiance(le), direction(dir) { type = TYPEID; }
    vec3f radiance;
    vec3f direction;
};

struct PointVpLight : VpLight {
    static const int TYPEID = 303;
    PointVpLight() { type = TYPEID; }
    PointVpLight(const vec3f &i, const vec3f &pos)
        : intensity(i), position(pos) { type = TYPEID; }
    vec3f intensity;
    vec3f position;
};

struct VpLightEval {
    vec3f   le;
    ray3f   ray;
};

VpLightEval evalVpLight(const VpLight* light, const vec3f &pl, const vec3f &wo, float r2min);

void sampleDirectVpLights(std::vector<PointVpLight> &ptLights, std::vector<DirectVpLight> &dirLights,
    std::vector<OrientedVpLight> &ortLights, const std::vector<Light*> lights,
    SampleGenerator *generator, int areaLightSamples, int envLightSamples);

void sampleIndirectVpLights(std::vector<OrientedVpLight> &indirectLights, const Accelerator *accelerator,
    const std::vector<Light*> &lights, SampleGenerator* generator, const vec3f &center, float radius,
    int indirectsamples, int batchsize, int maxdepth, bool diffusemultibounce, bool parallel);

#endif // _TRACE_VPL_H_