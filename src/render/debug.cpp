#include "debug.h"
#include <scene/transform.h>
#include <integration/opts.h>
#include <integration/utils.h>
#include <accelerator/accelerator.h>

DebugRenderOpts::DebugRenderOpts() : RenderOpts(DebugRender::TYPEID) {}

jsonObject DebugRenderOpts::print() const {
    jsonObject json;
    json["type"] = "debugrenderopts";
    if (debugType == DEBUG_HIT) json["debug"] = "hit";
    else if (debugType == DEBUG_NORMAL) json["debug"] = "normal";
    else if (debugType == DEBUG_POSITION) json["debug"] = "position";
    else error("unknown debug type");
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    return json;
}

void DebugRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("debugrenderopts");
    if (jsonGet(json, "debug").as_string() == "hit") debugType = DEBUG_HIT;
    else if (jsonGet(json, "debug").as_string() == "normal") debugType = DEBUG_NORMAL;
    else if (jsonGet(json, "debug").as_string() == "position") debugType = DEBUG_POSITION;
    else error("unknown debug type");
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json,"general"));
}

DebugRender::DebugRender() : Render(TYPEID) {}

DebugRender::DebugRender(const DebugRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

DebugRender::~DebugRender() { clearState(); }

bool DebugRender::initState(const Scene* scene, const Accelerator *accelerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    return true;
}

void DebugRender::clearState() {}

vec3f evalRadiance(const ray3f& ray, int depth, SampleSequence& seq,
                   const Scene *scene, const Accelerator* accelerator,
                   const DebugType& debugType) {
    intersection3f intersection;
    if(accelerator->intersectFirst(ray, intersection)) {
        // shading frame
        frame3f &f = intersection.f;
        if (debugType == DEBUG_HIT) {
            return one3f;
        } else if (debugType == DEBUG_NORMAL) {
            vec3f normal = transformVectorInverse(scene->cameras[0]->transform, f.z);
            normal = f.z;
            return (normal);
        } else if (debugType == DEBUG_POSITION) {
            vec3f position = f.o;
            return abs_component(position);
        }
    }
    return zero3f;
}

bool DebugRender::tracePass() {
    auto func = [this](const ray3f& ray, SampleSequence& seq) {
        return evalRadiance(ray, 0, seq, scene, accelerator, _opts.debugType);
    };
    sampleImage(scene, func, _imageSampling, _opts.imageSampling, _opts.general);
    return false;
}

Image<vec3f> DebugRender::traceImage() {
    renderImageFunc<DebugRender>(this, _opts.imageSampling);
    return _imageSampling.image;
}

vec2i DebugRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* DebugRender::getImage() const {
    return &(_imageSampling.image);
}
