#include "lightcut.h"
#include <integration/opts.h>
#include <integration/utils.h>
#include "options.h"
#include "vpl.h"
#include <common/pam.h>
#include <accelerator/accelerator.h>
#include <CL/cl_platform.h>

LightcutRenderOpts::LightcutRenderOpts(): RenderOpts(LightcutRender::TYPEID) {}

jsonObject LightcutRenderOpts::print() const {
    jsonObject json;
    json["type"] = "lightcutrenderopts";
    json["general"] = printGeneralOpts(general);
    json["diffusemultibounce"] = diffusemultibounce;
    json["arealightsamples"] = arealightsamples;
    json["envlightsamples"] = envlightsamples;
    json["indirectlightsamples"] = indirectlightsamples;
    json["maxdepth"] = maxdepth;
    json["generatortype"] = printSampleGenerator(generatortype);
    json["geoclamp"] = geoclamp;
    json["maxcutsize"] = maxcutsize;
    json["maxerror"] = maxerror;
    json["blocksize"] = blocksize;
    return json;
}

void LightcutRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("lightcutrenderopts");
    general = parseGeneralOpts(jsonGet(json,"general"));
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
    arealightsamples = jsonGet(json, "arealightsamples");
    envlightsamples = jsonGet(json, "envlightsamples");
    indirectlightsamples = jsonGet(json, "indirectlightsamples");
    maxdepth = jsonGet(json, "maxdepth");
    generatortype = parseSampleGenerator(jsonGet(json, "generatortype"));
    geoclamp = jsonGet(json, "geoclamp");
    maxcutsize = jsonGet(json, "maxcutsize");
    maxerror = jsonGet(json, "maxerror");
    blocksize = jsonGet(json, "blocksize");
}

LightcutRender::LightcutRender()
    : Render(TYPEID) {}

LightcutRender::LightcutRender(const LightcutRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

LightcutRender::~LightcutRender() { clearState(); }

bool LightcutRender::initState(const Scene* scene, const Accelerator *acclerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    _image.assign(_opts.general.width, _opts.general.height,zero3f);
    int w = ceil((float)_opts.general.width / _opts.blocksize);
    int h = ceil((float)_opts.general.height / _opts.blocksize);
    _blocks.resize(w * h);
    for (uint j = 0; j < h; j++) {
        for (uint i = 0; i < w; i++) {
            range2u &block = _blocks.at(j * w + i);
            block.min = vec2u(i * _opts.blocksize, j * _opts.blocksize);
            block.max = vec2u(min((i+1)*_opts.blocksize, _opts.general.width),
                              min((j+1)*_opts.blocksize, _opts.general.height));
        }
    }
    _samples.assign(w * h, 0.0f);
    _passes = 0;
    return true;
}

void LightcutRender::clearState() {
    _ortLight.clear();
    _dirLight.clear();
    _ptLight.clear();
    _ortLightTree.clear();
    _dirLightTree.clear();
    _ptLightTree.clear();
    if (_generator) delete _generator;
}

void LightcutRender::initLightTree() {
    range3f sceneBound = accelerator->computeWorldBound();
    vec3f sceneCenter = sceneBound.center();
    float sceneRadius = sceneBound.diagonal() * 0.5f;
    float rmin = sceneRadius * _opts.geoclamp;
    float normalScale = sceneRadius * 0.125f;
    _r2min = rmin * rmin;
    // generate VPLs
    warning_if(_opts.generatortype == ParametricFastSobolSampleGenerator::TYPEID,
               "parametric sequence is not supported in generating vpls");
    _generator = initGenerator(_opts.generatortype, 1, 1, 0);

    std::vector<OrientedVpLight> directOtrLight;
    sampleDirectVpLights(_ptLight, _dirLight, directOtrLight,
                         scene->lights, _generator, _opts.arealightsamples,
                         _opts.envlightsamples);

    int nTotal = _ptLight.size() + _dirLight.size() + _ortLight.size();
    message_va("direct light number: %d", nTotal);

    int maxvpldepth = _opts.maxdepth - 1;
    std::vector<OrientedVpLight> indirectOtrLight;
    sampleIndirectVpLights(indirectOtrLight, accelerator,
                           scene->lights, _generator, sceneCenter,
                           sceneRadius, _opts.indirectlightsamples,
                           _opts.indirectlightbatchsize, maxvpldepth,
                           _opts.diffusemultibounce, _opts.general.parallel);
    message_va("indirect light number %d", indirectOtrLight.size());

    _ortLight.reserve(directOtrLight.size() + indirectOtrLight.size());
    for (int i = 0; i < directOtrLight.size(); i++)
        _ortLight.push_back(directOtrLight[i]);
    for (int i = 0; i < indirectOtrLight.size(); i++)
        _ortLight.push_back(indirectOtrLight[i]);

    // build light tree
    _ortLightTree = buildLightTree<OrtNode>(_ortLight, normalScale,
                                           _opts.general.parallel);
    _dirLightTree = buildLightTree<DirNode>(_dirLight, normalScale,
                                           _opts.general.parallel);
    _ptLightTree = buildLightTree<PtNode>(_ptLight, normalScale,
                                         _opts.general.parallel);
}


struct LightcutItem {
    LightcutItem(int type, const vec3f &l, const vec3f &bound, int node)
        : type(type), l(l), bound(bound), node(node) {}
    int     type;
    vec3f   l;
    vec3f   bound;
    int     node;
    float   error = consts<float>::max;
};

inline bool operator< (const LightcutItem &p1, const LightcutItem &p2) {
    return p1.error == p2.error ? (p1.node > p2.node) : p1.error < p2.error;
}

vec3f evalNode(const VpLight* light, const Accelerator* accelerator,
               const ReflectanceSample& rs,
               const frame3f& f, const vec3f& wo, float r2min) {
    float g = 0.0f;
    ray3f ray;
    switch (light->type) {
        case PointVpLight::TYPEID: {
            const PointVpLight *l = (const PointVpLight*)light;
            g = 1.0f / (f.o -l->position).lengthSqr();
            ray = makeraysegment3f(f.o,l->position);
        } break;
        case DirectVpLight::TYPEID: {
            const DirectVpLight *l = (const DirectVpLight*)light;
            g = 1.0f;
            ray = ray3f(f.o,-l->direction);
        } break;
        case OrientedVpLight::TYPEID: {
            const OrientedVpLight *l = (const OrientedVpLight*)light;
            vec3f wi = normalize(l->position - f.o);
            float r2 = (l->position - f.o).lengthSqr();
            g = max(0.0f, dot(l->normal, -wi)) / max(r2min, r2);
            ray = makeraysegment3(f.o, l->position);
        } break;
        default: error("unknow virtual light type"); break;
    }
    vec3f vl = evalReflectance(rs,f,ray.d,wo) * abs(dot(ray.d, f.z)) * g;
    if (!vl.iszero()) {
        if(!accelerator->intersectAny(ray)) return vl;
    }
    return zero3f;
}

float boundCosTerm(const range3f &bbox, const ReflectanceSample& rs,
                    const frame3f& f, const vec3f& wo) {
    range3f xbox = f.transformRangeInverse(bbox);
    float cosb;
    if (xbox.max.z > 0) {;
        float minx2 = (sign(xbox.min.x) != sign(xbox.max.x)) ? 0.0f :
        min(pow2(xbox.min.x), pow2(xbox.max.x));

        float miny2 = (sign(xbox.min.y) != sign(xbox.max.y)) ? 0.0f :
        min(pow2(xbox.min.y), pow2(xbox.max.y));
        cosb = xbox.max.z / sqrt(minx2 + miny2 + pow2(xbox.max.z));
    }
    else
        cosb = 0.0f;
    cosb = clamp(cosb, 0.0f, 1.0f);
    return cosb;
}

vec3f evalReflectanceBound(const range3f &bbox, const ReflectanceSample& rs,
                           const frame3f& f, const vec3f& wo) {
    float cosBound = boundCosTerm(bbox, rs, f, wo);
    vec3f diffuseBound = zero3f;
    vec3f specularBound = zero3f;
    switch(rs.type) {
        case LambertMaterial::TYPEID: {
            diffuseBound = rs.rhod * consts<float>::inv_pi;
        } break;
        case MicrofacetMaterial::TYPEID: {
            warning_if_not(rs.diffuseMode == MDM_LAMBERT, "unsupported diffuse mode");
            warning_if_not(rs.specularMode == MSM_BLINNPHONG, "unsupported specular mode");
            frame3f rf;
            rf.o = f.o;
            rf.z = wo;
            rf.y = normalize(cross(wo, f.z));
            rf.x = normalize(cross(rf.y, rf.z));
            range3f tbox = rf.transformRangeInverse(bbox);
            float maxcosphih = 0.0f;
            float maxcosthetah = 0.0f;

            if (tbox.max.y >= 0) {
                float minx2 = (sign(tbox.min.x) != sign(tbox.max.x)) ? 0.0f:
                               min(pow2(tbox.min.x), pow2(tbox.max.x));
                maxcosphih = tbox.max.y / sqrt(minx2 + pow2(tbox.max.y));
            } else {
                float maxx2 = min(pow2(tbox.min.x), pow2(tbox.max.x));
                maxcosphih = tbox.max.y / sqrt(maxx2 + pow2(tbox.max.y));
            }

            vec2f costhetai = vec2f(0.0f, 0.0f);
            if (tbox.max.z >= 0) {;
                float minx2 = (sign(tbox.min.x) != sign(tbox.max.x)) ? 0.0f :
                min(pow2(tbox.min.x), pow2(tbox.max.x));
                float miny2 = (sign(tbox.min.y) != sign(tbox.max.y)) ? 0.0f :
                min(pow2(tbox.min.y), pow2(tbox.max.y));
                costhetai.x = tbox.max.z / sqrt(minx2 + miny2 + pow2(tbox.max.z));
            }
            costhetai.x = clamp(costhetai.x, 0.0f, 1.0f);
            if (tbox.min.z >= 0) {;
                float minx2 = (sign(tbox.min.x) != sign(tbox.max.x)) ? 0.0f :
                min(pow2(tbox.min.x), pow2(tbox.max.x));
                float miny2 = (sign(tbox.min.y) != sign(tbox.max.y)) ? 0.0f :
                min(pow2(tbox.min.y), pow2(tbox.max.y));
                costhetai.y = tbox.min.z / sqrt(minx2 + miny2 + pow2(tbox.min.z));
            }
            costhetai.y = clamp(costhetai.y, 0.0f, 1.0f);

            //bound interval of cos(theta_h)
            vec2f costhetah = sqrt_component((costhetai+one2f)*0.5f);

            float costhetan = dot(f.z, rf.z);
            float costhetan2 = pow2(costhetan);
            float sinthetan2 = 1 - costhetan2;
            float sinthatan = sqrt(sinthetan2);
            if (maxcosphih >= 0.0f) {
                maxcosthetah = sqrtf(costhetan2 / (costhetan2 + sinthetan2 * pow2(maxcosphih)));
            } else {
                maxcosthetah = 1.0f;
            }

            if (maxcosphih < costhetah.x) {
                maxcosphih = costhetah.x;
            } else if (maxcosphih > costhetah.y) {
                maxcosphih = costhetah.y;
            }

            float maxsinthetah = sqrt(1 - pow2(maxcosthetah));
            float nhBound = sinthatan * maxsinthetah * maxcosphih + costhetan * maxcosthetah;
            diffuseBound = rs.rhod * consts<float>::inv_pi;
            specularBound = rs.rhos * ((rs.n+2) * pow(nhBound,rs.n) / (2 * consts<float>::pi));
        } break;
        default: error("unsupported reflectance");
    }
    return (diffuseBound + specularBound) * cosBound;
}

vec3f evalBound(const OrtNode& node, const ReflectanceSample& rs,
                const frame3f& f, const vec3f& wo, float r2min) {
    //find closest point on bounding box.
    vec3f boundp = min_component(node.bbox.max, max_component(node.bbox.min, f.o));
    float lenSqr = max((boundp - f.o).lengthSqr(), r2min);

    float cosBound = 1.0f;
    float cosHalfAngle = cosAngle(node.cone);
    if (cosHalfAngle <= 0.0f)
        cosBound = 1.0f;
    else {
        range3f bbox = range3f(f.o-node.bbox.max, f.o-node.bbox.min);
        frame3f rf;
        rf.o = zero3f;
        rf.z = node.cone.getAxis();
        xyFromZ(rf.z, rf.x, rf.y);
        range3f xbox = rf.transformRangeInverse(bbox);
        float cosTheta = 0;
        if (xbox.max.z >= 0) {
            float minx2 = (sign(xbox.min.x) != sign(xbox.max.x)) ?
                  0.0f : min(pow2(xbox.min.x), pow2(xbox.max.x));

            float miny2 = (sign(xbox.min.y) != sign(xbox.max.y)) ?
                  0.0f : min(pow2(xbox.min.y), pow2(xbox.max.y));
            float maxz2 = pow2(xbox.max.z);
            cosTheta = xbox.max.z / sqrt(minx2 + miny2 + maxz2);
        } else {
            cosTheta = 0.0f;
        }

        cosTheta = clamp(cosTheta, 0.0f, 1.0f);
        if (cosTheta > cosHalfAngle) // means theta_cone > theta_bound
            cosBound = 1.0f;
        else {
            //compute cos(Theta - HalfAngle);
            float sinHalfAngle = sqrt(1 - cosHalfAngle * cosHalfAngle);
            float sinTheta = sqrt(1 - cosTheta * cosTheta);
            cosBound = cosTheta * cosHalfAngle + sinTheta * sinHalfAngle;
            cosBound = clamp(cosBound, 0.0f, 1.0f);
        }
    }
    vec3f brdf = evalReflectanceBound(node.bbox, rs, f, wo);
    vec3f le =  node.le * brdf * cosBound / lenSqr;
    return max_component(zero3f,le);
}

vec3f LightcutRender::evalRadiance(const ray3f& ray, int *cutSize) {
    // trace ray
    vec3f le = zero3f;
    vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, true, true, scene->lights, accelerator, f, wo, le, rs)) return le;
    if(!isvalid(rs)) return le;

    std::vector<LightcutItem> lightcut;
    vec3f l = zero3f;
    lightcut.reserve(_opts.maxcutsize);
    auto lighttype = OrientedVpLight::TYPEID;
    if (!_ortLightTree.empty()) {
        OrtNode &root = _ortLightTree.at(0);
        vec3f estimate = evalNode(root.light, accelerator, rs, f, wo, _r2min);
        vec3f bound = evalBound(root, rs, f, wo, _r2min);
        l += root.le * estimate;
        lightcut.emplace_back(lighttype, estimate, bound, 0);
        std::push_heap(lightcut.begin(), lightcut.end());
    }

    while (lightcut.size() < _opts.maxcutsize) {
        float err = (lightcut[0].error == 0.0f) ? 0.0f :
                    lightcut[0].bound.luminance() / l.luminance();
        if (err <= _opts.maxerror) break;
        LightcutItem item = lightcut[0];
        std::pop_heap(lightcut.begin(), lightcut.end());
        lightcut.pop_back();

        if (item.type != lighttype)
            error("unsupported light type");

        std::vector<OrtNode> &nodes = _ortLightTree;
        const OrtNode &node = nodes[item.node];
        const OrtNode &leftNode = nodes[node.leftIndex];
        const OrtNode &rightNode = nodes[node.rightIndex];

        vec3f leftL =
            (node.light == leftNode.light) ? item.l :
                evalNode(leftNode.light, accelerator, rs, f, wo, _r2min);
        vec3f ll = leftNode.le * leftL;

        vec3f rightL =
            (node.light == rightNode.light) ? item.l :
                evalNode(rightNode.light, accelerator, rs, f, wo, _r2min);
        vec3f rl = rightNode.le * rightL;

        l = l - (node.le * item.l) + (ll + rl);

        if (leftNode.leftIndex < nodes.size() && leftNode.rightIndex < nodes.size()) {
            vec3f bound = evalBound(leftNode, rs, f, wo, _r2min);
            lightcut.emplace_back(lighttype, leftL, bound, node.leftIndex);
            std::push_heap(lightcut.begin(), lightcut.end());
        }

        if (rightNode.leftIndex < nodes.size() && rightNode.rightIndex < nodes.size()) {
            vec3f bound = evalBound(rightNode, rs, f, wo, _r2min);
            lightcut.emplace_back(lighttype, rightL, bound, node.rightIndex);
            std::push_heap(lightcut.begin(), lightcut.end());
        }
    }
    if (cutSize) *cutSize = lightcut.size();
    return l + le;
}

void LightcutRender::renderPixel(int x, int y) {
    vec2f is = vec2f((x+0.5f)/_image.width(),
                     (y+0.5f)/_image.height());
    ray3f ray = sampleCamera(scene->cameras[0], is);
    vec3f l = evalRadiance(ray, nullptr);
    _image.at(x, y) = l;
}

void LightcutRender::renderBlock(int index) {
    const range2u &block = _blocks.at(index);
    if (_opts.general.parallel) {
        auto func = [this](const tbb::blocked_range2d<int>& r) {
            for (int j = r.rows().begin(); j < r.rows().end(); j++) {
                for (int i = r.cols().begin(); i < r.cols().end(); i++) {
                    renderPixel(i, j);
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range2d<int>(block.min.y, block.max.y,
                                                    block.min.x, block.max.x),
                          func, tbb::simple_partitioner());
    } else {
        for (int j = block.min.y; j < block.max.y; j++) {
            for (int i = block.min.x; i < block.max.x; i++) {
                renderPixel(i, j);
            }
        }
    }
    _samples.at(index) += 1.0f;
}

bool LightcutRender::tracePass() {
    renderBlock(_passes++ % _blocks.size());
    return false;
}

Image<vec3f> LightcutRender::traceImage() {
    for (int p = 0; p < _blocks.size(); p++)
        renderBlock(p);
    return _image;
}

vec2i LightcutRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* LightcutRender::getImage() const {
    return &_image;
}
