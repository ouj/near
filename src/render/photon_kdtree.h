#ifndef _RENDER_KD_TREE_H_
#define _RENDER_KD_TREE_H_
#include <common/vec.h>
#include <common/array.h>
#include <algorithm>
#include <common/geom.h>
#include <vector>

struct Photon {
    Photon() {}
    Photon(const vec3f &p, const vec3f &n, const vec3f &a, const vec3f &w)
    : position(p), normal(n), alpha(a), wi(w) {}
    vec3f position;
    vec3f normal;
    vec3f alpha;
    vec3f wi;
};

struct KdNode {
    float       split;
    int         axis;
    int         leftIndex;
    int         rightIndex;
};


struct KdTree {
    KdTree() = default;
    KdTree(KdTree &&kt) { nodes.swap(kt.nodes); data.swap(kt.data); }
    KdTree(const KdTree &kt) = default;
    KdTree& operator=(const KdTree &kt) = default;
    KdTree& operator=(KdTree &&kt) { nodes.swap(kt.nodes); data.swap(kt.data); return *this; }
    std::vector<KdNode> nodes;
    std::vector<Photon> data;
};

KdTree buildKdTree(Photon** data, int size);
KdTree parallelBuildKdTree(Photon** data, int size);

inline float component(const vec3f &v, int index) { return ((float*)&v.x)[index]; }
template <typename LookupProc>
void __privateLookup(const KdTree &tree, int nodeNum, const vec3f &p,
    LookupProc &process, float &maxDistSquared) {
        const KdNode *node = &(tree.nodes[nodeNum]);
        // Process kd-tree node's children
        int axis = node->axis;
        if (axis != -1) {
            float px = component(p, axis);
            float d = px - node->split;
            float dist2 = d * d;

            if (px <= node->split) {
                if (node->leftIndex < tree.nodes.size())
                    __privateLookup(tree, node->leftIndex, p, process, maxDistSquared);
                if (dist2 < maxDistSquared && node->rightIndex < tree.nodes.size())
                    __privateLookup(tree, node->rightIndex, p, process, maxDistSquared);
            }
            else {
                if (node->rightIndex < tree.nodes.size())
                    __privateLookup(tree, node->rightIndex, p, process, maxDistSquared);
                if (dist2 < maxDistSquared && node->leftIndex < tree.nodes.size())
                    __privateLookup(tree, node->leftIndex, p, process, maxDistSquared);
            }
        }

        // Hand kd-tree node to processing function
        float dist2 = distanceSqr(tree.data[nodeNum].position, p);
        if (dist2 < maxDistSquared)
            process(p, tree.data[nodeNum], dist2, maxDistSquared);
}

template <typename LookupProc>
void lookup(const KdTree &tree, const vec3f &p, LookupProc &proc,
    float &maxDistSquared) {
    if (tree.nodes.empty()) return;
        __privateLookup(tree, 0, p, proc, maxDistSquared);
}

struct ClosePhoton {
    const Photon *photon;
    float distanceSquared;
};

inline bool operator<(const ClosePhoton &p1, const ClosePhoton &p2) {
    return p1.distanceSquared == p2.distanceSquared ?
        (p1.photon < p2.photon) : (p1.distanceSquared < p2.distanceSquared);
}


inline ClosePhoton makeClosePhoton(const Photon* p, float md2 = std::numeric_limits<float>::max()) {
    ClosePhoton photon;
    photon.photon = p;
    photon.distanceSquared = md2;
    return photon;
}

template<typename PHOTON>
struct PhotonProcess {
    PhotonProcess(unsigned int mp, ClosePhoton *buf) : photons(buf), nlookup(mp), nfound(0) {} ;
    void operator()(const vec3f &p, const PHOTON &photon, float dist2,
        float &maxDistSquared) {
            if (nfound < nlookup) {
                // Add photon to unordered array of photons
                photons[nfound++] = makeClosePhoton(&photon, dist2);
                if (nfound == nlookup) {
                    std::make_heap(&photons[0], &photons[nlookup]);
                    maxDistSquared = photons[0].distanceSquared;
                }
            }
            else {
                // Remove most distant photon from heap and add new photon
                std::pop_heap(&photons[0], &photons[nlookup]);
                photons[nlookup-1] = makeClosePhoton(&photon, dist2);
                std::push_heap(&photons[0], &photons[nlookup]);
                maxDistSquared = photons[0].distanceSquared;
            }
    }
    ClosePhoton *photons;
    unsigned int nlookup;
    unsigned int nfound;
};

#endif // _KD_TREE_H_