#ifndef RENDER_DEBUG_H
#define RENDER_DEBUG_H

#include "render.h"
#include <integration/integration.h>

enum DebugType{ DEBUG_HIT, DEBUG_POSITION, DEBUG_NORMAL };

struct DebugRenderOpts : public RenderOpts {
    DebugRenderOpts();
    jsonObject print() const;
    void parse(const jsonObject &opts);

    DebugType           debugType = DEBUG_NORMAL;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
};

class DebugRender : public Render {
public:
    const static int TYPEID = 800600;
    DebugRender();
    DebugRender(const DebugRenderOpts &opts);
    virtual ~DebugRender();

    virtual bool                initState(const Scene *scene, const Accelerator* accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    ImageSamplingState          _imageSampling;
    DebugRenderOpts             _opts;
};

#endif //TRACE_DEBUG_H
