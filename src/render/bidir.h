#ifndef RENDER_BIDIR_H
#define RENDER_BIDIR_H

#include "render.h"
#include <integration/integration.h>
#include "path_vertex.h"
#include "options.h"

// opts ------------------------
struct BidirRenderOpts : RenderOpts {
    BidirRenderOpts();
    jsonObject print() const;
    void parse(const jsonObject &opts);

    TraceDirectMode     directmode = TDM_ALL;
    TraceDirectMode     bounceddirectmode = TDM_UNIFORM;
    int                 maxdepth = 2;
    bool                diffusemultibounce = true;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
};

typedef std::array<PathVertex, options::bidir::maxpathlength> PathBuffer;
struct BidirRender : Render {
    const static int TYPEID = 800605;
    BidirRender();
    BidirRender(const BidirRenderOpts &opts);
    virtual ~BidirRender();

    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
protected:
    vec3f evalRadiance(const ray3f& ray, SampleSequence& seq);
    vec3f LPath(SampleSequence &seq,
                const PathBuffer &path, int pLength,
                const ray3f &escapedRay, const vec3f &escapedAlpha);
    vec3f LBidir(SampleSequence &seq,
                 const PathBuffer &camPath, int camLength,
                 const PathBuffer &ltPath, int ltLength,
                 const ray3f &escRay, const vec3f &escAlpha);

    ImageSamplingState       _imageSampling;
    vec3f                    _sceneCenter;
    float                    _sceneRadius;
    BidirRenderOpts          _opts;
};

#endif
