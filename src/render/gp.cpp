#include "gp.h"
#include <integration/sequence.h>
#include <integration/integration.h>
#include <integration/sampling.h>
#include <accelerator/accelerator.h>

std::vector<GatherPoint> sampleGatherPoints(const Camera* camera, const Accelerator *accelerator, const std::vector<Light*> &lights,
                                       SampleGenerator* generator, Image<Rng> &rngs, int ss,
                                       int w, int h, bool parallel) {
    std::vector<GatherPoint> gps;
    for (int j = 0; j < h; j++) {
        for (int i = 0; i < w; i++) {
            SampleSequence seq;
            initSequence(seq, generator, rngs.at(i,j), i, j, 0, ss);
            for (int s = 0; s < ss; s++) {
                intersection3f   ti;
                vec2f rs = next2f(seq);
                vec2f is = vec2f((i+rs.x)/w, (j+rs.y)/h);
                ray3f ray = sampleCamera(camera, is);
                vec3f le = zero3f;
                GatherPoint hp;
                if(accelerator->intersectFirst(ray, ti)) {
                    hp.f = ti.f;
                    hp.wo = -ray.d;
                    EmissionSample es = sampleEmission(ti.m, ti.st);
                    le = isvalid(es) ? evalEmission(es,ti.f,-ray.d) : zero3f;
                    hp.rs = sampleReflectance(ti.m, ti.st);
                    hp.le = le;
                    hp.weight = 1.0f / ss;
                    hp.pixel = vec2i(i, j);
                    gps.push_back(hp);
                }
                nextSample(seq);
            }
            float p = (float)(j * w + i) / (w * h);
            fprintf(stdout, "\rprogress: %.2f", p * 100);
            fflush(stdout);
        }
    }
    return gps;
}
