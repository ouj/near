#ifndef RENDER_LIGHTCUT_H
#define RENDER_LIGHTCUT_H

#include "render.h"
#include <integration/integration.h>
#include "lighttree.h"

// opts ------------------------
struct LightcutRenderOpts : RenderOpts {
    LightcutRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    GeneralOpts         general;
    bool                diffusemultibounce = false;
    int                 arealightsamples = 1024;
    int                 envlightsamples = 8192;
    int                 indirectlightsamples = 100000;
    int                 indirectlightbatchsize = 5000;
    int                 maxdepth = 16;
    int                 generatortype = PixelSobolSampleGenerator::TYPEID;
    float               geoclamp = 0.1f;;
    int                 maxcutsize = 1000;
    float               maxerror = 0.02f;
    int                 blocksize = 64;
};

struct LightcutRender : Render {
    const static int TYPEID = 800608;
    LightcutRender();
    LightcutRender(const LightcutRenderOpts &opts);
    virtual ~LightcutRender();

    // operations ------------------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    void                        initLightTree();
    void                        renderBlock(int blockIndex);
    void                        renderPixel(int x, int y);
    vec3f                       evalRadiance(const ray3f& ray, int *cutSize = nullptr);
    LightcutRenderOpts              _opts;
    std::vector<OrientedVpLight>    _ortLight;
    std::vector<DirectVpLight>      _dirLight;
    std::vector<PointVpLight>       _ptLight;
    std::vector<OrtNode>            _ortLightTree;
    std::vector<DirNode>            _dirLightTree;
    std::vector<PtNode>             _ptLightTree;
    float                           _r2min = 0.0f;
    SampleGenerator                *_generator = nullptr;
    Image<vec3f>                    _image;
    std::vector<range2u>            _blocks;
    std::vector<float>              _samples;
    int                             _passes = 0;
};

#endif // RENDER_LIGHTCUT_H
