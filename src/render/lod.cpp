#include "lod.h"
#include "options.h"
#include <scene/transform.h>
#include <common/geom.h>
#include <common/nrange.h>
#include <integration/opts.h>
#include <integration/utils.h>
#include <scene/light.h>
#include <common/pam.h>
#include <common/morton.h>

using namespace std;
using namespace tbb;

LodRenderOpts::LodRenderOpts() : RenderOpts(LodRender::TYPEID) {}

jsonObject LodRenderOpts::print() const {
    jsonObject json;
    json["type"] = "lodrenderopts";
    json["maxdepth"] = maxdepth;
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    json["tau"] = tau;
    if(mode == NO_LOD) {
        json["mode"] = "nolod";
    } else if (mode == LOD){
        json["mode"] = "lod";
    }
    return json;
}

void LodRenderOpts::parse(const jsonObject &json) {
    error_if_not(jsonGet(json, "type").as_string() == "lodrenderopts",
                 "incorrect type");
    maxdepth = jsonGet(json, "maxdepth");
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json, "general"));
    tau = jsonGet(json, "tau");
    string mode = jsonGet(json, "mode");
    if(mode == "nolod") {
        mode = NO_LOD;
    } else if (mode == "lod") {
        mode = LOD;
    } else error("unknown mode");
}

//#define UNIFORM_SOLIDANGEL
#define TEST_VERIFY

LodRender::LodRender() : Render(TYPEID) {}

LodRender::LodRender(const LodRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

LodRender::~LodRender() { clearState(); }

float pixelRadius(const Camera* camera, int width) {
    switch (camera->type) {
        case PerspectiveCamera::TYPEID: {
            const PerspectiveCamera *cam = (const PerspectiveCamera*)camera;
            float r = (cam->width / cam->distance) / width;
            return r;
        } break;
        default: warning("unsupported camera type");
            return 0.0f;
            break;
    }
}

bool LodRender::initState(const Scene* scene, const Accelerator *accelerator) {
    // set scene
    if(error_if(accelerator->type != StreamBvhAccelerator::TYPEID,
                "incorrect accelerator")) return false;
    this->accelerator = (StreamBvhAccelerator*)accelerator;
    _lightAccelerator = new ListAccelerator();
    _lightAccelerator->buildAccelerator(std::vector<Surface*>(), scene->lights);

    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);

    // setup image blocks for block rendering.
    const uint blockSize = _opts.imageSampling.blocksize;
    uint bWidth = (int)ceil((float)_opts.general.width / blockSize);
    uint bHeight = (int)ceil((float)_opts.general.height / blockSize);
    for (int j = 0; j < bHeight; j++) {
        for (int i = 0; i < bWidth; i++) {
            _imageBlocks.emplace_back(vec2u(i*blockSize, j*blockSize),
                                      vec2u(min((i+1)*blockSize, _opts.general.width),
                                            min((j+1)*blockSize, _opts.general.height)));
        }
    }
    sort(_imageBlocks.begin(), _imageBlocks.end(),
         [blockSize](const range2u &b1, const range2u &b2) -> bool{
             uint c1 = morton2d::encode(b1.min.x / blockSize, b1.min.y / blockSize);
             uint c2 = morton2d::encode(b2.min.x / blockSize, b2.min.y / blockSize);
             return c1 < c2;
         });
    _blockIndex = 0;
    _passes = 0;

    float r = pixelRadius(scene->cameras[0], _opts.general.width * sqrt(_opts.imageSampling.maxsamples));
    _gamma = r * r * consts<float>::pi;
    return true;
}

void LodRender::clearState() {
    if(_lightAccelerator) delete _lightAccelerator;
}

ray3f LodRender::sampleRay(int i, int j, SampleSequence &seq) const {
    vec2f rs = next2f(seq);
    vec2f is = vec2f((i+rs.x)/_opts.general.width, (j+rs.y)/_opts.general.height);
    ray3f ray = sampleCamera(scene->cameras[0], is);
    return ray;
}

bool LodRender::traceRay(const ray3f& ray, bool resolveEmission, bool resolveEnvironment,
                              frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs) const {
    // init
    le = zero3f;
    rs = invalidReflectanceSample;

    // intersect
    intersection3f intersection;
    if(!accelerator->intersectFirst(ray, intersection)) {
        if(resolveEnvironment) le = evalEnvironment(ray, scene->lights);
        return false;
    }

    // shading frame
	f = intersection.f;
    wo = -ray.d;

    // emission
    if(resolveEmission) {
        EmissionSample es = sampleEmission(intersection.m,intersection.st);
        if (isvalid(es)) le = evalEmission(es,f,-ray.d);
    }

    // reflectance
    rs = sampleReflectance(intersection.m,intersection.st);

    // done
    return true;
}

bool LodRender::traceRayLOD(const ray3f& ray, bool resolveEmission, bool resolveEnvironment,
                                 frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs, float
                                 sigma2, float &mint) const {
    // init
    le = zero3f;
    rs = invalidReflectanceSample;

    // intersect
    intersectionlod3f intersection;
    if(!_accelerator->intersectFirstLOD(ray, intersection, sigma2)) {
        if(resolveEnvironment) le = evalEnvironment(ray, scene->lights);
        return false;
    }

    // shading frame
	f = intersection.f;
    wo = -ray.d;
    mint = intersection.mint;

    // emission
    if(resolveEmission) {
        EmissionSample es = sampleEmission(intersection.m,intersection.st);
        if (isvalid(es)) le = evalEmission(es,f,-ray.d);
    }

    // reflectance
    rs = sampleReflectance(intersection.m,intersection.st);

    // done
    return true;
}

vec3f LodRender::evalDirect(const ReflectanceSample& rs, const frame3f& f,
                                 const vec3f& wo, const vec2f& ls, const Light* light) const {
    // sample shadow
    ShadowSample ss = sampleShadow(light, f.o, ls);
    if(ss.le.iszero()) return zero3f;

    // accumulate direct
    vec3f brdf = evalReflectance(rs,f,ss.ray.d,wo);
    float projection = dot(ss.ray.d, f.z);
    vec3f ld = ss.le * brdf * projection / ss.pdf;

    if(ld.iszero()) return zero3f;
    if(accelerator->intersectAny(ss.ray)) return zero3f;
    else return ld;
}

vec3f LodRender::evalDirectMis(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                    const vec2f& ls, float bls, const vec2f& bas, const Light* light) const {
    error_if_not(!isDelta(light), "MIS only works for non-delta lights");

    // init values
    vec3f ld = zero3f;

    // sample according to the light
    ShadowSample lss = sampleShadow(light, f.o, ls);
    if(!lss.le.iszero()) {
        vec3f ldl = lss.le * evalReflectance(rs,f,lss.ray.d,wo) *
        dot(lss.ray.d, f.z) / lss.pdf;
        if(!ldl.iszero()) {
            if(!accelerator->intersectAny(lss.ray)) {
                float brdfPdf = sampleScatteringPdf(rs, f, wo, lss.ray.d);
                float w = powerHeuristic(lss.pdf, brdfPdf);
                ld += ldl * w;
            }
        }
    }

    // sample according to the brdf
    ScatteringSample bss = sampleScattering(rs, f, wo, bls, bas);
    if(!bss.bsdf.iszero()) {
        ShadowSample ss = sampleShadow(light, f.o, bss.ray.d);
        if(ss.pdf > 0) {
            float w = powerHeuristic(bss.pdf, ss.pdf);
            assert(ss.ray.d == bss.ray.d);
            vec3f ldb = zero3f;
            if(!accelerator->intersectAny(ss.ray)) {
                ldb = ss.le;
            } else {
                ldb = sampleEnvironment(light, bss.ray.d);
            }
            if(!ldb.iszero()) {
                ld += ldb * bss.bsdf * dot(bss.ray.d, f.z) / bss.pdf * w;
            }
        }
    }

    return ld;
}

vec3f LodRender::evalDirect(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                 SampleSequence& seq) const {
    const std::vector<Light*>& lights = scene->lights;
    vec3f ld = zero3f;
    LightSample ls = sampleLight(lights,next1f(seq));
//    ld = evalDirect(rs, f, wo, next2f(seq), ls.light) / ls.pdf;
    if(isDelta(ls.light)) ld = evalDirect(rs, f, wo, next2f(seq), ls.light) / ls.pdf;
    else ld = evalDirectMis(rs, f, wo, next2f(seq), next1f(seq), next2f(seq), ls.light) / ls.pdf;
    return ld;
}

inline vec3f intersectAnyLOD(StreamBvhAccelerator *accelerator, const ray3f &ray, float sigma, const vec3f &l) {
#ifdef TEST_VERIFY
    float occlusion = accelerator->intersectAnyLOD(ray, sigma);
    if (occlusion >= 1.0) return zero3f;
    else if (occlusion <= 0.0f) return l;
    else return accelerator->intersectAny(ray) ? zero3f : l;
#else
    float occlusion = accelerator->intersectAnyLOD(ray, sigma);
    return l * (1.0f - occlusion);
#endif
}

vec3f LodRender::evalDirectLOD(const ReflectanceSample& rs, const frame3f& f,
                                    const vec3f& wo, const vec2f& ls, const Light* light, float sigma2, float mint) const {
    // sample shadow
    ShadowSample ss = sampleShadow(light, f.o, ls);
    if(ss.le.iszero()) return zero3f;

    // accumulate direct
    vec3f brdf = evalReflectance(rs,f,ss.ray.d,wo);
    float projection = dot(ss.ray.d, f.z);
    vec3f ld = ss.le * brdf * projection / ss.pdf;

    if(ld.iszero()) return zero3f;
    ss.ray.mint = mint;
#ifdef UNIFORM_SOLIDANGEL
    float sigma = consts<float>::twopi / _opts.tau;
#else
    float sigma = (1.0f / ss.pdf) / _opts.tau;
#endif
    return ::intersectAnyLOD(_accelerator, ss.ray, sigma, ld);
}

vec3f LodRender::evalDirectMisLOD(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                       const vec2f& ls, float bls, const vec2f& bas, const Light* light,
                                       float sigma2, float mint) const {
    error_if_not(!isDelta(light), "MIS only works for non-delta lights");

    // init values
    vec3f ld = zero3f;

    // sample according to the light
    ShadowSample lss = sampleShadow(light, f.o, ls);
    if(!lss.le.iszero()) {
        vec3f ldl = lss.le * evalReflectance(rs,f,lss.ray.d,wo) *
        dot(lss.ray.d, f.z) / lss.pdf;
        if(!ldl.iszero()) {
            float brdfPdf = sampleScatteringPdf(rs, f, wo, lss.ray.d);
            float w = powerHeuristic(lss.pdf, brdfPdf);
            lss.ray.mint = mint;
#ifdef UNIFORM_SOLIDANGEL
            float sigma = consts<float>::twopi / _opts.tau;
#else
            float sigma = (1.0f / lss.pdf) / _opts.tau;
#endif
            ld += ::intersectAnyLOD(_accelerator, lss.ray, sigma, ldl) * w;
        }
    }

    // sample according to the brdf
    ScatteringSample bss = sampleScattering(rs, f, wo, bls, bas);
    if(!bss.bsdf.iszero()) {
        ShadowSample ss = sampleShadow(light, f.o, bss.ray.d);
        if(ss.pdf > 0) {
            float w = powerHeuristic(bss.pdf, ss.pdf);

            vec3f ldb = zero3f;
            // check if this a light that contains an emission
            if(light->type == AreaLight::TYPEID) {
                vec3f le = ss.le;
                if (!le.iszero()) {
                    bss.ray.mint = mint;
                    bss.ray.maxt = ss.ray.maxt;
#ifdef UNIFORM_SOLIDANGEL
                    float sigma = consts<float>::twopi / _opts.tau;
#else
                    float sigma = (1.0f / bss.pdf) / _opts.tau;
#endif
                    ldb = ::intersectAnyLOD(_accelerator, bss.ray, sigma, le);
                }
            } else {
                vec3f le = sampleEnvironment(light, bss.ray.d);
                if(!le.iszero()) {
                    bss.ray.mint = mint;
#ifdef UNIFORM_SOLIDANGEL
                    float sigma = consts<float>::twopi / _opts.tau;
#else
                    float sigma = (1.0f / bss.pdf) / _opts.tau;
#endif
                    ldb = ::intersectAnyLOD(_accelerator, bss.ray, sigma, le);
                }
            }
            if(!ldb.iszero()) {
                ld += ldb * bss.bsdf * dot(bss.ray.d, f.z) / bss.pdf * w;
            }
        }
    }

    return ld;
}


vec3f LodRender::evalDirectLOD(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                 SampleSequence& seq, float sigma2, float mint) const {
    const std::vector<Light*>& lights = scene->lights;
    vec3f ld = zero3f;
    LightSample ls = sampleLight(lights,next1f(seq));
//    ld = evalDirectLOD(rs, f, wo, next2f(seq), ls.light, sigma2, mint) / ls.pdf;
    if(isDelta(ls.light)) ld = evalDirectLOD(rs, f, wo, next2f(seq), ls.light, sigma2, mint) / ls.pdf;
    else ld = evalDirectMisLOD(rs, f, wo, next2f(seq), next1f(seq), next2f(seq), ls.light, sigma2, mint) / ls.pdf;
    return ld;
}

vec3f LodRender::evalIndirectLOD(const ray3f& ray, SampleSequence& seq, float sigma2, int depth) const {
    vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs; float mint;
    if(!traceRayLOD(ray, false, false, f, wo, le, rs, sigma2, mint) || !isvalid(rs)) { return le; }

    rs = reflectanceToLambert(rs); // use lambert since second bound
    vec3f ld = zero3f;
    ld = evalDirectLOD(rs, f, wo, seq, sigma2, mint);
    vec3f li = zero3f;

    if(depth < _opts.maxdepth) {
        ScatteringSample ss = sampleScattering(rs, f, wo,next1f(seq), next2f(seq));
        if (!ss.bsdf.iszero() && ss.pdf != 0) {
            vec3f scale = ss.bsdf * abs(dot(ss.ray.d, f.z)) / ss.pdf;
            if(!scale.iszero()) {
#ifdef UNIFORM_SOLIDANGEL
                float sigma = consts<float>::twopi / _opts.tau;
#else
                float sigma = (1.0f / ss.pdf) / _opts.tau;
#endif
                vec3f lr = evalIndirectLOD(ss.ray, seq, sigma, depth + 1);
                li = scale * lr;
            }
        }
    }
    return le + ld + li;
}

vec3f LodRender::evalIndirect(const ray3f& ray, SampleSequence& seq, int depth) const {
    vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, false, false, f, wo, le, rs) || !isvalid(rs)) { return le; }

    rs = reflectanceToLambert(rs); // use lambert since second bound
    vec3f ld = zero3f;
    ld = evalDirect(rs, f, wo, seq);

    vec3f li = zero3f;
    if(depth < _opts.maxdepth) {
        ScatteringSample ss = sampleScattering(rs, f, wo,next1f(seq), next2f(seq));
        if (!ss.bsdf.iszero() && ss.pdf != 0) {
            vec3f scale = ss.bsdf * abs(dot(ss.ray.d, f.z)) / ss.pdf;
            if(!scale.iszero()) {
                vec3f lr = evalIndirect(ss.ray, seq, depth + 1);
                li = scale * lr;
            }
        }
    }

    return le + ld;
}


void LodRender::tracePixel(int i, int j) {
    SampleSequence seq;
    vec3f l = zero3f;
    for (int s = 0; s < _opts.imageSampling.maxsamples; s++) {
        initSequence(seq, _imageSampling.generator, _imageSampling.rngs.at(i,j), i, j, s, 1);
        ray3f ray = sampleRay(i, j, seq);
        vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
        vec3f ld = zero3f; vec3f li = zero3f;

        if(traceRay(ray, true, true, f, wo, le, rs) && isvalid(rs)) {
            // direct
            ld = evalDirect(rs, f, wo, seq);
            // indirect
            ScatteringSample ss = sampleScattering(rs, f, wo,next1f(seq), next2f(seq));
            if (!ss.bsdf.iszero() && ss.pdf != 0) {
                vec3f scale = ss.bsdf * abs(dot(ss.ray.d, f.z)) / ss.pdf;
                if(!scale.iszero()) {
                    vec3f lr = evalIndirect(ss.ray, seq, 2);
                    li = scale * lr;
                }
            }
        }
        vec3f a = (le + ld + li);
        l += a;
    }
    l /= _opts.imageSampling.maxsamples;
    _imageSampling.accimage.at(i, j) += l;
    _imageSampling.accweight.at(i, j) += 1.0f;
    _imageSampling.image.at(i, j) = _imageSampling.accimage.at(i, j) /  _imageSampling.accweight.at(i, j);
}

void LodRender::tracePixelLOD(int i, int j) {
    SampleSequence seq;
    vec3f l = zero3f;
    for (int s = 0; s < _opts.imageSampling.maxsamples; s++) {
        initSequence(seq, _imageSampling.generator, _imageSampling.rngs.at(i,j), i, j, s, 1);
        ray3f ray = sampleRay(i, j, seq);
        vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
        vec3f ld = zero3f; vec3f li = zero3f;
        float mint;
        if(traceRayLOD(ray, true, true, f, wo, le, rs, _gamma, mint) && isvalid(rs)) {
            // direct
            ld = evalDirectLOD(rs, f, wo, seq, 0, mint);
            // indirect
            ScatteringSample ss = sampleScattering(rs, f, wo,next1f(seq), next2f(seq));
            if (!ss.bsdf.iszero() && ss.pdf != 0) {
                vec3f scale = ss.bsdf * abs(dot(ss.ray.d, f.z)) / ss.pdf;
                if(!scale.iszero()) {
#ifdef UNIFORM_SOLIDANGEL
                    float sigma = consts<float>::twopi / _opts.tau;
#else
                    float sigma = (1.0f / ss.pdf) / _opts.tau;
#endif
                    ss.ray.mint = mint;
                    vec3f lr = evalIndirectLOD(ss.ray, seq, sigma, 2);
                    li = scale * lr;
                }
            }
        }
        vec3f a = (le + ld + li);
        l += a;
    }
    l /= _opts.imageSampling.maxsamples;
    _imageSampling.accimage.at(i, j) += l;
    _imageSampling.accweight.at(i, j) += 1.0f;
    _imageSampling.image.at(i, j) = _imageSampling.accimage.at(i, j) /  _imageSampling.accweight.at(i, j);
}

void LodRender::traceBlock(int index) {
    const range2u &block = _imageBlocks[index];
    //   do things
    if(!_opts.general.parallel) {
        for (int j = block.min.y; j < block.max.y; j++) {
            for (int i = block.min.x; i < block.max.x; i++) {
                if (_opts.mode == NO_LOD) {
                    tracePixel(i, j);
                } else if (_opts.mode == LOD) {
                    tracePixelLOD(i, j);
                } else error("unknown mode");
            }
        }
    } else {
        tbb::parallel_for(tbb::blocked_range2d<int>(block.min.y, block.max.y, block.min.x, block.max.x),
                          [this](const tbb::blocked_range2d<int> &r) {
                              for (int j = r.rows().begin(); j < r.rows().end(); j++) {
                                  for (int i = r.cols().begin(); i < r.cols().end(); i++) {
                                      if (_opts.mode == NO_LOD) {
                                          tracePixel(i, j);
                                      } else if (_opts.mode == LOD) {
                                          tracePixelLOD(i, j);
                                      } else error("unknown mode");
                                  }
                              }
                          });
    }
}

bool LodRender::tracePass() {
    traceBlock(_blockIndex++);
    if (_blockIndex == _imageBlocks.size()) { _blockIndex = 0; return false; }
    return false;
}

Image<vec3f> LodRender::traceImage() {
    for (int i = 0; i < _imageBlocks.size(); i++) {
        tracePass();
    }
    return _imageSampling.image;
}

vec2i LodRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* LodRender::getImage() const {
    return &(_imageSampling.image);
}
