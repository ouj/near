#ifndef RENDER_SP_PHOTON_H
#define RENDER_SP_PHOTON_H

#include "render.h"
#include <integration/integration.h>
#include "photon_kdtree.h"

// opts ----------------
struct SpPhotonRenderOpts : RenderOpts {
    SpPhotonRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    int                 maxdepth = 5;
    int                 photonrays = 50000;
    int                 photonbatchsize = 2500;
    float               r1 = 0.5f;
    int                 generatortype = PixelSobolSampleGenerator::TYPEID;
    float               alpha = 2.0f / 3.0f;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
};

// Stochastic Progressive Photon Mapper
struct KdTree;
struct SpPhotonRender : Render {
    const static int TYPEID = 800603;
    SpPhotonRender();
    SpPhotonRender(const SpPhotonRenderOpts &opts);
    virtual ~SpPhotonRender();
    // operations ----------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    vec3f                       evalRadiance(const ray3f& ray, SampleSequence& seq, int depth);
    void                        buildPhotonmap(bool parallel);
    SpPhotonRenderOpts          _opts;
    int                         _photonbatches;
    KdTree                      _photonmap;
    float                       _rr;
    int                         _passes;
    ImageSamplingState          _imageSampling;
    vec3f                       _sceneCenter;
    float                       _sceneRadius;

};

#endif
