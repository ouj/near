#ifndef RENDER_ERPT_H
#define RENDER_ERPT_H

#include "render.h"
#include <integration/integration.h>
#include <tbb/tbb.h>
#include <tbb/mutex.h>
#include <integration/sequence.h>
#include "path_vertex.h"
#include "mlt.h"

// opts ------------------------
struct ErptRenderOpts : RenderOpts {
    ErptRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    int                 maxdepth = 16;
    int                 generatortype = RandomSampleGenerator::TYPEID;
    GeneralOpts         general;
    int                 bootstrapsamples = 100000;
    int                 samplechainlength = 500;
    int                 mcsamples = 16;
    int                 batchsamples = 1;
    int                 blocksize = 64;
    int                 mutations = 16;
    bool                mis = false;
    bool                diffusemultibounce = false;
};

typedef MLTSample ErptSample;
struct ErptRender : Render {
    const static int TYPEID = 800606;
    ErptRender();
    ErptRender(const ErptRenderOpts &opts);
    virtual ~ErptRender();

    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
protected:
    void                        samplePixel(int i, int j);
    void                        bootstrap();
    void                        equalDepositionFlow(Rng &rng, const ErptSample &x, const vec3f &l);

    ErptRenderOpts              _opts;
    SampleGenerator            *_generator;
    Image<vec3f>                _image;              // current rendering
    Image<spixelf>              _accImage;
    int                         _passes;
    float                       _ed;
    int                         _npixels;
    Image<Rng>                  _rngs;

};


#endif
