#ifndef RENDER_OPTIONS_H
#define RENDER_OPTIONS_H

namespace options {
    constexpr int maxthreads = 64;

    namespace lightcut {
        constexpr bool randrep = true;
    }

    namespace spphoton {
        constexpr float normal_disagree = 1e-3f;
        constexpr int photo_lookup_buf_size = 256;
        constexpr bool russianroulette = true;
        constexpr bool compute_init_radius = true;
    }

    namespace photon {
        constexpr float normal_disagree = 1e-6f;
        constexpr bool const_kernel = false;
        constexpr int photo_lookup_buf_size = 64;
        constexpr bool russianroulette = true;
    }

    namespace bidir {
        constexpr int  maxpathlength = 16;
        constexpr bool russianroulette = true;
        constexpr bool bidirectional = true;
    }

    namespace path {
        constexpr bool russianroulette = false;
    }

    namespace mlt {
        constexpr int  maxpathlength = 16;
        constexpr bool russianroulette = false;
    }

    namespace erpt {
        constexpr bool russianroulette = false;
    }
}

#endif
