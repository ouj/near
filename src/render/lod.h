#ifndef RENDER_RESAMP_H_
#define RENDER_RESAMP_H_
#include "render.h"
#include <integration/integration.h>
#include "vpl.h"
#include <integration/gather.h>
#include <accelerator/accelerator_streambvh.h>
#include <accelerator/accelerator_list.h>
#include <set>
#include <array>
#include <common/distribution.h>

// opts ------------------------
enum ResampleMode { NO_LOD, LOD };
struct LodRenderOpts : RenderOpts {
    LodRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    int                 maxdepth = 16;
    int                 reconsamples = 32;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
    float               tau = 512.0f;
    ResampleMode        mode = LOD;
};

struct LodRender : Render {
    const static int TYPEID = 800620;
    LodRender();
    LodRender(const LodRenderOpts &opts);
    virtual ~LodRender();

    // operations ------------------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    void                        traceBlock(int index);
    void                        tracePixel(int i, int j);
    void                        tracePixelLOD(int i, int j);
    ray3f                       sampleRay(int i, int j, SampleSequence &seq) const;
    vec3f                       evalDirect(const ReflectanceSample& rs, const frame3f& f,
                                           const vec3f& wo, SampleSequence& seq) const;
    vec3f                       evalDirect(const ReflectanceSample& rs, const frame3f& f,
                                           const vec3f& wo, const vec2f& ls, const Light* light) const;
    vec3f                       evalDirectMis(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                              const vec2f& ls, float bls, const vec2f& bas, const Light* light) const;
    vec3f                       evalDirectLOD(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                              SampleSequence& seq, float sigma2, float mint) const;
    vec3f                       evalDirectLOD(const ReflectanceSample& rs, const frame3f& f,
                                              const vec3f& wo, const vec2f& ls, const Light* light, float sigma2, float mint) const;
    vec3f                       evalDirectMisLOD(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                                                 const vec2f& ls, float bls, const vec2f& bas, const Light* light,
                                                 float sigma2, float mint) const;
    bool                        traceRay(const ray3f& ray, bool resolveEmission, bool resolveEnvironment,
                                         frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs) const;
    bool                        traceRayLOD(const ray3f& ray, bool resolveEmission, bool resolveEnvironment,
                                            frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs, float sigma2, float &mint) const;

    vec3f                       evalIndirect(const ray3f& ray, SampleSequence& seq, int depth) const;
    vec3f                       evalIndirectLOD(const ray3f& ray, SampleSequence& seq, float sigma2, int depth) const;

    int                         _blockIndex;
    LodRenderOpts               _opts;
    ImageSamplingState          _imageSampling;

    std::vector<range2u>        _imageBlocks;
    int                         _passes;
    float                       _gamma;

    StreamBvhAccelerator       *_accelerator = 0;
    ListAccelerator            *_lightAccelerator = 0;
};

#endif //_INTEGRATOR_MIGI_H_
