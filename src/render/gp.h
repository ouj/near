#ifndef _RENDER_GP_H_
#define _RENDER_GP_H_
#include <scene/scene.h>
#include <integration/gather.h>

struct Accelerator;
struct SampleGenerator;

std::vector<GatherPoint> sampleGatherPoints(const Camera* camera, const Accelerator *accelerator,
                                            const std::vector<Light*> &lights,
                                            SampleGenerator* generator, Image<Rng> &rngs,
                                            int samples, int width, int height, bool parallel);

#endif // _TRACE_GP_H_