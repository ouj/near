#include "mlt.h"
#include <integration/opts.h>
#include <integration/utils.h>
#include <common/pam.h>
#include "options.h"
#include <tbb/parallel_do.h>

MltRenderOpts::MltRenderOpts(): RenderOpts(MltRender::TYPEID) {
    rendertasknum = tbb::task_scheduler_init::default_num_threads();
}

jsonObject MltRenderOpts::print() const {
    jsonObject json;
    json["type"] = "mltrenderopts";
    json["maxdepth"] = maxdepth;
    json["general"] = printGeneralOpts(general);
    json["bootstrapsamples"] = bootstrapsamples;
    json["passes"] = passes;
    json["largesteprate"] = largesteprate;
    json["mutations"] = mutations;
    json["generatortype"] = printSampleGenerator(generatortype);
    json["rendertasknum"] = rendertasknum;
    json["maxconsecutiverejects"] = maxconsecutiverejects;
    json["mis"] = mis;
    json["diffusemultibounce"] = diffusemultibounce;
    return json;
}

void MltRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("mltrenderopts");
    maxdepth = jsonGet(json, "maxdepth");
    generatortype = parseSampleGenerator(jsonGet(json, "generatortype"));
    general = parseGeneralOpts(jsonGet(json,"general"));
    passes = jsonGet(json, "passes");
    largesteprate = jsonGet(json, "largesteprate");
    mutations = jsonGet(json, "mutations");
    mis = jsonGet(json, "mis");
    maxconsecutiverejects = jsonGet(json, "maxconsecutiverejects");
    bootstrapsamples = jsonGet(json, "bootstrapsamples");
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
    if(jsonHas(json, "rendertasknum")) rendertasknum = jsonGet(json, "rendertasknum");
}

MltRender::MltRender() : Render(TYPEID) {}

MltRender::MltRender(const MltRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

MltRender::~MltRender() { clearState(); }

bool MltRender::initState(const Scene* scene, const Accelerator *accelerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    int width = _opts.general.width;
    int height = _opts.general.height;
    _image.resize(width, height);
    _accImage.assign(width, height, makespixel(zero3f));
    _generator = initGenerator(_opts.generatortype, _tasks.size(), 1, _opts.mutations+1);
    bootstrap();
    _pass = 0;
    return true;
}

void MltRender::clearState() {}

static void largestep(Rng &rng, MLTSample *sample, int maxdepth,
                      float x, float y) {
    sample->cameraSample.image = vec2f(x,y) + rng.next2f();
    sample->cameraSample.lens = rng.next2f();

    for (int i = 0; i < maxdepth; ++i) {
        PathSample &cps = sample->pathSamples[i];
        cps.ar = rng.next2f();
        cps.lr = rng.next1f();
        cps.rrSample = rng.next1f();

        LightingSample &ls = sample->lightingSamples[i];
        ls.ss = rng.next2f();
        ls.ls = rng.next1f();
    }
}

static void smallstep(Rng &rng, const MLTSample &cs, MLTSample &ps,
                      int maxDepth, int width, int height) {
    ps.cameraSample.image.x = mutate(rng, cs.cameraSample.image.x, 0, width);
    ps.cameraSample.image.y = mutate(rng, cs.cameraSample.image.y, 0, height);
    ps.cameraSample.lens = mutate2(rng, cs.cameraSample.lens);
    // Apply small step mutation to camera, lighting, and light samples
    for (int i = 0; i < maxDepth; ++i) {
        const PathSample &ces = cs.pathSamples[i];
        PathSample &pes = ps.pathSamples[i];
        pes.ar = mutate2(rng, ces.ar);
        pes.lr = mutate(rng, ces.lr);
        pes.rrSample = mutate(rng, ces.rrSample);

        const LightingSample &cls = cs.lightingSamples[i];
        LightingSample &pls = ps.lightingSamples[i];
        pls.lr = mutate(rng, cls.lr);
        pls.ar = mutate2(rng, cls.ar);
        pls.ls = mutate(rng, cls.ls);
        pls.ss = mutate2(rng, cls.ss);
    }
}

static vec3f evalDirect(const ReflectanceSample& rs, const frame3f& f,
                 const vec3f& wo, const LightingSample& sample,
				 const std::vector<Light*>& lights, const Accelerator* accelerator,
                 bool mis) {
    LightSample ls = sampleLight(lights, sample.ls);

    if (!mis || isDelta(ls.light))
        return evalDirect(rs, f, wo, sample.ss, ls.light, accelerator) / ls.pdf;
    else {
        return evalDirectMis(rs, f, wo, sample.ss, sample.lr, sample.ar, ls.light, accelerator) / ls.pdf;
    }
}

vec3f MltRender::evalRadiance(const ray3f& ray, int depth,
                              const MLTSample &sample) {
    // trace ray
    vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, depth == 0, true, scene->lights, accelerator, f, wo, le, rs)) return le;
    if(!isvalid(rs)) return le;

    // convert reflectance to diffuse if needed
    ReflectanceSample brs = rs;
    if(_opts.diffusemultibounce && depth > 0) brs = reflectanceToLambert(brs);

    // direct
    vec3f ld = evalDirect(brs, f, wo, sample.lightingSamples[depth],
                          scene->lights, accelerator, _opts.mis);

    // indirect
    vec3f li = zero3f;
    if(depth < _opts.maxdepth-1) {
        ScatteringSample ss = sampleScattering(rs,f,wo,
                                               sample.pathSamples[depth].lr,
                                               sample.pathSamples[depth].ar);
        vec3f lr;
        if(!ss.bsdf.iszero()) {
            vec3f scale = ss.bsdf * dot(ss.ray.d,f.z) / ss.pdf;
            if (options::erpt::russianroulette) {
                float continueProb = min(1.0f, mean(scale));
                if (sample.pathSamples[depth].rrSample > continueProb) {
                    lr = zero3f;
                } else {
                    lr = evalRadiance(ss.ray, depth+1, sample);
                    lr /= continueProb;
                }
            } else {
                lr = evalRadiance(ss.ray, depth+1, sample);
            }
            li = scale * lr;
        }
    }
    // done
    return le+ld+li;
}

vec3f MltRender::evalRadiance(const MLTSample &mltSample) {
    vec2f is = vec2f(mltSample.cameraSample.image.x/_opts.general.width,
                     mltSample.cameraSample.image.y/_opts.general.height);
    ray3f r = sampleCamera(scene->cameras[0], is, mltSample.cameraSample.lens);
    return evalRadiance(r, 0, mltSample);
}

void MltRender::bootstrap() {
    _npixels = _opts.general.width * _opts.general.height;
    _bootstrapSamples.resize(_opts.bootstrapsamples);
    Rng rng;
    float energy = 0.0f;
    std::vector<MLTSample> samples(_opts.bootstrapsamples);
    std::vector<vec3f> Ls(_opts.bootstrapsamples);
    std::vector<float> Is(_opts.bootstrapsamples);

    MLTSample sample(_opts.maxdepth);
    for (int i = 0; i < _opts.bootstrapsamples; i++) {
        float x = lerp(0u, _opts.general.width, rng.next1f());
        float y = lerp(0u, _opts.general.height, rng.next1f());
        largestep(rng, &sample, _opts.maxdepth, x, y);
        vec3f L = evalRadiance(sample);
        float I = L.luminance();
        _bootstrapSamples.at(i) = I;
        energy += I;
        samples.at(i) = sample;
        Ls.at(i) = L;
        Is.at(i) = I;
    }
    _b = energy / (_opts.bootstrapsamples);
    message_va("bootstraps samples %d", _opts.bootstrapsamples);
    message_va("scaling factor b = %f", _b);

    Distribution1D<float> dist(Is);
    _tasks.resize(_opts.rendertasknum);
    std::vector<Rng> rngs = initRngs(_tasks.size());
    for (int t = 0; t < _tasks.size(); t++) {
        float pdf;
        int index = dist.sample(rng.next1f(), &pdf);
        _tasks[t].initTask(samples[index], Ls[index], Is[index], rngs.at(t), this);
    }
    message_va("sample per pixel per pass: %d",
               _tasks.size() * _opts.mutations / _npixels);
}

void MltRenderTask::initTask(const MLTSample &initSample,
                             const vec3f &initL, float initI, Rng &r, MltRender *render) {
    this->render = render;
    MltRenderOpts &opts = render->_opts;
    rng = r;
    samples[0] = initSample;
    samples[1] = MLTSample(opts.maxdepth);
    L[0] = initL;
    I[0] = initI;
    b = render->_b;

    pixelOffset = 0;
    int totalPixels = opts.general.width * opts.general.height;
    shuffledPixelIndices.resize(totalPixels);
    for (int i = 0; i < totalPixels ; i++) shuffledPixelIndices.at(i) = i;
    scramble(shuffledPixelIndices, rng);
}


inline void accumulate(vec3f &v, const vec3f &a, tbb::spin_mutex &m) {
    tbb::spin_mutex::scoped_lock l(m);
    v += a;
};

void MltRenderTask::run() {
    int         current = 0;
    int         proposed = 1;
    int         consecutiveRejects = 0;
    MltRenderOpts &opts = render->_opts;

    for (int i = 0; i < opts.mutations; i++) {
        if (i % opts.largesteprate == 0) {
            int index = shuffledPixelIndices[pixelOffset++];
            if (pixelOffset == shuffledPixelIndices.size()) pixelOffset = 0;
                int x = index / opts.general.width;
                int y = index % opts.general.height;
                largestep(rng, &samples[proposed], opts.maxdepth, x, y);
        } else {
            smallstep(rng, samples[current], samples[proposed], opts.maxdepth,
                      opts.general.width, opts.general.height);
        }
        L[proposed] = render->evalRadiance(samples[proposed]);
        I[proposed] = L[proposed].luminance();
        float a = min(1.0f, I[proposed] / I[current]);

        if (I[current] > 0.0f) {
            int px = samples[current].cameraSample.image.x;
            int py = samples[current].cameraSample.image.y;
            vec3f contrib = L[current] * b / I[current];
            render->_accImage.at(px, py) += (1 - a) * contrib;
        }
        if (I[proposed] > 0.0f) {
            int px = samples[proposed].cameraSample.image.x;
            int py = samples[proposed].cameraSample.image.y;
            vec3f contrib =  L[proposed] * b / I[proposed];
            render->_accImage.at(px, py) += a * contrib;
        }

        // Randomly accept proposed path mutation (or not)
        if (consecutiveRejects >= opts.maxconsecutiverejects ||
            rng.next1f() < a) {
            current ^= 1;
            proposed ^= 1;
            consecutiveRejects = 0;
        } else {
            consecutiveRejects++;
        }
    }
}

bool MltRender::tracePass() {
    if (_opts.general.parallel) {
        auto func = [this](const tbb::blocked_range<int>& r) {
            for(int i = r.begin(); i < r.end(); i++) {
                _tasks[i].run();
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0, _tasks.size()),
                          func, tbb::simple_partitioner());
    } else {
        for (int t = 0; t < _tasks.size(); t++) {
            _tasks[t].run();
        }
    }

    int totalSamples = (_pass + 1) * _tasks.size() * _opts.mutations;
    float pixelSamples = (float)totalSamples / _npixels;
    for (int j = 0; j < _opts.general.height; j++) {
        for (int i = 0; i < _opts.general.width; i++) {
            _image.at(i, j) = _accImage.at(i, j) / pixelSamples;
        }
    }
    _pass++;
    return false;
}

Image<vec3f> MltRender::traceImage() {
    for (int s = 0; s < _opts.passes; s++) {
        tracePass();
    }
    return _image;
}

vec2i MltRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* MltRender::getImage() const {
    return &(_image);
}
