#include "gather_kdtree.h"
#include <tbb/tbb.h>
#include <common/array.h>
#include <common/range.h>

inline GatherTreeNode __makeLeaf() {
    GatherTreeNode leaf;
    leaf.split = 0;
    leaf.axis = -1;
    leaf.leftIndex = std::numeric_limits<int>::max();
    leaf.rightIndex = std::numeric_limits<int>::max();
    return leaf;
}

inline GatherTreeNode __makeBranch(float split, int axis) {
    GatherTreeNode branch;
    branch.split = split;
    branch.axis = axis;
    branch.leftIndex = std::numeric_limits<int>::max();
    branch.rightIndex = std::numeric_limits<int>::max();
    return branch;
}

struct CompareNode {
    CompareNode(int a) { axis = a; }
    int axis;
    bool operator()(const GatherPoint *d1, const GatherPoint *d2) const {
        return d1->f.o[axis] == d2->f.o[axis] ? (d1 < d2) :
        d1->f.o[axis] < d2->f.o[axis];
    }
};

void __recursiveBuild(std::vector<GatherPoint> &data, std::vector<GatherTreeNode> &nodes, int nodeNum,
                      const GatherPoint** buildNodes, int size) {
    if(size <= 1) {
        nodes[nodeNum] = __makeLeaf();
        data[nodeNum] = **buildNodes;
        return;
    }
    
    range3f bound = invalidrange3f;
    for(int p = 0; p < size; p++) bound.grow(buildNodes[p]->f.o);
    int splitAxis = bound.extent().max_component_index();
    int mid = static_cast<int>(size * 0.5f);
    std::nth_element(buildNodes, buildNodes + mid, buildNodes + size, CompareNode(splitAxis));
    
    nodes[nodeNum] = __makeBranch(buildNodes[mid]->f.o[splitAxis], splitAxis);
    data[nodeNum] = *buildNodes[mid];
    
    if (0 < mid) {
        nodes[nodeNum].leftIndex = nodeNum + 1;
        __recursiveBuild(data, nodes, nodes[nodeNum].leftIndex, buildNodes, mid);
    }
    if (mid+1 < size) {
        nodes[nodeNum].rightIndex = nodeNum + mid + 1;
        __recursiveBuild(data, nodes, nodes[nodeNum].rightIndex, buildNodes + mid + 1, size - mid - 1);
    }
    //error(nodes[nodeNum].hasLeft || nodes[nodeNum].rightIndex < nodes.size(), "error kdtree");
}

bool GatherTree::buildKdTree(GatherPoint* data, int size) {
    if(size == 0 || data == 0)
        return false;
    
    this->nodes.resize(size);
    this->data.resize(size);
    std::vector<const GatherPoint*> buildNodes(size);
    for (int i = 0; i < size; i++)
        buildNodes[i] = &data[i];
    
    __recursiveBuild(this->data, this->nodes, 0, buildNodes.data(), size);
    return true;
}


struct KdTreeBuildTask : tbb::task {
    KdTreeBuildTask(std::vector<GatherPoint> &d, std::vector<GatherTreeNode> &ns, int nn, const GatherPoint **bn, int s)
    : data(d), nodes(ns), nodeNum(nn), buildNodes(bn), size(s) {}
    
    tbb::task* execute() {
        if(size < 1024) {
            __recursiveBuild(data, nodes, nodeNum, buildNodes, size);
        } else {
            range3f bound = invalidrange3f;
            for(int p = 0; p < size; p++)
                bound.grow(buildNodes[p]->f.o);
            
            int splitAxis = bound.extent().max_component_index();
            int mid = static_cast<int>(size * 0.5f);
            //_sortAlongAxis(splitAxis, buildNodes, size);
            std::nth_element(buildNodes, buildNodes + mid, buildNodes + size, CompareNode(splitAxis));
            
            nodes[nodeNum] = __makeBranch(buildNodes[mid]->f.o[splitAxis], splitAxis);
            data[nodeNum] = *buildNodes[mid];
            
            if (0 < mid && mid+1 < size) {
                tbb::empty_task& c = *new( allocate_continuation() ) tbb::empty_task();
                nodes[nodeNum].leftIndex = nodeNum + 1;
                KdTreeBuildTask& a = *new( c.tbb::task::allocate_child() ) KdTreeBuildTask(data, nodes, nodes[nodeNum].leftIndex, buildNodes, mid);
                nodes[nodeNum].rightIndex = nodeNum + mid + 1;
                KdTreeBuildTask& b = *new( c.tbb::task::allocate_child() ) KdTreeBuildTask(data, nodes, nodes[nodeNum].rightIndex, buildNodes+mid+1, size-mid-1);
                c.set_ref_count(2);
                spawn(a);
                spawn(b);
                return 0;
            } else {
                error("invalid split");
            }
        }
        return 0;
    }
    
    std::vector<GatherPoint> &data;
    std::vector<GatherTreeNode> &nodes;
    int nodeNum;
    const GatherPoint** buildNodes;
    int size;
};


bool GatherTree::parallelBuildKdTree(GatherPoint* data, int size) {
    if(size == 0 || data == 0)
        return false;
    
    this->nodes.resize(size);
    this->data.resize(size);
    std::vector<const GatherPoint*> buildNodes(size);
    for (int i = 0; i < size; i++)
        buildNodes[i] = &data[i];
    
    KdTreeBuildTask& root = *new(tbb::task::allocate_root()) KdTreeBuildTask(this->data, this->nodes, 0, buildNodes.data(), size);
    tbb::task::spawn_root_and_wait(root);
    return true;
}



