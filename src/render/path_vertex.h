#ifndef RENDER_PATH_VERTEX_H
#define RENDER_PATH_VERTEX_H

#include <integration/integration.h>
#include <accelerator/accelerator.h>

struct PathVertex {
    vec3f               wPrev;
    vec3f               wNext;
    frame3f             f;
    ReflectanceSample   rs;
    EmissionSample      es;
    vec3f               alpha;
    float               pdf;    // approximate pdf wrt to area
};

template<int maxpathlenght, bool russianroulette>
int generatePath(std::array<PathVertex, maxpathlenght> &path,
                 const Accelerator *accelerator, SampleSequence& seq,
                 const ray3f &r, const vec3f &a, float p, int maxdepth, 
                 ray3f *escRay, vec3f *escAlpha) {
    ray3f ray = r;
    vec3f alpha = a;
    if (escAlpha) *escAlpha = zero3f;
    int length = 0;
    float pdf = p;
    while (length < maxdepth) {
        intersection3f intersection;
        PathVertex &v = path[length];
        if (!accelerator->intersectFirst(ray, intersection)) {
            if (escAlpha) *escAlpha = alpha;
            if (escRay)   *escRay = ray;
            break; 
        }
        
        v.alpha = alpha;
        v.f = intersection.f;
        v.rs = sampleReflectance(intersection.m, intersection.st);
        v.es = sampleEmission(intersection.m, intersection.st);
        
        ScatteringSample ss = sampleScattering(v.rs, intersection.f, -ray.d, 
                                               next1f(seq), next2f(seq));
        v.wPrev = -ray.d;
        v.wNext = ss.ray.d;
        
        pdf *= abs(dot(-ray.d, intersection.f.z)) / distanceSqr(intersection.f.o, ray.o);
        v.pdf = pdf;
        
        if (!isvalid(ss)) return length + 1;
        
        vec3f scale = ss.bsdf * abs(dot(ss.ray.d, intersection.f.z)) / ss.pdf;
        if (russianroulette) {
            float continueProb = min(1.0f, mean(scale));
            if (seq.rng->next1f() > continueProb) return length + 1;
            alpha *= scale / continueProb;
        } else 
            alpha *= scale;
        pdf *= ss.pdf;
        ray = ss.ray;
        length++;
    }
    return length;
}


#endif
