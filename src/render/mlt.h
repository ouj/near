#ifndef RENDER_MLT_H
#define RENDER_MLT_H

#include "render.h"
#include <integration/integration.h>
#include <tbb/tbb.h>
#include <tbb/mutex.h>
#include <integration/sequence.h>
#include "path_vertex.h"
#include "spixel.h"
#include "options.h"

struct MltRenderOpts : RenderOpts {
    MltRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    int                 maxdepth = 16;
    int                 generatortype = RandomSampleGenerator::TYPEID;
    GeneralOpts         general;
    int                 bootstrapsamples = 100000;
    int                 largesteprate = 64;
    int                 maxconsecutiverejects = 64;
    int                 passes = 256;
    int                 mutations = 512 * 512;
    int                 rendertasknum;
    bool                diffusemultibounce = false;
    bool                mis = true;
};

struct PathSample {
    vec2f ar;
    float lr;
    float rrSample;
};

struct LightingSample {
    vec2f ar;
    float lr;
    vec2f ss;
    float ls;
};

struct CameraSample {
    vec2f image;
    vec2f lens;
};

struct MLTSample {
    MLTSample() {}
    MLTSample(int maxLength) { error_if(maxLength > options::mlt::maxpathlength, "maxleng exceed limit."); }
    CameraSample cameraSample;
    std::array<PathSample, options::mlt::maxpathlength>      pathSamples;
    std::array<LightingSample, options::mlt::maxpathlength>  lightingSamples;
};

inline float mutate(Rng &rng, float v, float min = 0.f, float max = 1.f) {
    if (min == max) { return min; }
    error_if_not(min < max, "incorrect mutate interval");
    float a = 1.f / 1024.f, b = 1.f / 64.f;
    static const float logRatio = -logf(b/a);
    float delta = (max - min) * b * expf(logRatio * rng.next1f());
    float m = v;
    if (rng.next1f() < 0.5f) {
        m += delta;
        if (m >= max) m = min + (m - max);
    }
    else {
        m -= delta;
        if (m < min) m = max - (min - m);
    }
    if (m < min || m >= max) m = min;
    return m;
}

inline vec2f mutate2(Rng &rng, const vec2f &v) {
    return vec2f(mutate(rng, v.x), mutate(rng, v.y));
}

struct MltRenderTask;
struct MltRender : Render {
    const static int TYPEID = 800607;
    MltRender();
    MltRender(const MltRenderOpts &opts);
    virtual ~MltRender();
    // operations ------------------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
protected:
    friend struct MltRenderTask;
    vec3f                       evalRadiance(const MLTSample &mltSample);
    vec3f                       evalRadiance(const ray3f& ray, int depth,
                                             const MLTSample &sample);
    void                        bootstrap();
    MltRenderOpts               _opts;
    SampleGenerator            *_generator;
    Image<vec3f>                _image;              // current rendering
    Image<spixelf>              _accImage;
    int                         _pass;
    float                       _b;
    std::vector<MltRenderTask>  _tasks;
    std::vector<float>          _bootstrapSamples;
    int                         _npixels;
};

struct MltRenderTask {
    void                initTask(const MLTSample &initSample, const vec3f &initL,
                                 float initI, Rng &r, MltRender *render);
    void                run();

    MltRender          *render;
    std::vector<int>    shuffledPixelIndices;
    int                 pixelOffset;
    Rng                 rng;
    MLTSample           samples[2];
    vec3f               L[2];
    float               I[2];
    float               b;
};

#endif
