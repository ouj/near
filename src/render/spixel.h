#ifndef RENDER_SPIXEL_H_
#define RENDER_SPIXEL_H_
#include <common/vec.h>
#include <tbb/spin_mutex.h>

using tbb::spin_mutex;

template<typename R>
struct spixel {
    vec3<R>     v;
    spin_mutex  m;
};

template<typename R>
spixel<R> makespixel(const vec3<R> &v) {
    spixel<R> s;
    s.v = v;
    return s;
}

template<typename R>
inline spixel<R> makespixel(const R& x, const R& y, const R& z) {
    spixel<R> sp; sp.v = vec3<R>(x, y, z);
    return sp;
}

template <typename R>
inline spixel<R>& operator+=(spixel<R>& a, const vec3<R>& b) {
    spin_mutex::scoped_lock l(a.m);
    a.v += b;
    return a;
}

template <typename R>
inline spixel<R>& operator-=(spixel<R>& a, const vec3<R>& b) {
    spin_mutex::scoped_lock l(a.m);
    a.v -= b;
    return a;
}

template <typename R>
inline spixel<R>& operator*=(spixel<R>& a, const R& b) {
    spin_mutex::scoped_lock l(a.m);
    a.v *= b;
    return a;
}

template <typename R>
inline spixel<R>& operator*=(spixel<R>& a, const vec3<R>& b) {
    spin_mutex::scoped_lock l(a.m);
    a.v *= b;
    return a;
}

template <typename R>
inline spixel<R>& operator/=(spixel<R>& a, const R& b) {
    spin_mutex::scoped_lock l(a.m);
	a.v /= b;
    return a;
}

template <typename R>
inline spixel<R>& operator/=(spixel<R>& a, const vec3<R>& b) {
    spin_mutex::scoped_lock l(a.m);
	a.v /= b;
    return a;
}

template <typename R>
inline vec3<R> operator+(const spixel<R>& a, const vec3<R>& b) {
	return a.v + b;
}

template <typename R>
inline vec3<R> operator-(const spixel<R>& a, const vec3<R>& b) {
	return a.v - b;
}

template <typename R>
inline vec3<R> operator*(const spixel<R>& a, const vec3<R>& b) {
	return a.v * b;
}

template <typename R>
inline vec3<R> operator*(const spixel<R>& a, const R& b) {
    return a.v * b;
}

template <typename R>
inline vec3<R> operator*(const R& b, const spixel<R>& a) {
    return a.v * b;
}


template <typename R>
inline vec3<R> operator/(const spixel<R>& a, const vec3<R>& b) {
	return a.v / b;
}

template <typename R>
inline vec3<R> operator/(const spixel<R>& a, const R& b) {
	return a.v / b;
}

template <typename R>
inline vec3<R> operator/(const R& a, const spixel<R>& b) {
	return a.v / b;
}

typedef spixel<float>  spixelf;
typedef spixel<double> spixeld;
#endif // RENDER_SPIXEL_H_