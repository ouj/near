#include "render.h"
#include "path.h"
#include "debug.h"
#include "photon.h"
#include "spphoton.h"
#include "igi.h"
#include "bidir.h"
#include "erpt.h"
#include "mlt.h"
#include "lightcut.h"
#include "lod.h"
#include <common/stdheader.h>

RenderOpts* parseRenderOpts(const jsonObject& json) {
    RenderOpts* opts = nullptr;
    std::string type = jsonGet(json, "type").as_string();
    if(type == "debugrenderopts") {
        opts = new DebugRenderOpts();
    } else if(type == "pathrenderopts") {
        opts = new PathRenderOpts();
    } else if(type == "photonrenderopts") {
        opts = new PhotonRenderOpts();
    } else if(type == "spphotonrenderopts") {
        opts = new SpPhotonRenderOpts();
    } else if(type == "igirenderopts") {
        opts = new IgiRenderOpts();
    } else if(type == "bidirrenderopts") {
        opts = new BidirRenderOpts();
    } else if(type == "erptrenderopts") {
        opts = new ErptRenderOpts();
    } else if(type == "mltrenderopts") {
        opts = new MltRenderOpts();
    } else if(type == "lightcutrenderopts") {
        opts = new LightcutRenderOpts();
    } else if(type == "lodrenderopts") {
        opts = new LodRenderOpts();
    } else error("invalid renderer type");
    if (opts) opts->parse(json);
    return opts;
}

Render* createRender(const RenderOpts *opts) {
    switch (opts->type) {
        case DebugRender::TYPEID:
            return new DebugRender(*(DebugRenderOpts*)opts);
        case PathRender::TYPEID:
            return new PathRender(*(PathRenderOpts*)opts);
        case PhotonRender::TYPEID:
            return new PhotonRender(*(PhotonRenderOpts*)opts);
        case SpPhotonRender::TYPEID:
            return new SpPhotonRender(*(SpPhotonRenderOpts*)opts);
        case IgiRender::TYPEID:
            return new IgiRender(*(IgiRenderOpts*)opts);
        case BidirRender::TYPEID:
            return new BidirRender(*(BidirRenderOpts*)opts);
        case ErptRender::TYPEID:
            return new ErptRender(*(ErptRenderOpts*)opts);
        case MltRender::TYPEID:
            return new MltRender(*(MltRenderOpts*)opts);
        case LightcutRender::TYPEID:
            return new LightcutRender(*(LightcutRenderOpts*)opts);
        case LodRender::TYPEID:
            return new LodRender(*(LodRenderOpts*)opts);
        default:
            error("invalid renderer type");
    }
    return nullptr;
}

Render* createRender(const jsonObject& json) {
    std::unique_ptr<RenderOpts> opts(parseRenderOpts(json));
    return createRender(opts.get());
}

Render* createDefaultRender() { return new PathRender(); }
