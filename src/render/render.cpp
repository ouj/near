#include "render.h"
#include <common/ray.h>
#include <scene/scene.h>
#include <integration/integration.h>

bool Render::initState(const Scene *sc, const Accelerator *acc) {
    scene = sc;
    accelerator = acc;
    ray3f::epsilon = scene->settings.rayepsilon;
    initLightSampling(scene->lights);
    return true;
}
