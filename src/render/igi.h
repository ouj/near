#ifndef RENDER_IGI_H_
#define RENDER_IGI_H_
#include "render.h"
#include <integration/integration.h>
#include "vpl.h"

// opts ------------------------
struct IgiRenderOpts : RenderOpts {
    IgiRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    TraceDirectMode     directmode = TDM_ALL;
    bool                diffusemultibounce = false;
    int                 arealightsamples = 1024;
    int                 envlightsamples = 8192;
    int                 indirectlightsamples = 500000;
    int                 indirectlightbatchsize = 5000;
    int                 maxdepth = 16;
    bool                directvplight = true;
    int                 generatortype = PixelSobolSampleGenerator::TYPEID;
    float               geoclamp = 0.1f;
    ImageSamplingOpts   imageSampling;
    GeneralOpts         general;
};


struct IgiRender : Render {
    const static int TYPEID = 800604;
    IgiRender();
    IgiRender(const IgiRenderOpts &opts);
    virtual ~IgiRender();
    // operations ------------------
    virtual bool                initState(const Scene *scene, const Accelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    void                        generateLights();
    vec3f                       evalRadiance(const ray3f& ray, SampleSequence& seq);

    IgiRenderOpts                   _opts;
    ImageSamplingState              _imageSampling;
    std::vector<VpLight*>           _directLights;
    std::vector<OrientedVpLight>    _indirectLights;

    std::vector<OrientedVpLight>    _ortLight;
    std::vector<DirectVpLight>      _dirLight;
    std::vector<PointVpLight>       _ptLight;

    float                           _r2min;
    SampleGenerator                *_generator;

};

#endif //_INTEGRATOR_MIGI_H_