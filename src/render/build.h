#ifndef BUILD_H
#define BUILD_H
#include "render.h"

RenderOpts* parseRenderOpts(const jsonObject& json);

Render* createRender(const RenderOpts *opts);
Render* createRender(const jsonObject& json);

Render* createDefaultRender();

#endif // BUILD_H
