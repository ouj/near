#ifndef _RENDER_KD_TREE_H_
#define _RENDER_KD_TREE_H_
#include <common/vec.h>
#include <common/array.h>
#include <algorithm>
#include <common/geom.h>
#include <integration/gather.h>

struct GatherTreeNode {
    float       split;
    int         axis;
    int         leftIndex;
    int         rightIndex;
};

struct GatherTree {
    std::vector<GatherTreeNode>  nodes;
    std::vector<GatherPoint> data;
    bool buildKdTree(GatherPoint* data, int size);
    bool parallelBuildKdTree(GatherPoint* data, int size);
    template<typename LookupProc>
    void lookup(const vec3f &p, LookupProc &proc, float &maxDistSquared);
};

template <typename LookupProc>
void __privateLookup(const GatherTree *tree, int nodeNum, const vec3f &p,
                     LookupProc &process, float &maxDistSquared) {
    const GatherTreeNode *node = &(tree->nodes[nodeNum]);
    // Process kd-tree node's children
    int axis = node->axis;
    if (axis != -1) {
        float px = p[axis];
        float d = px - node->split;
        float dist2 = d * d;

        if (px <= node->split) {
            if (node->leftIndex < tree->nodes.size())
                __privateLookup(tree, node->leftIndex, p, process, maxDistSquared);
            if (dist2 < maxDistSquared && node->rightIndex < tree->nodes.size())
                __privateLookup(tree, node->rightIndex, p, process, maxDistSquared);
        }
        else {
            if (node->rightIndex < tree->nodes.size())
                __privateLookup(tree, node->rightIndex, p, process, maxDistSquared);
            if (dist2 < maxDistSquared && node->leftIndex < tree->nodes.size())
                __privateLookup(tree, node->leftIndex, p, process, maxDistSquared);
        }
    }

    // Hand kd-tree node to processing function
    float dist2 = distanceSqr(tree->data[nodeNum].f.o, p);
    if (dist2 < maxDistSquared)
        process(p, tree->data[nodeNum], dist2, maxDistSquared);
}

template <typename LookupProc>
void GatherTree::lookup(const vec3f &p, LookupProc &proc, float &maxDistSquared) {
    __privateLookup(this, 0, p, proc, maxDistSquared);
}

struct CloseGather {
    CloseGather() : gp(0), distanceSquared(0) {}
    CloseGather(const GatherPoint* g, float md2 = std::numeric_limits<float>::max())
                : gp(g), distanceSquared(md2) {}
    const GatherPoint *gp;
    float distanceSquared;
};

inline bool operator<(const CloseGather &p1, const CloseGather &p2) {
    return p1.distanceSquared == p2.distanceSquared ?
    (p1.gp < p2.gp) : (p1.distanceSquared < p2.distanceSquared);
}

template<typename GATHER>
struct GatherProcess {
    GatherProcess(unsigned int mp, CloseGather *buf) : photons(buf), nlookup(mp), nfound(0) {} ;
    void operator()(const vec3f &p, const GATHER &gather, float dist2,
                    float &maxDistSquared) {
        if (nfound < nlookup) {
            // Add photon to unordered array of photons
            photons[nfound++] = CloseGather(&gather, dist2);
            if (nfound == nlookup) {
                std::make_heap(&photons[0], &photons[nlookup]);
                maxDistSquared = photons[0].distanceSquared;
            }
        }
        else {
            // Remove most distant photon from heap and add new photon
            std::pop_heap(&photons[0], &photons[nlookup]);
            photons[nlookup-1] = CloseGather(&gather, dist2);
            std::push_heap(&photons[0], &photons[nlookup]);
            maxDistSquared = photons[0].distanceSquared;
        }
    }
    CloseGather *photons;
    unsigned int nlookup;
    unsigned int nfound;
};

#endif // _KD_TREE_H_