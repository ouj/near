#ifndef _LIGHTTREE_H_
#define _LIGHTTREE_H_

#include "options.h"
#include "vpl.h"
#include <tbb/tbb.h>
#include <common/cone.h>

// Light Tree data structure
template <typename R>
struct vec6 {
    R x, y, z, w, s, t;
};

template <typename R>
inline vec6<R> makevec6(const R x, const R y, const R z, 
                        const R w, const R s, const R t) {
    vec6<R> v; v.x = x; v.y= y; v.z = z; v.w = w; v.s = s; v.t = t; return v;
}


template <typename R>
inline vec6<R> operator-(const vec6<R>& a, const vec6<R>& b) {
    return makevec6<R>(a.x-b.x,a.y-b.y,a.z-b.z,a.w-b.w,a.s-b.s,a.t-b.t);
}

template <typename R>
inline vec6<R> min_component(const vec6<R>& a, const vec6<R>& b) {
    return makevec6<R>(min(a.x,b.x),min(a.y,b.y),min(a.z,b.z),
                       min(a.w,b.w),min(a.s,b.s),min(a.t,b.t));
}

template <typename R>
inline vec6<R> max_component(const vec6<R>& a, const vec6<R>& b) {
    return makevec6<R>(max(a.x,b.x),max(a.y,b.y),max(a.z,b.z),
                       max(a.w,b.w),max(a.s,b.s),max(a.t,b.t));
}

template <typename R>
inline int max_component_index(const vec6<R>& v) {
    int a, b, c;
    float av, bv, cv;
    a = (v.x > v.y) ? 0 : 1;
    av = (v.x > v.y) ? v.x : v.y;
    b = (v.z > v.w) ? 2 : 3;
    bv = (v.z > v.w) ? v.z : v.w;
    c = (v.s > v.t) ? 4 : 5;
    cv = (v.s > v.t) ? v.s : v.t;
    if (av > bv && av > cv) return a;
    else if(bv > av && bv > cv) return b;
    else return c;
}

typedef vec6<float> vec6f;
typedef vec6<double> vec6d;

//range6
template <typename R>
struct range6 {
    vec6<R> min, max;
};

template <typename R>
inline bool isvalid(const range6<R>& r) { 
    return r.max.x >= r.min.x && r.max.y >= r.min.y && r.max.z >= r.min.z && 
    r.max.w >= r.min.w && r.max.s >= r.min.s && r.max.t >= r.min.t; 
} 

template <typename R>
inline range6<R> makerange6(const vec6<R>& min, const vec6<R>& max) {
    range6<R> r; r.min = min; r.max = max; return r;
}

template <typename R>
inline range6<R> invalidrange6() {
    R m = numeric_limits<R>::max();
    return makerange6<R>(makevec6<R>(m,m,m,m,m,m),
                         makevec6<R>(-m,-m,-m,-m,-m,-m));
}

template<typename R>
inline range6<R> runion(const range6<R>& range, const vec6<R> v) {
    return makerange6<R>(min_component(range.min,v),max_component(range.max,v));
}

template<typename R>
inline range6<R> runion(const range6<R>& range, const range6<R>& r) {
    return makerange6<R>(min_component(range.min,r.min),max_component(range.max,r.max));
}

template<typename R>
inline vec6<R> extent(const range6<R>& range) {
    error_if_not_va(isvalid(range), "invalid range");
    return ( range.max - range.min );
}

typedef range6<float> range6f;
typedef range6<double> range6d;
const range6f invalidrange6f = invalidrange6<float>();
const range6d invalidrange6d = invalidrange6<double>();

//cone



// Light Tree
struct PtNode {
    typedef PointVpLight lighttype;
    vec3f           le;
    range3f         bbox;
    PointVpLight    *light;
    int             leftIndex;
    int             rightIndex;
};

struct DirNode {
    typedef DirectVpLight lighttype;
    vec3f           le;
    range3f         bbox;
    DirectVpLight   *light;
    int             leftIndex;
    int             rightIndex;
};

struct OrtNode {
    typedef OrientedVpLight lighttype;
    vec3f           le; 
    range3f         bbox;
    cone3f          cone;
    OrientedVpLight *light;
    int             leftIndex;
    int             rightIndex;
};

inline vec6f getPoint(const PointVpLight &light, float normalScale) {
    return makevec6(light.position.x, light.position.y, light.position.z, 
                    0.0f, 0.0f, 0.0f);
}


inline vec6f getPoint(const DirectVpLight &light, float normalScale) {
    return makevec6(0.0f, 0.0f, 0.0f,
                    light.direction.x, light.direction.y, light.direction.z);
}


inline vec6f getPoint(const OrientedVpLight &light, float normalScale) {
    return makevec6(light.position.x, light.position.y, light.position.z, 
                    light.normal.x * normalScale, 
                    light.normal.y * normalScale, 
                    light.normal.z * normalScale);
}

inline PtNode makeLeaf(PointVpLight* light) {
    PtNode node;
    node.le = light->intensity;
    node.bbox = range3f(light->position, light->position);
    node.light = light;
    node.leftIndex = std::numeric_limits<int>::max();
    node.rightIndex = std::numeric_limits<int>::max();
    return node;
}

inline OrtNode makeLeaf(OrientedVpLight* light) {
    OrtNode node;
    node.le = light->irradiance;
    node.bbox = range3f(light->position, light->position);
    node.cone = cone3f(light->normal);
    node.light = light;
    node.leftIndex = std::numeric_limits<int>::max();
    node.rightIndex = std::numeric_limits<int>::max();
    return node;
}

inline DirNode makeLeaf(DirectVpLight* light) {
    DirNode node;
    node.le = light->radiance;
    node.bbox = range3f(light->direction, light->direction);
    node.light = light;
    node.leftIndex = std::numeric_limits<int>::max();
    node.rightIndex = std::numeric_limits<int>::max();
    return node;
}

inline PtNode makeBranch(const std::vector<PtNode> &nodes, int l, int r, float random) {
    PtNode branch;
    const PtNode &left = nodes[l];
    const PtNode &right = nodes[r];
    branch.le = left.le + right.le;
    branch.bbox = runion(left.bbox, right.bbox);
    if (random <= mean(left.le) / mean(branch.le))
        branch.light = left.light;
    else 
        branch.light = right.light;
    branch.leftIndex = l;
    branch.rightIndex = r;
    return branch;
}

inline DirNode makeBranch(const std::vector<DirNode> &nodes, int l, int r, float random) {
    DirNode branch;
    const DirNode &left = nodes[l];
    const DirNode &right = nodes[r];
    branch.le = left.le + right.le;
    branch.bbox = runion(left.bbox, right.bbox);
    if (random <= mean(left.le) / mean(branch.le))
        branch.light = left.light;
    else 
        branch.light = right.light;
    branch.leftIndex = l;
    branch.rightIndex = r;
    return branch;
}

inline OrtNode makeBranch(const std::vector<OrtNode> &nodes, int l, int r, float random) {
    OrtNode branch;
    const OrtNode &left = nodes[l];
    const OrtNode &right = nodes[r];
    branch.le = left.le + right.le;
    branch.bbox = runion(left.bbox, right.bbox);
    branch.cone = runion(left.cone, right.cone);
    if (random <= mean(left.le) / mean(branch.le))
        branch.light = left.light;
    else 
        branch.light = right.light;
    branch.leftIndex = l;
    branch.rightIndex = r;
    return branch;
}

template <typename Light>
struct LtBuildNode {
    vec6f point;
    Light *light;
};

inline float component(const vec6f &v, int index) { return ((float*)&v.x)[index]; }

template <typename Node> struct CompareLtNode {
    CompareLtNode(int a) { axis = a; }
    int axis;
    bool operator()(const Node &d1, const Node &d2) const {
        return component(d1.point,axis) == component(d2.point,axis) ? (d1.light < d2.light) :
        component(d1.point,axis) < component(d2.point,axis);
    }
};
                   
template <typename Node>
void __recursiveBuild(std::vector<Node> &nodes, int nodeNum, LtBuildNode<typename Node::lighttype>* buildNodes, int size, const std::vector<float> &rngs) {
    if(size == 1) {
        nodes[nodeNum] = makeLeaf(buildNodes->light);
        return;
    } else {
        range6f bound = invalidrange6f;
        for(int p = 0; p < size; p++) 
            bound = runion(bound, buildNodes[p].point);
        int splitAxis = max_component_index(extent(bound));
        int mid = static_cast<int>(size * 0.5f);
        
        std::nth_element(buildNodes, buildNodes + mid, buildNodes + size, CompareLtNode<LtBuildNode<typename Node::lighttype> >(splitAxis));
        
        if (0 < mid && mid < size){
            int left = nodeNum + 1;
            int right = nodeNum + mid * 2;
            __recursiveBuild(nodes, left, buildNodes, mid, rngs);
            __recursiveBuild(nodes, right, buildNodes + mid, size - mid, rngs);
            nodes[nodeNum] = makeBranch(nodes, left, right, rngs[nodeNum]);
        } else {
            error("invalid split");
        }
    }
}

template<typename Node>
struct LightTreeBuildTask : tbb::task {
    LightTreeBuildTask(std::vector<Node> &ns, int nn, 
                       LtBuildNode<typename Node::lighttype>* bn, int sz, 
                       const std::vector<float> &r) 
        : nodes(ns), nodeNum(nn), buildNodes(bn), size(sz), rngs(r) {}
    
    tbb::task* execute() {
        if(size < 2048) {
            __recursiveBuild(nodes, nodeNum, buildNodes, size, rngs);
        } else {
            range6f bound = invalidrange6f;
            for(int p = 0; p < size; p++) 
                bound = runion(bound, buildNodes[p].point);
            int splitAxis = max_component_index(extent(bound));
            int mid = static_cast<int>(size * 0.5f);
            
            std::nth_element(buildNodes, buildNodes + mid, buildNodes + size, CompareLtNode<LtBuildNode<typename Node::lighttype> >(splitAxis));
            
            if (0 < mid && mid < size){
                int left = nodeNum + 1;
                int right = nodeNum + mid * 2;
                
                LightTreeBuildTask<Node> &a = *new (allocate_child())LightTreeBuildTask<Node>(nodes, left, buildNodes, mid, rngs);
                LightTreeBuildTask<Node> &b = *new (allocate_child())LightTreeBuildTask<Node>(nodes, right, buildNodes+mid, size - mid, rngs);
                
                set_ref_count(3);
                spawn(b);
                spawn_and_wait_for_all(a);

                nodes[nodeNum] = makeBranch(nodes, left, right, rngs[nodeNum]);
            } else {
                error("invalid split");
            }

        }
        return 0;
    }
    
    std::vector<Node> &nodes;
    const int nodeNum;
    LtBuildNode<typename Node::lighttype>* buildNodes;
    const int size;
    const std::vector<float> &rngs;
};

template<typename Node>
std::vector<Node> parallelBuildLightTree(std::vector<typename Node::lighttype> &lights,
                                         float normalScale) {
    if (lights.size() == 0) std::vector<Node>();
    
    std::vector<Node> nodes(lights.size() * 2);
    Rng rng;
    std::vector<float> rngs(lights.size() * 2);
    for (int i = 0; i < rngs.size(); i++) {
        rngs[i] = rng.next1f();
    }
    
    std::vector<LtBuildNode<typename Node::lighttype> > buildNodes(lights.size());
    for (int i = 0; i < lights.size(); i++) {
        buildNodes[i].light = &(lights[i]);
        buildNodes[i].point = getPoint(lights[i], normalScale);
    }
    
    LightTreeBuildTask<Node>& root = *new(tbb::task::allocate_root()) LightTreeBuildTask<Node>(nodes, 0, buildNodes.data(), lights.size(), rngs);
    tbb::task::spawn_root_and_wait(root);
    return nodes;
}

template<typename Node>
std::vector<Node> buildLightTree(std::vector<typename Node::lighttype> &lights,
                                 float normalScale) {
    if(lights.size() == 0) return std::vector<Node>();
    
    std::vector<Node> nodes(lights.size() * 2);
    Rng rng;
    std::vector<float> rngs(lights.size() * 2);
    for (int i = 0; i < rngs.size(); i++) {
        rngs[i] = rng.next1f();
    }
    std::vector<LtBuildNode<typename Node::lighttype> > buildNodes(lights.size());
    for (int i = 0; i < lights.size(); i++) {
        buildNodes[i].light = &(lights[i]);
        buildNodes[i].point = getPoint(lights[i], normalScale);
    }
    
    __recursiveBuild<Node>(nodes, 0, buildNodes.data(), lights.size(), rngs);
    return nodes;
}

template<typename Node>
std::vector<Node> buildLightTree(std::vector<typename Node::lighttype> &lights, 
                             float normalScale, bool parallel) {
    if(parallel) 
        return parallelBuildLightTree<Node>(lights, normalScale);
    else 
        return buildLightTree<Node>(lights, normalScale);
}

template<typename Node>
void cleanLightTree(std::vector<Node>*& tree) {
    delete tree;
    tree = 0;
}

#endif //_INTERGRATOR_LIGHTCUT_LIGHTTREE_H_