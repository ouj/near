#include "photon_kdtree.h"
#include <tbb/tbb.h>
#include <common/array.h>
#include <common/range.h>
#include <vector>

using namespace std;

inline KdNode __makeLeaf() {
    KdNode leaf;
    leaf.split = 0;
    leaf.axis = -1;
    leaf.leftIndex = std::numeric_limits<int>::max();
    leaf.rightIndex = std::numeric_limits<int>::max();
    return leaf;
}

inline KdNode __makeBranch(float split, int axis) {
    KdNode branch;
    branch.split = split;
    branch.axis = axis;
    branch.leftIndex = std::numeric_limits<int>::max();
    branch.rightIndex = std::numeric_limits<int>::max();
    return branch;
}

void __recursiveBuild(vector<Photon> &data, vector<KdNode> &nodes, int nodeNum, const Photon** buildNodes, int size) {
    if(size <= 1) {
        nodes[nodeNum] = __makeLeaf();
        data[nodeNum] = **buildNodes;
        return;
    }

    range3f bound = invalidrange3f;
    for(int p = 0; p < size; p++) 
        bound.grow(buildNodes[p]->position);
    int axis = bound.extent().max_component_index();
    int mid = static_cast<int>(size * 0.5f);
    std::nth_element(buildNodes, buildNodes + mid, buildNodes + size,
                     [axis](const Photon *d1, const Photon *d2) {
                         return d1->position[axis] == d2->position[axis] ? (d1 < d2) :
                         d1->position[axis] < d2->position[axis];
                     });

    nodes[nodeNum] = __makeBranch(component(buildNodes[mid]->position, axis), axis);
    data[nodeNum] = *buildNodes[mid];

    if (0 < mid) {
        nodes[nodeNum].leftIndex = nodeNum + 1;
        __recursiveBuild(data, nodes, nodes[nodeNum].leftIndex, buildNodes, mid);
    }
    if (mid+1 < size) {
        nodes[nodeNum].rightIndex = nodeNum + mid + 1;
        __recursiveBuild(data, nodes, nodes[nodeNum].rightIndex, buildNodes + mid + 1, size - mid - 1);
    }
    //error(nodes[nodeNum].hasLeft || nodes[nodeNum].rightIndex < nodes.size(), "error kdtree");
}


void rebuildKdTree(KdTree* tree, Photon** data, int size) {
    if(size == 0 || data == 0) {
        warning("invalid kd tree");
        return;
    }
    if (tree->nodes.size() < size) {
        tree->nodes.resize(size);
        tree->data.resize(size);
    }
    std::vector<const Photon*> buildNodes(size);
    for (int i = 0; i < size; i++)
        buildNodes[i] = data[i];

    __recursiveBuild(tree->data, tree->nodes, 0, buildNodes.data(), size);
}

KdTree buildKdTree(Photon** data, int size) {
    KdTree tree;
    if(size == 0 || data == 0)
        return tree;

    tree.nodes.resize(size);
    tree.data.resize(size);
    vector<const Photon*> buildNodes(size);
    for (int i = 0; i < size; i++)
        buildNodes[i] = data[i];

    __recursiveBuild(tree.data, tree.nodes, 0, buildNodes.data(), size);
    return tree;
}


struct KdTreeBuildTask : tbb::task {
    KdTreeBuildTask(vector<Photon> &d, vector<KdNode> &ns, int nn, const Photon **bn, int s)
        : data(d), nodes(ns), nodeNum(nn), buildNodes(bn), size(s) {}

    tbb::task* execute() {
        if(size < 1024) {
            __recursiveBuild(data, nodes, nodeNum, buildNodes, size);
        } else {
            range3f bound = invalidrange3f;
            for(int p = 0; p < size; p++) 
                bound.grow(buildNodes[p]->position);   

            int axis = bound.extent().max_component_index();
            int mid = static_cast<int>(size * 0.5f);
            //_sortAlongAxis(splitAxis, buildNodes, size);
            std::nth_element(buildNodes, buildNodes + mid, buildNodes + size,
                             [axis](const Photon *d1, const Photon *d2) {
                                 return d1->position[axis] == d2->position[axis] ? (d1 < d2) :
                                 d1->position[axis] < d2->position[axis];
                             });

            nodes[nodeNum] = __makeBranch(component(buildNodes[mid]->position, axis), axis);
            data[nodeNum] = *buildNodes[mid];

            if (0 < mid && mid+1 < size) {
                tbb::empty_task& c = *new( allocate_continuation() ) tbb::empty_task();
                nodes[nodeNum].leftIndex = nodeNum + 1;
                KdTreeBuildTask& a = *new( c.tbb::task::allocate_child() ) KdTreeBuildTask(data, nodes, nodes[nodeNum].leftIndex, buildNodes, mid);
                nodes[nodeNum].rightIndex = nodeNum + mid + 1;
                KdTreeBuildTask& b = *new( c.tbb::task::allocate_child() ) KdTreeBuildTask(data, nodes, nodes[nodeNum].rightIndex, buildNodes+mid+1, size-mid-1);
                c.set_ref_count(2);
                spawn(a);
                spawn(b);
                return 0;
            } else { 
                error("invalid split"); 
            }
        }
        return 0;
    }

    vector<Photon>  &data;
    vector<KdNode>  &nodes;
    int             nodeNum;
    const Photon**  buildNodes;
    int             size;
};

//void parallelRebuildKdTree(KdTree* tree, Photon** data, int size) {
//    if(size == 0 || data == 0) {
//        warning("invalid kd tree");
//        return;
//    }
//    if (tree->nodes.size() < size) {
//        tree->nodes.resize(size);
//        tree->data.resize(size);
//    }
//
//    std::vector<const Photon*> buildNodes(size);
//    for (int i = 0; i < size; i++)
//        buildNodes[i] = data[i];
//
//    KdTreeBuildTask& root = *new(tbb::task::allocate_root()) KdTreeBuildTask(tree->data, tree->nodes, 0, buildNodes.data(), size);
//    tbb::task::spawn_root_and_wait(root);
//}

KdTree parallelBuildKdTree(Photon** data, int size) {
    KdTree tree;
    if(size == 0 || data == 0)
        return tree;

    tree.nodes.resize(size);
    tree.data.resize(size);
    vector<const Photon*> buildNodes(size);
    for (int i = 0; i < size; i++)
        buildNodes[i] = data[i];

    KdTreeBuildTask& root = *new(tbb::task::allocate_root())
    KdTreeBuildTask(tree.data, tree.nodes, 0, buildNodes.data(), size);
    tbb::task::spawn_root_and_wait(root);
    return tree;
}

