#include "path.h"
#include <integration/opts.h>
#include <integration/utils.h>
#include "options.h"

PathRenderOpts::PathRenderOpts(): RenderOpts(PathRender::TYPEID) {}

jsonObject PathRenderOpts::print() const {
    jsonObject json;
    json["type"] = "pathrenderopts";
    json["directmode"] = printDirectMode(directmode);
    json["diffusemultibounce"] = diffusemultibounce;
    json["maxdepth"] = maxdepth;
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["general"] = printGeneralOpts(general);
    return json;
}

void PathRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("pathrenderopts");
    directmode = parseDirectMode(jsonGet(json, "directmode"));
    diffusemultibounce = jsonGet(json, "diffusemultibounce");
    maxdepth = jsonGet(json, "maxdepth");
    imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    general = parseGeneralOpts(jsonGet(json,"general"));
}

PathRender::PathRender() : Render(TYPEID) {}

PathRender::PathRender(const PathRenderOpts& opts)
    : Render(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

PathRender::~PathRender() { clearState(); }

bool PathRender::initState(const Scene* scene, const Accelerator *accelerator) {
    if (!Render::initState(scene, accelerator)) return false;
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    return true;
}

void PathRender::clearState() {}

vec3f evalRadiance(const ray3f& ray, int depth,
                   SampleSequence& seq, const std::vector<Light*> &lights,
                   const Accelerator *accelerator, const PathRenderOpts& opts) {
    // trace ray
    vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
    if(!traceRay(ray, depth == 0, depth == 0, lights, accelerator, f, wo, le, rs))
        return le;
    if(!isvalid(rs)) return le;

    // convert reflectance to diffuse if needed
    ReflectanceSample brs = rs;
    if(opts.diffusemultibounce && depth > 0) brs = reflectanceToLambert(brs);

    // direct
    vec3f ld = zero3f;
    if(depth == 0) ld = evalDirect(brs, f, wo, seq, opts.directmode, lights, accelerator);
    else ld = evalDirect(brs, f, wo, seq, TDM_UNIFORM_MIS, lights, accelerator);

    error_if(ld.hasinvalid(), "invalid radiance");

    // indirect
    vec3f li = zero3f;
    if(depth < opts.maxdepth-1) {
        ScatteringSample ss = sampleScattering(rs,f,wo,next1f(seq),next2f(seq));
        vec3f lr;
        if(!ss.bsdf.iszero()) {
            vec3f scale = ss.bsdf * dot(ss.ray.d,f.z) / ss.pdf;
            if (options::path::russianroulette) {
                float continueProb = min(1.0f, mean(scale));
                if (seq.rng->next1f() > continueProb) {
                    lr = zero3f;
                } else {
                    lr = evalRadiance(ss.ray,depth+1,seq,lights,accelerator,opts);
                    lr /= continueProb;
                }
            } else {
                lr = evalRadiance(ss.ray,depth+1,seq,lights,accelerator,opts);
            }
            li = scale * lr;
        }
    }
    error_if(li.hasinvalid(), "invalid indirect radiance");

    // done
    return le+ld+li;
}

bool PathRender::tracePass() {
    auto func = [this](const ray3f& ray, SampleSequence& seq) {
        return evalRadiance(ray, 0, seq, scene->lights, accelerator, _opts);
    };
    sampleImage(scene, func, _imageSampling, _opts.imageSampling, _opts.general);
    return false;
}

Image<vec3f> PathRender::traceImage() {
    renderImageFunc<PathRender>(this, _opts.imageSampling);
    return _imageSampling.image;
}

vec2i PathRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* PathRender::getImage() const {
    return &(_imageSampling.image);
}
