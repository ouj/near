#include <scene/sceneio.h>
#include <scene/scenestat.h>
#include <scene/transform.h>
#include <scene/shape.h>
#include <scene/material.h>
#include <common/json.h>
#include <integration/utils.h>
#include <glutil/glutil.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

using std::string;

namespace options {
    namespace view {
        const bool vertexarrays = false;
        const bool fixmeshnormals = true;
    }
}

enum LightingType { LT_AMBIENT = 0, LT_CAMERA = 1 }; const int LT_MAX = 2;
enum PolygonType {
    PT_SOLID,
    PT_WIRE,
    PT_COUNT
};

struct ViewOpts {
    int width;
    int height;
    LightingType lightingType;
    PolygonType polygonType;
    vec3f worldY;
};
const ViewOpts defaultViewOpts = { 512, 512, LT_CAMERA, PT_SOLID, y3f };

vec3f translation = zero3f;
float translationSpeed = 0.001f;
vec3f rotation = zero3f;
float rotationSpeed = 0.001f;
vec3f rotationY = y3f;
vec3f rotationX = x3f;

ViewOpts parseViewOpts(const jsonObject& json) {
    ViewOpts opts;
    error_if_not(jsonGet(json,"type").as_string() == "viewopts", "incorrect type");
    opts.width = jsonGet(json,"width").as_int();
    opts.height = jsonGet(json,"height").as_int();
    return opts;
}

jsonObject printViewOpts(const ViewOpts& opts) {
    jsonObject json;
    json["type"] = "traceopts";
    json["width"] = opts.width;
    json["height"] = opts.height;
    return json;
}

Scene* scene;
string scene_filename;
string config_filename;
ViewOpts opts;
GLScene glScene;
GLFWwindow *window = 0;

GLProgram program;
const char* vertexShaderSource = GLSL_SHADER_SOURCE();

enum TransformType { TT_SURFACE, TT_CAMERA };
void drawTransform(const Transform* transform, TransformType type) {
    switch(transform->type) {
        case IdentityTransform::TYPEID: break;
        case RigidTransform::TYPEID: {
            frame3f f = ((const RigidTransform*)transform)->frame;
            if(type == TT_CAMERA) {
                f.z = -f.z; f.y = -f.y;
                f = invert(f);
            }
            float m[16];
            m[ 0] = f.x.x; m[ 1] = f.x.y; m[ 2] = f.x.z; m[ 3] = 0;
            m[ 4] = f.y.x; m[ 5] = f.y.y; m[ 6] = f.y.z; m[ 7] = 0;
            m[ 8] = f.z.x; m[ 9] = f.z.y; m[10] = f.z.z; m[11] = 0;
            m[12] = f.o.x; m[13] = f.o.y; m[14] = f.o.z; m[15] = 1;
//            glMultMatrixf(m);
        } break;
        default: error("unknown transform type"); break;
    }
}

void drawCamera(const Camera* camera) {
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
    switch(camera->type) {
        case PerspectiveCamera::TYPEID: {
            const PerspectiveCamera* lens = (const PerspectiveCamera*)camera;
//            glFrustum(-lens->width/2,  lens->width/2,
//                      -lens->height/2, lens->height/2,
//                      lens->distance, 1e12f);
        } break;
        default: error("unknown lens"); break;
    }
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
    drawTransform(camera->transform, TT_CAMERA);
}

void drawSurface(const GLSurface &surface) {
    // Bind matrix
    // Bind VBA
    // Draw
}

void drawScene(const Scene* scene) {
    glClearColor(0, 0, 0, 0);
    glClearDepth(1);

    switch (opts.polygonType) {
        case PT_SOLID: glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;
        case PT_WIRE: glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
        default: error("unknown poly mode"); break;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    for (const GLSurface &surface : glScene.surfaces) {
        drawSurface(surface);
    }

    drawCamera(scene->cameras[0]);

//    for(int i = 0; i < scene->surfaces.size(); i ++) {
//        drawSurface(scene->surfaces[i]);
//    }

//    glPopAttrib();
//    glPopMatrix();
}

void display() {
    drawScene(scene);
}

//void keyboardDown(unsigned char c, int x, int y) {
//    switch (c) {
//        case 'l': opts.lightingType = (LightingType)((opts.lightingType + 1) % LT_MAX); break;
//        case 'p': opts.polygonType = (PolygonType)((opts.polygonType + 1) % PT_MAX); break;
//        case 'd': translation.x = -translationSpeed; break;
//        case 'a': translation.x =  translationSpeed; break;
//        case 'w': translation.z = -translationSpeed; break;
//        case 's': translation.z =  translationSpeed; break;
//        case 'e': translation.y = -translationSpeed; break;
//        case 'q': translation.y =  translationSpeed; break;
//        case '-': translationSpeed *= 0.1f; break;
//        case '+': translationSpeed *= 10.0f; break;
//    }
//}
//
//void keyboardUp(unsigned char c, int x, int y) {
//    switch (c) {
//        case 'd': translation.x =  0; break;
//        case 'a': translation.x =  0; break;
//        case 'w': translation.z =  0; break;
//        case 's': translation.z =  0; break;
//        case 'e': translation.y =  0; break;
//        case 'q': translation.y =  0; break;
//    }
//}

static void keyboard(GLFWwindow* window, int key, int scancode,
                     int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    bool handled_main = true;
    switch(key) {
        case GLFW_KEY_P:
            opts.polygonType = (PolygonType)((opts.polygonType + 1) % PT_COUNT);
            break;
        default: handled_main = false; break;
    }
    // done
    if(!handled_main) warning("key not handled");
}

static void resize(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}

void mouse(int button, int state, int x, int y) {
    rotation = zero3f;
    rotationX = ((RigidTransform*)scene->cameras[0]->transform)->frame.x;
    rotationY = opts.worldY;
}

void motion(int x, int y) {
    int s = min(opts.width/2, opts.height/2);
    x = clamp(x-opts.width/2, -s, s);
    y = clamp(y-opts.height/2, -s, s);
    rotation = vec3f(y * rotationSpeed / s, x * rotationSpeed / s, 0);
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);
}

void fixCamera() {
    Camera* camera = scene->cameras[0];
    switch(camera->transform->type) {
        case IdentityTransform::TYPEID: {
            warning("original camera has identiy xform, switching to rigid transform");
            RigidTransform* transform = new RigidTransform();
            transform->frame = defaultframe3f;
            camera->transform = transform;
        } break;
        case RigidTransform::TYPEID: break;
        default: error("unknown transform type"); break;
    }
}

int main(int argc, char** argv) {
    const string usage = "usage: view scene.json [config.json]";

    error_if_not(argc >= 2 && argc <= 3, usage.c_str());
    if(argc < 2 || argc > 3) return false;

    scene_filename = argv[1];
    if (argc == 3) {
        config_filename = argv[2];
    }

    if(config_filename.size() > 0) {
        jsonValue json = loadJson(config_filename);
        opts = parseViewOpts(json.as_object());
    } else {
        opts = defaultViewOpts;
    }

    message((string("scene: ") + scene_filename).c_str());
    message((string("opts:  ") + printJson(printViewOpts(opts))).c_str());

    scene = loadScene(scene_filename);
    message_va("scene %s loaded", scene_filename.c_str());
    message_va("scene stat: %s", printJson(statistics(scene)).c_str());
    fixCamera();
    opts.height = adjustedHeight(scene->cameras[0], opts.width);

    if(!glfwInit()) {
        error("failed to initialize GLFW.");
        return EXIT_FAILURE;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    window =
        glfwCreateWindow(opts.width, opts.height, "view", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return EXIT_FAILURE;
    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, keyboard);
    glfwSetWindowSizeCallback(window, resize);

    glScene = GLCreator().process(scene);

    while (!glfwWindowShouldClose(window)) {
        display();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}
