#include <common/debug.h>
#include <common/stddir.h>
#include <common/json.h>
#include <render/render.h>
#include <render/path.h>
#include <render/debug.h>
#include <render/photon.h>
#include <render/spphoton.h>
#include <render/igi.h>
#include <render/bidir.h>
#include <render/erpt.h>
#include <render/mlt.h>
#include <render/lightcut.h>
#include <render/lod.h>
#include <grender/gpath.h>
#include <accelerator/accelerator_list.h>
#include <accelerator/accelerator_bvh.h>
#include <accelerator/accelerator_streambvh.h>
#include <accelerator/accelerator_flatbvh.h>
#include <accelerator/accelerator_lux.h>
#include <accelerator/accelerator_mix.h>

template<typename T>
void writeOpts() {
    T opts;
    jsonObject json = opts.print();
    std::string type = jsonGet(json, "type").as_string();
    message_va("generator config for %s", type.c_str());
    std::string filename = type + ".json";
    saveJson(filename, json);
}

int main(int argc, char** argv) {
    writeOpts<DebugRenderOpts>();
    writeOpts<PathRenderOpts>();
    writeOpts<PhotonRenderOpts>();
    writeOpts<SpPhotonRenderOpts>();
    writeOpts<IgiRenderOpts>();
    writeOpts<BidirRenderOpts>();
    writeOpts<ErptRenderOpts>();
    writeOpts<MltRenderOpts>();
    writeOpts<LightcutRenderOpts>();
    writeOpts<LodRenderOpts>();

    writeOpts<GpuPathRenderOpts>();

    writeOpts<ListAcceleratorOpts>();
    writeOpts<BvhAcceleratorOpts>();
    writeOpts<LuxAcceleratorOpts>();
    writeOpts<MixAcceleratorOpts>();
    writeOpts<StreamBvhAcceleratorOpts>();
    writeOpts<FlatBvhAcceleratorOpts>();
}

