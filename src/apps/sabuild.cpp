#include <accelerator/accelerator_streambvh_build.h>
#include <common/debug.h>

int main(int argc, char** argv) {
    const std::string usage = "usage: sabuild scene.json";
    if (argc < 2) { error(usage); return 0; }
    StreamAcceleratorBuilder builder;
    std::string scenefilename = argv[1];
    std::string treefilename = "streambvhaccelerator.tree";
    if (argc >= 3) treefilename = argv[2];
    message_va("build stream bvh for scene %s", scenefilename.c_str());
    message_va("output to %s", treefilename.c_str());
    if(!builder.buildStreamAccelerator(scenefilename, treefilename)) {
        error("failed to build stream accelerator");
    }
    return 0;
}
