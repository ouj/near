#include <scene/scenestat.h>
#include <common/pam.h>

#include <glutil/glutil.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <grender/gpath.h>

using namespace std;

static const char* vertex_shader_source = GLSL_SHADER_SOURCE(
attribute vec2 position;
attribute vec2 texcoord;
varying vec2 v_texcoord;

void main(void) {
    gl_Position = vec4(position.x, position.y, 0.0, 1.0);
    v_texcoord = texcoord;
}
);

static const char* pixel_shader_source = GLSL_SHADER_SOURCE(
uniform sampler2D texture;
uniform float exposure;
uniform float gamma;
varying vec2 v_texcoord;

void main(void) {
    float scale = pow(2.0, exposure);
    float exponent = 1.0 / gamma;
    vec3 v = texture2D(texture, v_texcoord).rgb;
    v.r = pow(v.r, exponent);
    v.g = pow(v.g, exponent);
    v.b = pow(v.b, exponent);
    v = v * scale;
    gl_FragColor = vec4(v, 1.0);
}
);

string scene_filename;
string image_filename;
Scene *scene = 0;
GpuPathRender *render = 0;
GpuBvhAccelerator *accelerator = 0;

tbb::tick_count::interval_t time_load;
tbb::tick_count::interval_t time_build;
tbb::tick_count::interval_t time_clear;
tbb::tick_count::interval_t time_render;
tbb::tick_count render_start, render_end;

float display_exposure = 0;
float display_gamma = 2.2;

GLFWwindow *window = 0;
vec2i imageSize { 512, 512 };
vec2i windowSize { 512, 512 };
const Image<vec3f> *image = 0;

GLProgram program;
GLQuad quad;
GLuint textureId = 0;
GLuint uniformExposure = 0;
GLuint uniformGamma = 0;
GLuint uniformTexture = 0;

tbb::tbb_thread*    renderer_thread = 0;
tbb::atomic<bool>   renderer_imageready;
tbb::atomic<bool>   renderer_on;
tbb::atomic<bool>   renderer_refresh;
tbb::atomic<bool>   renderer_exit;

void renderer_render();
void renderer_start() {
    renderer_on = true;
    renderer_exit = false;
    renderer_refresh = false;
    renderer_imageready = false;
    message("----------");
    render_start = tbb::tick_count::now();
    renderer_thread = new tbb::tbb_thread(renderer_render);
}

void renderer_stop() {
    renderer_on = false;
    if (renderer_thread) {
        renderer_thread->join();
        delete renderer_thread;
    }
    message("\n");
    render_end = tbb::tick_count::now();
    time_render = render_end - render_start;
    message_va("time render: %g", time_render.seconds());

    message_va("scene stat: \n%s", printJson(statistics(scene)).c_str());

    savePfm(image_filename, *render->getImage());

    tbb::tick_count start, stop;
    start = tbb::tick_count::now();
    render->clearState();
    delete accelerator;
    deleteScene(scene);
    stop = tbb::tick_count::now();
    time_clear = stop - start;
    message_va("time clear: %g", time_clear.seconds());
}

void renderer_restart() {
    renderer_refresh = true;
    renderer_on = true;
    renderer_imageready = false;
}

void renderer_render() {
    static int pass = 0;
    static bool reached_max = false;
    const bool done = false;
    while (renderer_on) {
        if(renderer_refresh) {
            renderer_refresh = false;
            render->initState(scene, accelerator);
            pass = 0;
            reached_max = false;
        }
        if(!done) {
            if(render->tracePass()) {
                renderer_exit = true;
                return;
            }
            pass++;
            fprintf(stdout, "\rPASS: %4d", pass);
            fflush(stdout);
            image = render->getImage();
            renderer_imageready = true;
        } else {
            if (reached_max == false) {
                warning("reached max samples");
                reached_max = true;
            }
        }
    }
}

void updateTexture(const Image<vec3f> &image) {
    if(textureId == 0) {
        glGenTextures(1, &textureId);
        RETURN_IF_GL_ERROR("glGenTextures");
        glBindTexture(GL_TEXTURE_2D, textureId);
        RETURN_IF_GL_ERROR("glBindTexture");
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        RETURN_IF_GL_ERROR("glTexParameteri");
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        RETURN_IF_GL_ERROR("glTexParameteri");
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, image.width(), image.height(),
                     0, GL_RGB, GL_FLOAT, &image.at(0));
        RETURN_IF_GL_ERROR("glTexImage2D");
    } else {
        glBindTexture(GL_TEXTURE_2D, textureId);
        RETURN_IF_GL_ERROR("glBindTexture");
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.width(), image.height(),
                        GL_RGB, GL_FLOAT, &image.at(0));
        RETURN_IF_GL_ERROR("glTexSubImage2D");
    }
    glGetError();
    renderer_imageready = false;
}

void drawView() {
    if (textureId == 0) return;
    if(!program.use())
        return;

    glActiveTexture(GL_TEXTURE0);
    RETURN_IF_GL_ERROR("glActiveTexture");
    glBindTexture(GL_TEXTURE_2D, textureId);
    RETURN_IF_GL_ERROR("glBindTexture");
    glUniform1i(uniformTexture, 0);
    RETURN_IF_GL_ERROR("glUniform1i");

    glUniform1f(uniformExposure, display_exposure);
    RETURN_IF_GL_ERROR("glUniform1f");
    glUniform1f(uniformGamma, display_gamma);
    RETURN_IF_GL_ERROR("glUniform1f");

    quad.draw();
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    RETURN_IF_GL_ERROR("glClear");
    if (image) {
        if (renderer_imageready)
            updateTexture(*image);
        drawView();
    }
}

GLProgram createGLShader() {
    GLProgram program;

    GLShader vertexShader, fragShader;
    if (!vertexShader.compile(GL_VERTEX_SHADER, vertex_shader_source)) {
        error_va("failed to compile vertex shader %s", vertex_shader_source);
        return program;
    }
    if (!fragShader.compile(GL_FRAGMENT_SHADER, pixel_shader_source)) {
        error_va("failed to compile vertex shader %s", pixel_shader_source);
        return program;
    }

    if(!program.attachShaders(vertexShader, fragShader)) {
        error_va("failed to attach shaders");
        return program;
    }
    if(!program.link()) {
        error_va("failed to link program");
        return program;
    }
    return program;
}

static void keyboard(GLFWwindow* window, int key, int scancode,
                     int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    bool handled_main = true;
    switch(key) {
        case GLFW_KEY_1: display_gamma = 1; display_exposure = 0; break;
        case GLFW_KEY_2: display_gamma = 2.2f; display_exposure = 0; break;
        case GLFW_KEY_MINUS: display_exposure -= 0.5; break;
        case GLFW_KEY_EQUAL: display_exposure += 0.5; break;
        case GLFW_KEY_LEFT_BRACKET: display_gamma -= 0.1f; break;
        case GLFW_KEY_RIGHT_BRACKET: display_gamma += 0.1f; break;
        case GLFW_KEY_SPACE: renderer_restart(); break;
        default: handled_main = false; break;
    }
    // done
    if(!handled_main) warning("key not handled");
}

static void resize(GLFWwindow *window, int width, int height) {
    windowSize = vec2i(width, height);
    if (image != 0) {
        double ratio = width / height;
        double img_ratio = (float)image->width() / image->height();
        if (ratio < img_ratio) {
            imageSize.x = width;
            imageSize.y = width / img_ratio;
        } else {
            imageSize.y = height;
            imageSize.x = height * img_ratio;
        }
    }
    glViewport((windowSize.x - imageSize.x) / 2, (windowSize.y - imageSize.y) / 2,
               imageSize.x, imageSize.y);
}

bool parseCmdLine(int argc, char** argv) {
    const char *usage = "usage: cl_bvh scene.json [image.pfm]";
    if(argc < 2) {
        message(usage);
        return false;
    }
    scene_filename = argv[1];
    image_filename = "gimage.pfm";
    if (argc > 2) {
        image_filename = argv[2];
    }

    message_va("scene: %s", scene_filename.c_str());
    return true;
}

int main(int argc, char** argv) {
    if(parseCmdLine(argc,argv) == false) return 0;
    tbb::tick_count start, stop;

    start = tbb::tick_count::now();
    scene = loadScene(scene_filename);
    stop = tbb::tick_count::now();
    message_va("time load : %g", (stop - start).seconds());

    start = tbb::tick_count::now();
    accelerator = new GpuBvhAccelerator();
    if (!accelerator->buildAccelerator(scene->surfaces, std::vector<Light*>())) {
        error("Failed to build GPU BVH");
        return EXIT_FAILURE;
    };
    stop = tbb::tick_count::now();
    message_va("time build: %g", (stop - start).seconds());

    render = new GpuPathRender();
    if (!render->initState(scene, accelerator)) {
        error("Failed to init renderer");
        return EXIT_FAILURE;
    }
    windowSize = imageSize = render->getImageSize();

    if(!glfwInit()) {
        error("failed to initialize GLFW.");
        return EXIT_FAILURE;
    }

    window = glfwCreateWindow(windowSize.x, windowSize.y, "gtrace", NULL, NULL);
    if (!window) {
        error("falied to create gl window.");
        glfwTerminate();
        return EXIT_FAILURE;
    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, keyboard);
    glfwSetWindowSizeCallback(window, resize);

    program = createGLShader();
    if (!program.isValid()) {
        error("failed to create gl shading program.");
        return EXIT_FAILURE;
    }
    uniformExposure = program.getUniformLocation("exposure");
    uniformGamma = program.getUniformLocation("gamma");
    uniformTexture = program.getUniformLocation("texture");
    quad.setPositionAttribHandle(program.getAttributeLocation("position"));
    quad.setTexcoordAttribHandle(program.getAttributeLocation("texcoord"));

    renderer_start();
    while (!glfwWindowShouldClose(window)) {
        display();
        glfwSwapBuffers(window);
        glfwPollEvents();
        if (renderer_exit) {
            break;
        }
    }
    renderer_stop();
    if (window) {
        glfwDestroyWindow(window);
    }
    return 0;
}


