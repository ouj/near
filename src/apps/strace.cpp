#include <scene/sceneio.h>
#include <scene/scenestat.h>
#include <accelerator/build.h>
#include <render/build.h>
#include <common/pam.h>
#include <common/json.h>
#include <common/stdheader.h>
#include <tbb/tick_count.h>

Scene* scene;
string scene_filename;
string image_filename;

const string usage = "usage: strace scene.json image.pfm [opts.json]";

Accelerator* accelerator;
Render* render = 0;

tbb::tick_count::interval_t time_load;
tbb::tick_count::interval_t time_build;
tbb::tick_count::interval_t time_trace;
tbb::tick_count::interval_t time_clear;

bool parseCmdLine(int argc, char** argv) {
    error_if_not(argc >= 3 || argc <= 5, usage.c_str());
    if (argc < 3 || argc > 5) return false;
    scene_filename = argv[1];
    image_filename = argv[2];

    if(argc > 3) {
        string opts_filename = argv[3];
        jsonValue json = loadJson(opts_filename);
        std::unique_ptr<RenderOpts> opts(parseRenderOpts(json.as_object()));
        message_va("render opts: %s", printJson(opts->print()).c_str());
        render = createRender(opts.get());
    } else {
        render = createDefaultRender();
    }

    if(argc > 4) {
        string opts_filename = argv[4];
        jsonValue json = loadJson(opts_filename);
        std::unique_ptr<AcceleratorOpts> opts(parseAcceleratorOpts(json.as_object()));
        message_va("accelerator opts: %s", printJson(opts->print()).c_str());
        accelerator = createAccelerator(opts.get());
    } else {
        accelerator = createDefaultAccelerator();
    }

    message_va("scene: %s", scene_filename.c_str());
    message_va("image: %s", image_filename.c_str());
    return true;
}

int main(int argc, char** argv) {
    if(!parseCmdLine(argc,argv)) return 0;

    tbb::tick_count start, stop;

    start = tbb::tick_count::now();
    scene = loadScene(scene_filename, false);
    stop = tbb::tick_count::now();
    time_load = stop - start;
    message_va("time load : %g", time_load.seconds());

    start = tbb::tick_count::now();
    accelerator->buildAccelerator(scene->surfaces, scene->lights);
    stop = tbb::tick_count::now();
    time_build = stop - start;
    message_va("time build: %g", time_build.seconds());

    render->initState(scene, accelerator);
    start = tbb::tick_count::now();
    Image<vec3f> image = render->traceImage();
    stop = tbb::tick_count::now();
    time_trace = stop - start;
    message_va("time trace: %g", time_trace.seconds());
    render->clearState();

    savePfm(image_filename, image);

    message_va("accelerator stat: \n%s", printJson(accelerator->statistics()).c_str());
    message_va("scene stat: \n%s", printJson(statistics(scene)).c_str());

    start = tbb::tick_count::now();
    delete accelerator;
    stop = tbb::tick_count::now();
    time_clear = stop - start;
    message_va("time clear: %g", time_clear.seconds());
}
