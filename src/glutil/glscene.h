#ifndef GL_SCENE_H
#define GL_SCENE_H

#include "glshape.h"
#include "glmaterial.h"
#include "glsurface.h"
#include <vector>

struct GLScene : GLObject {
    GLScene();
    GLScene(GLScene &&scene) noexcept;
    GLScene& operator=(GLScene &&scene) noexcept;
    ~GLScene();

    std::vector<GLShape>    shapes;
    std::vector<GLMaterial> materials;
    std::vector<GLSurface>  surfaces;
};

#endif