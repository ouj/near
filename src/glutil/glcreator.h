#ifndef GL_CONV_H
#define GL_CONV_H

#include "glshape.h"
#include "glmaterial.h"
#include "glsurface.h"
#include "glscene.h"
#include "glmatrix.h"
#include <vector>
#include <unordered_map>

struct Shape;
struct Scene;
struct Material;
struct Transform;
class GLCreator {
public:
    GLScene&& process(const Scene *scene);
    void cleanup(void);
private:
    GLShape processShape(const Shape *shape,
                         GLint positionAttribHandle = 0,
                         GLint normalAttribHandle = 1,
                         GLint texcoordAttribHandle = 2);
    GLMaterial processMaterial(const Material *material);
    
    std::unordered_map<Shape*, GLShape*>        shapeMap;
    std::unordered_map<Material*, GLMaterial*>  materialMap;
    
    GLScene glScene;
};

#endif