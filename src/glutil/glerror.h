#ifndef GL_ERROR_H
#define GL_ERROR_H

#include "glheader.h"
#include <common/debug.h>
#include <string>

#define GL_ERROR_CHECK

inline const char* glErrorString(GLenum err) {
    switch(err) {
        case GL_NO_ERROR:
            return "NO ERROR";
        case GL_INVALID_ENUM:
            return "INVALID ENUM";
        case GL_INVALID_VALUE:
            return "INVALID VALUE";
        case GL_INVALID_OPERATION:
            return "INVALID OPERATION";
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            return "INVALID FRAMEBUFFER OPERATION";
        case GL_OUT_OF_MEMORY:
            return "OUT OF MEMORY";
        default:
            return "unknown";
    }
}

#ifdef GL_ERROR_CHECK

#define CHECK_GL_ERROR(func) {                                                 \
    GLenum err = glGetError();                                                 \
    if (err != GL_NO_ERROR) {                                                  \
        error_va("GL error %s for Command: %s [%s:%d].",                       \
                 glErrorString(err), func, __FILE__, __LINE__);                \
    }                                                                          \
}                                                                              \

#define RETURN_IF_GL_ERROR(func) {                                             \
    GLenum err = glGetError();                                                 \
    if (err != GL_NO_ERROR) {                                                  \
        error_va("GL error %s for Command: %s [%s:%d].",                       \
                 glErrorString(err), func, __FILE__, __LINE__);                \
        return;                                                                \
    }                                                                          \
}                                                                              \

#define RETURN_FALSE_IF_GL_ERROR(func) {                                       \
    GLenum err = glGetError();                                                 \
    if (err != GL_NO_ERROR) {                                                  \
        error_va("GL error %s for Command: %s [%s:%d].",                       \
                 glErrorString(err), func, __FILE__, __LINE__);                \
        return false;                                                          \
    }                                                                          \
}

#else
#define CHECK_GL_ERROR(msg)
#define RETURN_IF_GL_ERROR(msg)
#define RETURN_FALSE_IF_GL_ERROR(msg)

#endif

#endif