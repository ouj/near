#include "glsurface.h"
#include "glerror.h"
#include "glmacro.h"
#include <common/vec.h>

GLSurface::GLSurface(const GLShape *shape, const GLMaterial *material,
                     const GLMatrix &matrix) :
    shape(shape), material(material), matrix(matrix) {
}

GLSurface::GLSurface(GLSurface &&surface) noexcept :
    shape(std::move(surface.shape)), material(std::move(material)),
    matrix(std::move(matrix)) {
}

GLSurface::~GLSurface(void) {}

GLSurface& GLSurface::operator=(GLSurface &&surface) noexcept {
    if (this != &surface) {
        std::swap(this->shape, surface.shape);
        std::swap(this->material, surface.material);
        std::swap(this->matrix, surface.matrix);
    }
    return *this;
}


