#include "glquad.h"
#include "glerror.h"
#include "glmacro.h"
#include <common/vec.h>

struct GLQuadVertex {
    vec2f position;
    vec2f texcoord;
};

static const GLQuadVertex gVertices[] = {
    { { -1.0f, -1.0f }, { 0.0f, 1.0f } },
    { {  1.0f, -1.0f }, { 1.0f, 1.0f } },
    { {  1.0f,  1.0f }, { 1.0f, 0.0f } },
    { { -1.0f,  1.0f }, { 0.0f, 0.0f } }
};

GLQuad::GLQuad() {}

GLQuad::GLQuad(GLQuad &&quad) noexcept {
    using std::swap;
    swap(vbo, quad.vbo);
}

GLQuad::~GLQuad() {
    tearDown();
}

GLQuad& GLQuad::operator=(GLQuad &&quad) noexcept {
    using std::swap;
    if (this == &quad) {
        tearDown();
        swap(vbo, quad.vbo);
    }
    return *this;
}

bool GLQuad::draw() {
    if (vbo == 0) {
        if(!setup()) {
            tearDown();
            return false;
        }
    }
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    RETURN_FALSE_IF_GL_ERROR("glBindBuffer");
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    RETURN_FALSE_IF_GL_ERROR("glDrawArrays");
    return true;
}

bool GLQuad::setup() {
    assert(vbo == 0);
    glGenBuffers(1, &vbo);
    RETURN_FALSE_IF_GL_ERROR("glGenBuffers");
    error_if(vbo == 0, "Invalid vertex buffer object");
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    RETURN_FALSE_IF_GL_ERROR("glBindBuffer");
    glBufferData(GL_ARRAY_BUFFER, sizeof(gVertices), gVertices, GL_STATIC_DRAW);
    RETURN_FALSE_IF_GL_ERROR("glBufferData");
    glVertexAttribPointer(positionAttribHandle, 2, GL_FLOAT, GL_FALSE, sizeof(GLQuadVertex),
                          MEMEBER_OFFSET_OF(GLQuadVertex, position));
    RETURN_FALSE_IF_GL_ERROR("glVertexAttribPointer");
    glEnableVertexAttribArray(positionAttribHandle);
    RETURN_FALSE_IF_GL_ERROR("glEnableVertexAttribArray");
    glVertexAttribPointer(texcoordAttribHandle, 2, GL_FLOAT, GL_FALSE, sizeof(GLQuadVertex),
                          MEMEBER_OFFSET_OF(GLQuadVertex, texcoord));
    RETURN_FALSE_IF_GL_ERROR("glVertexAttribPointer");
    glEnableVertexAttribArray(texcoordAttribHandle);
    RETURN_FALSE_IF_GL_ERROR("glEnableVertexAttribArray");
    return true;
}

void GLQuad::tearDown() {
    if (vbo) {
        glDeleteBuffers(1, &vbo);
        vbo = 0;
    }
}

void GLQuad::setPositionAttribHandle(GLint handle) {
    positionAttribHandle = handle;
    if (vbo) {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        RETURN_IF_GL_ERROR("glBindBuffer");
        glVertexAttribPointer(positionAttribHandle, 2, GL_FLOAT, GL_FALSE,
                              sizeof(GLQuadVertex),
                              MEMEBER_OFFSET_OF(GLQuadVertex, position));
        RETURN_IF_GL_ERROR("glVertexAttribPointer");
        glEnableVertexAttribArray(positionAttribHandle);
        RETURN_IF_GL_ERROR("glEnableVertexAttribArray");
    }
}

void GLQuad::setTexcoordAttribHandle(GLint handle) {
    texcoordAttribHandle = handle;
    if (vbo) {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        RETURN_IF_GL_ERROR("glBindBuffer");
        glVertexAttribPointer(texcoordAttribHandle, 2, GL_FLOAT, GL_FALSE,
                              sizeof(GLQuadVertex),
                              MEMEBER_OFFSET_OF(GLQuadVertex, texcoord));
        RETURN_IF_GL_ERROR("glVertexAttribPointer");
        glEnableVertexAttribArray(texcoordAttribHandle);
        RETURN_IF_GL_ERROR("glEnableVertexAttribArray");
    }
}

