#ifndef GL_OBJECT_H
#define GL_OBJECT_H

#include "glheader.h"

class GLObject {
public:
    GLObject() {}
    GLObject(const GLObject& shader) = delete;
    GLObject& operator=(const GLObject &shader) = delete;
};

#endif