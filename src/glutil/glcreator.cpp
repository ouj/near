#include "glcreator.h"
#include <scene/shape.h>
#include <scene/material.h>
#include <scene/scene.h>
#include <scene/transform.h>
using std::vector;


vector<vec3f> computeNormals(const TriangleMeshShape* mesh) {
    vector<vec3f> facenormals(mesh->face.size());
    for(int f = 0; f < mesh->face.size(); f ++) {
        const vec3i& face = mesh->face[f];
        facenormals[f] = triangleNormal(mesh->pos[face.x],
                                        mesh->pos[face.y],
                                        mesh->pos[face.z]);
    }
    // vertex to face list
    vector<vector<int>> vertexToFace(mesh->pos.size());
    for(int f = 0; f < mesh->face.size(); f ++) {
        vertexToFace[mesh->face[f].x].push_back(f);
        vertexToFace[mesh->face[f].y].push_back(f);
        vertexToFace[mesh->face[f].z].push_back(f);
    }
    // compute average normal
    vector<vec3f> norm(mesh->pos.size());
    for(int v = 0; v < norm.size(); v ++) {
        norm[v] = zero3f;
        for(size_t f = 0; f < vertexToFace[v].size(); f ++) {
            norm[v] += facenormals[vertexToFace[v][f]];
        }
        norm[v] *= 1.0f / vertexToFace[v].size();
    }
    return norm;
}

void tesselateSphere(vector<GLShapeVertex> &vertices, vector<vec3i> &face,
                     GLfloat radius, GLint slices = 32, GLint stacks = 16) {
    int nvertex = (slices+1) * (stacks+1);
    int ntriangles = slices * stacks * 2;
    
    vertices.resize(nvertex);
    face.resize(ntriangles);
    
    for(int t = 0; t <= stacks; t ++) {
        for(int p = 0; p <= slices; p ++) {
            int index = p+t*(slices+1);
            float u = float(p) / float(slices);
            float v = float(t) / float(stacks);
            float phi = u * 2 * consts<float>::pi;
            float theta = v * consts<float>::pi;
            vec3f r = vec3f(sin(phi)*sin(theta), cos(phi)*sin(theta), cos(theta));
            r = normalize(r);
            vertices.at(index) = GLShapeVertex{r * radius, r, vec2f(u, v)};
        }
    }
    
    int count = 0;
    for(int t = 0; t < stacks; t ++) {
        for(int p = 0; p < slices; p ++) {
            face.at(count).x = (p + 0) + (t + 0) * (slices + 1);
            face.at(count).y = (p + 1) + (t + 0) * (slices + 1);
            face.at(count).z = (p + 1) + (t + 1) * (slices + 1);
            count++;
            face.at(count).x = (p + 0) + (t + 0) * (slices + 1);
            face.at(count).y = (p + 1) + (t + 1) * (slices + 1);
            face.at(count).z = (p + 0) + (t + 1) * (slices + 1);
            count++;
        }
    }
}

void tesselateQuad(vector<GLShapeVertex> &vertices, vector<vec3i> &face,
                   int width, int height) {
    float w = width * 0.5f;
    float h = height * 0.5f;
    
    vertices.reserve(4);
    face.reserve(2);
    vertices.emplace_back(vec3f(-w, -h, 0.0f), z3f, vec2f(-1.0f, -1.0f));
    vertices.emplace_back(vec3f( w, -h, 0.0f), z3f, vec2f( 1.0f, -1.0f));
    vertices.emplace_back(vec3f( w,  h, 0.0f), z3f, vec2f( 1.0f,  1.0f));
    vertices.emplace_back(vec3f(-w,  h, 0.0f), z3f, vec2f(-1.0f,  1.0f));
    face.emplace_back(0, 1, 2);
    face.emplace_back(0, 2, 3);
}

GLMatrix createGLWorldMatrix(const Transform *transform) {
    GLMatrix matrix;
    float *m = matrix.m;
    switch (transform->type) {
        case IdentityTransform::TYPEID: {
            m[ 0] = 1; m[ 1] = 0; m[ 2] = 0; m[ 3] = 0;
            m[ 4] = 0; m[ 5] = 1; m[ 6] = 0; m[ 7] = 0;
            m[ 8] = 0; m[ 9] = 0; m[10] = 1; m[11] = 0;
            m[12] = 0; m[13] = 0; m[14] = 0; m[15] = 1;
        } break;
        case RigidTransform::TYPEID: {
            frame3f f = ((const RigidTransform*)transform)->frame;
            m[ 0] = f.x.x; m[ 1] = f.x.y; m[ 2] = f.x.z; m[ 3] = 0;
            m[ 4] = f.y.x; m[ 5] = f.y.y; m[ 6] = f.y.z; m[ 7] = 0;
            m[ 8] = f.z.x; m[ 9] = f.z.y; m[10] = f.z.z; m[11] = 0;
            m[12] = f.o.x; m[13] = f.o.y; m[14] = f.o.z; m[15] = 1;
        } break;
        default: error("Unknow matrix type");
    }
    return matrix;
}

GLShape GLCreator::processShape(const Shape *shape,
                                GLint positionAttribHandle,
                                GLint normalAttribHandle,
                                GLint texcoordAttribHandle) {
    GLShape glShape;
    switch (shape->type) {
        case TriangleMeshShape::TYPEID: {
            const TriangleMeshShape* mesh = (const TriangleMeshShape*)shape;
            assert(!mesh->pos.empty());
            bool hasNormal = !mesh->norm.empty();
            bool hasTexcoord = !mesh->uv.empty();
            
            vector<GLShapeVertex> vertices;
            vertices.reserve(mesh->pos.size());
            vector<vec3f> norm;
            if (!hasNormal) {
                norm = computeNormals(mesh);
            }
            for (int i = 0; i < mesh->pos.size(); i++) {
                const vec3f &normal =  hasNormal ? mesh->norm.at(i) : norm[i];
                const vec2f &texcoord = hasTexcoord ? mesh->uv.at(i) : half2f;
                vertices.emplace_back(mesh->pos.at(i), normal, texcoord);
            }
            glShape.setup(vertices, mesh->face, positionAttribHandle,
                          normalAttribHandle, texcoordAttribHandle);
        } break;
        case SphereShape::TYPEID: {
            const SphereShape* sphere = (const SphereShape*)shape;
            vector<GLShapeVertex> vertices;
            vector<vec3i> face;
            tesselateSphere(vertices, face, sphere->radius);
            glShape.setup(vertices, face, positionAttribHandle,
                          normalAttribHandle, texcoordAttribHandle);
        } break;
        case QuadShape::TYPEID: {
            const QuadShape* quad = (const QuadShape*)shape;
            vector<GLShapeVertex> vertices;
            vector<vec3i> face;
            tesselateQuad(vertices, face, quad->width, quad->height);
            glShape.setup(vertices, face, positionAttribHandle,
                          normalAttribHandle, texcoordAttribHandle);
        } break;
        default:
            error("Unknown shape type");
            break;
    }
    return glShape;
}

GLMaterial GLCreator::processMaterial(const Material *material) {
    GLMaterial glMaterial;
    switch (material->type) {
        case LambertMaterial::TYPEID: {
            LambertMaterial *lambert = (LambertMaterial*)material;
            glMaterial.rhod = lambert->rhod;
            glMaterial.rhos = zero3f;
            glMaterial.n = 0.0f;
        } break;
        case MicrofacetMaterial::TYPEID: {
            MicrofacetMaterial *micro = (MicrofacetMaterial*)material;
            glMaterial.rhod = micro->rhod;
            glMaterial.rhos = micro->rhos;
            glMaterial.n = micro->n;
        } break;
        default: error("Material type not supported.");
    }
    return glMaterial;
}

GLScene&& GLCreator::process(const Scene* scene) {
    cleanup();
    
    vector<GLShape> &glShapes = glScene.shapes;
    glShapes.reserve(scene->shapes.size());
    for(Shape* shape : scene->shapes) {
        glShapes.push_back(processShape(shape));
        shapeMap[shape] = &glShapes.back();
    }
    vector<GLMaterial> &glMaterials = glScene.materials;
    glMaterials.reserve(scene->materials.size());
    for (Material* material : scene->materials) {
        glMaterials.push_back(processMaterial(material));
        materialMap[material] = &glMaterials.back();
    }
    vector<GLSurface> &surfaces = glScene.surfaces;
    for (Surface* surface : scene->surfaces) {
        GLShape* shape = shapeMap[surface->shape];
        GLMaterial *material = materialMap[surface->material];
        assert(shape && material);
        surfaces.emplace_back(shape, material,
                              createGLWorldMatrix(surface->transform));
    }
    return std::move(glScene);
}


void GLCreator::cleanup() {
    shapeMap.clear();
    materialMap.clear();
}


