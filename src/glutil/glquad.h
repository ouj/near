#ifndef GL_QUAD_H
#define GL_QUAD_H

#include "globject.h"

class GLQuad : GLObject {
public:
    GLQuad(void);
    GLQuad(GLQuad &&quad) noexcept;
    GLQuad& operator=(GLQuad &&quad) noexcept;
    ~GLQuad(void);
        
    void setPositionAttribHandle(GLint positionAttribHandle);
    void setTexcoordAttribHandle(GLint texcoordAttribHandle);
    void tearDown();
    bool draw();
private:
    bool setup();
    GLuint vbo = 0;
    GLint positionAttribHandle = -1;
    GLint texcoordAttribHandle = -1;
};

#endif