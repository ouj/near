#include "glscene.h"
#include <scene/shape.h>
#include <scene/material.h>
using std::vector;

GLScene::GLScene() {}

GLScene::GLScene(GLScene &&scene) noexcept :
    shapes(std::move(scene.shapes)), materials(std::move(scene.materials)),
    surfaces(std::move(surfaces)) {
}

GLScene& GLScene::operator=(GLScene &&scene) noexcept {
    if (this != &scene) {
        using std::swap;
        swap(shapes, scene.shapes);
        swap(materials, scene.materials);
        swap(surfaces, scene.surfaces);
    }
    return *this;
}

GLScene::~GLScene() {}
