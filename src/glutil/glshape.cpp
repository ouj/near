#include "glshape.h"
#include "glerror.h"
#include "glmacro.h"
#include <common/vec.h>

GLShape::GLShape() {}

GLShape::GLShape(GLShape &&shape) noexcept {
    using std::swap;
    swap(vbo, shape.vbo);
    swap(ibo, shape.ibo);
}

GLShape::~GLShape() {
    tearDown();
}

GLShape& GLShape::operator=(GLShape &&shape) noexcept {
    using std::swap;
    if (this != &shape) {
        tearDown();
        swap(vbo, shape.vbo);
        swap(ibo, shape.ibo);
    }
    return *this;
}

bool GLShape::setup(const std::vector<GLShapeVertex> &vertices,
                    const std::vector<vec3i> &indices,
                    GLint positionAttribHandle,
                    GLint normalAttribHandle,
                    GLint texcoordAttribHandle) {
    assert(vbo == 0 && ibo == 0 && vao == 0);
    glGenVertexArrays(1, &vao);
    RETURN_FALSE_IF_GL_ERROR("glGenVertexArrays");
    error_if(vao == 0, "Invalid vertex array object");
    glBindVertexArray(vao);
    RETURN_FALSE_IF_GL_ERROR("glBindVertexArray");
    
    glGenBuffers(1, &vbo);
    RETURN_FALSE_IF_GL_ERROR("glGenBuffers");
    error_if(vbo == 0, "Invalid vertex buffer object");
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    RETURN_FALSE_IF_GL_ERROR("glBindBuffer");
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLShapeVertex) * vertices.size(),
                 &vertices.front(), GL_STATIC_DRAW);
    RETURN_FALSE_IF_GL_ERROR("glBufferData");
    
    glVertexAttribPointer(positionAttribHandle, 2, GL_FLOAT, GL_FALSE,
                          sizeof(GLShapeVertex),
                          MEMEBER_OFFSET_OF(GLShapeVertex, position));
    RETURN_FALSE_IF_GL_ERROR("glVertexAttribPointer");
    glEnableVertexAttribArray(positionAttribHandle);
    RETURN_FALSE_IF_GL_ERROR("glEnableVertexAttribArray");
    
    glVertexAttribPointer(normalAttribHandle, 2, GL_FLOAT, GL_FALSE,
                          sizeof(GLShapeVertex),
                          MEMEBER_OFFSET_OF(GLShapeVertex, texcoord));
    RETURN_FALSE_IF_GL_ERROR("glVertexAttribPointer");
    glEnableVertexAttribArray(normalAttribHandle);
    RETURN_FALSE_IF_GL_ERROR("glEnableVertexAttribArray");
    
    glVertexAttribPointer(texcoordAttribHandle, 2, GL_FLOAT, GL_FALSE,
                          sizeof(GLShapeVertex),
                          MEMEBER_OFFSET_OF(GLShapeVertex, texcoord));
    RETURN_FALSE_IF_GL_ERROR("glVertexAttribPointer");
    glEnableVertexAttribArray(texcoordAttribHandle);
    RETURN_FALSE_IF_GL_ERROR("glEnableVertexAttribArray");
    
    glGenBuffers(1, &ibo);
    RETURN_FALSE_IF_GL_ERROR("glGenBuffers");
    error_if(ibo == 0, "Invalid index buffer object");
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    RETURN_FALSE_IF_GL_ERROR("glBindBuffer");
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vec3i) * indices.size(),
                 &indices.front(), GL_STATIC_DRAW);
    RETURN_FALSE_IF_GL_ERROR("glBufferData");
    
    triangleNum = indices.size();
    return true;
}

void GLShape::tearDown() {
    if (vbo) {
        glDeleteBuffers(1, &vbo);
        CHECK_GL_ERROR("glDeleteBuffers");
        vbo = 0;
    }
    if (ibo) {
        glDeleteBuffers(1, &ibo);
        CHECK_GL_ERROR("glDeleteBuffers");
        ibo = 0;
    }
    if (vao) {
        glDeleteVertexArrays(1, &vao);
        CHECK_GL_ERROR("glDeleteVertexArrays");
        vao = 0;
    }
}

bool GLShape::draw() {
    glBindVertexArray(vao);
    RETURN_FALSE_IF_GL_ERROR("glBindVertexArray");
    glDrawElements(GL_TRIANGLES, triangleNum * 3, GL_UNSIGNED_INT, 0);
    RETURN_FALSE_IF_GL_ERROR("glDrawElements");
    return true;
}
