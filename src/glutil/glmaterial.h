#ifndef GL_MATERIAL_H
#define GL_MATERIAL_H

#include "globject.h"
#include <common/vec.h>

class GLMaterial {
public:
    vec3f rhod;
    vec3f rhos;
    float n;
};

#endif