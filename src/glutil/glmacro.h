#ifndef GL_MACRO_H
#define GL_MACRO_H

#define GLSL_SHADER_SOURCE(...) #__VA_ARGS__
#define MEMEBER_OFFSET_OF(STRUCT, MEMBER) (&((STRUCT*)0)->MEMBER)

#endif