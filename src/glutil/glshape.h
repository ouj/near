#ifndef GL_SHAPE_H
#define GL_SHAPE_H

#include "globject.h"
#include <common/vec.h>
#include <vector>

struct GLShapeVertex {
    GLShapeVertex() {}
    GLShapeVertex(const vec3f &p, const vec3f &n, const vec2f &t)
        : position(p), normal(n), texcoord(t) {}
    vec3f position;
    vec3f normal;
    vec2f texcoord;
};

class GLShape : GLObject {
public:
    GLShape(void);
    GLShape(GLShape &&shader) noexcept;
    GLShape& operator=(GLShape &&shader) noexcept;
    ~GLShape(void);
    
    bool setup(const std::vector<GLShapeVertex> &vertices,
               const std::vector<vec3i> &indices, GLint positionAttribHandle,
               GLint normalAttribHandle, GLint texcoordAttribHandle);
    void tearDown();
    bool draw();
private:
    GLuint vbo = 0;
    GLuint ibo = 0;
    GLuint vao = 0;
    GLuint triangleNum = 0;
};

#endif