#include "glshader.h"
#include "glerror.h"
#include <vector>

GLShader::GLShader() {}

GLShader::GLShader(GLShader &&shader) noexcept {
    using std::swap;
    swap(_shader, shader._shader);
}

GLShader::~GLShader() {
    tearDown();
}

GLShader& GLShader::operator=(GLShader &&shader) noexcept {
    using std::swap;
    if (this != &shader) {
        tearDown();
        swap(_shader, shader._shader);
    }
    return *this;
}

bool GLShader::compile(const GLenum shaderType, const char *source) {
    error_if(source == 0, "invalid shader source");
    if (_shader != 0) tearDown();
    assert(_shader == 0);
    _shader = glCreateShader(shaderType);
    RETURN_FALSE_IF_GL_ERROR("glCreateShader");
    if (!_shader) {
        error("failed to create shader");
        return false;
    }

    glShaderSource(_shader, 1, &source, 0);
    RETURN_FALSE_IF_GL_ERROR("glShaderSource");
    glCompileShader(_shader);
    CHECK_GL_ERROR("glCompileShader");
#ifdef GL_ERROR_CHECK
    GLint logLength = 0, maxLength = 0;
    glGetShaderiv(_shader, GL_INFO_LOG_LENGTH, &maxLength);
    std::vector<GLchar> log(maxLength);
    glGetShaderInfoLog(_shader, maxLength, &logLength, &log.front());
    if (logLength > 0) {
        message_va("shader compile log:\n%s", &log.front());
        return false;
    }
#endif
    return true;
}

void GLShader::tearDown() {
    if (_shader) {
        glDeleteShader(_shader);
        _shader = 0;
    }
}



