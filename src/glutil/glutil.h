#ifndef GL_UTIL_H
#define GL_UTIL_H

#include "glmacro.h"
#include "glerror.h"
#include "glprogram.h"
#include "glquad.h"
#include "glscene.h"
#include "glshader.h"
#include "glshape.h"
#include "glsurface.h"
#include "glcreator.h"

#endif