#include "glprogram.h"
#include "glerror.h"
#include <vector>

GLProgram::GLProgram(void) {}

GLProgram::GLProgram(GLProgram &&program) noexcept {
    using std::swap;
    swap(_program, program._program);
}

GLProgram::~GLProgram() {
    tearDown();
}

GLProgram& GLProgram::operator=(GLProgram &&program) noexcept {
    using std::swap;
    if (this != &program) {
        tearDown();
        swap(_program, program._program);
    }
    return *this;
}

bool GLProgram::attachShaders(const GLShader& shader) {
    if (_program == 0) {
        _program = glCreateProgram();
        if (!_program) {
            error("failed to create program");
            return false;
        }
    }
	glAttachShader(_program, shader._shader);
    RETURN_FALSE_IF_GL_ERROR("glAttachShader");
    return true;
}

bool GLProgram::link(const Binding &bindings) {
    error_if(!_program, "gl program is not craeted");
    if (!bindings.empty()) {
        for (auto b : bindings) {
            glBindAttribLocation(_program, b.second, b.first.c_str());
            RETURN_FALSE_IF_GL_ERROR("glBindAttribLocation");
        }
    }
    glLinkProgram(_program);
    RETURN_FALSE_IF_GL_ERROR("glLinkProgram");
#ifdef GL_ERROR_CHECK
    GLint logLength = 0, maxLength = 0;
    glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &maxLength);
    std::vector<GLchar> log(maxLength);
    glGetProgramInfoLog(_program, maxLength, &logLength, &log.front());
    if(logLength > 0) {
        message_va("program info log: %s\n", &log.front());
        return false;
    }
#endif
    return true;
}

bool GLProgram::isValid() {
    if (_program == 0) {
        return false;
    }
    glValidateProgram(_program);
    CHECK_GL_ERROR("glValidateProgram");
    GLint valid;
    glGetProgramiv(_program, GL_VALIDATE_STATUS, &valid);
    return valid;
}

bool GLProgram::use() {
    glUseProgram(_program);
    RETURN_FALSE_IF_GL_ERROR("glUseProgram");
    return true;
}

GLint GLProgram::getUniformLocation(const GLchar *name) {
    GLint uniform = glGetUniformLocation(_program, name);
    if (uniform < 0) {
        warning_va("invalid uniform location for %s", name);
    }
    return uniform;
}

GLint GLProgram::getAttributeLocation(const GLchar *name) {
    GLint attribute = glGetAttribLocation(_program, name);
    if (attribute < 0) {
        warning_va("invalid attribute location for %s", name);
    }
    return attribute;
}

void GLProgram::tearDown() {
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}



