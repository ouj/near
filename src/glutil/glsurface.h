#ifndef GL_SURFACE_H
#define GL_SURFACE_H

#include "globject.h"
#include "glshape.h"
#include "glmaterial.h"
#include "glmatrix.h"

class GLSurface : GLObject {
public:
    GLSurface(const GLShape *shape, const GLMaterial *material,
              const GLMatrix &matrix);
    GLSurface(GLSurface &&shader) noexcept;
    GLSurface& operator=(GLSurface &&shader) noexcept;
    ~GLSurface(void);

    const GLShape       *shape;
    const GLMaterial    *material;
    GLMatrix            matrix;
};

#endif