#ifndef GL_PROGRAM_H
#define GL_PROGRAM_H

#include "glshader.h"
#include <vector>
#include <utility>
#include <string>

typedef std::vector<std::pair<std::string, GLuint>> Binding;

class GLProgram : GLObject {
public:
    GLProgram(void);
    GLProgram(GLProgram &&program) noexcept;
    GLProgram& operator=(GLProgram &&shader) noexcept;
    ~GLProgram(void);

    bool attachShaders(const GLShader &shader);
    template <typename ...Args>
    bool attachShaders(const GLShader &shader, Args &&...args);

    bool link(const Binding &bindings = Binding());
    bool setup();
    void tearDown();

    GLint getUniformLocation(const GLchar *name);
    GLint getAttributeLocation(const GLchar *name);

    bool isValid();
    bool use();

    GLuint _program = 0;
};

template <typename ...Args>
bool GLProgram::attachShaders(const GLShader &shader, Args &&...args) {
    if(!attachShaders(shader)) return false;
    return attachShaders(std::forward<Args>(args)...);
}

#endif

