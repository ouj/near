#ifndef GL_SHADER_H
#define GL_SHADER_H

#include "globject.h"

class GLShader : GLObject {
public:
    GLShader(void);
    GLShader(GLShader &&shader) noexcept;
    GLShader& operator=(GLShader &&shader) noexcept;
    ~GLShader(void);

    bool compile(const GLenum shaderType, const char *source);
    void tearDown();

    GLuint _shader = 0;
};

#endif