#ifndef GPU_BVH_ACCELERATOR_H
#define GPU_BVH_ACCELERATOR_H

#include <accelerator/accelerator.h>
#include <common/stdheader.h>
#include <common/debug.h>
#include <scene/sceneio.h>
#include <functional>
#include "gray.h"

typedef cl_float4 gintersection;

struct GpuBvhAcceleratorOpts : AcceleratorOpts {
    GpuBvhAcceleratorOpts();
    virtual jsonObject  print() const;
    virtual void        parse(const jsonObject &opts);
    bool profiling = true;
    int maxLeafItems = 8;
};

typedef cl_float8 GpuBvhNode;

struct IntersectFirstTask {
    virtual const std::vector<ray3f> getRays() const = 0;
    virtual void intersectionHandler(std::vector<intersection3f> &intersections) = 0;
};

class GpuBvhAccelerator {
public:
    const static int TYPEID = 200901;
    GpuBvhAccelerator();
    GpuBvhAccelerator(const cl::Context &context, const cl::CommandQueue &commandQueue);
    virtual ~GpuBvhAccelerator();

    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces, const std::vector<Light*>& lights);
    virtual range3f     computeWorldBound() const { return _worldBound; }
    virtual void        clearAccelerator();

    virtual bool        intersectFirst(const std::vector<ray3f> &rays,
                                       std::vector<intersection3f> &intersections) const;
    virtual bool        intersectFirst(std::shared_ptr<IntersectFirstTask> task) const;
    virtual bool        intersectAny(const std::vector<ray3f> &rays, std::vector<uint> &hits) const;
    inline const cl::Context           clContext() const { return _context; }
    inline const cl::CommandQueue      clCommandQueue() const { return _commandQueue; }
private:
    virtual void        resolveIntersections(const std::vector<ray3f> &rays,
                                             const std::vector<gintersection> &gintersections,
                                             std::vector<intersection3f> &intersections) const;
    bool                buildBvhAccelerator();
    void                collectProfilingDataIF(const cl::Event &writeEvent, const cl::Event &kernelEvent,
                                               const cl::Event &readEvent, size_t rayNum) const;
    void                collectProfilingDataIA(const cl::Event &writeEvent, const cl::Event &kernelEvent,
                                               const cl::Event &readEvent, size_t rayNum) const;
    void                printProfilingData();
    cl::Context                                 _context;
    cl::CommandQueue                            _commandQueue;

    mutable cl::Kernel                          _firstKernel;
    mutable cl::Kernel                          _anyKernel;
    cl::Buffer                                  _nodeBuffer;
    cl::Buffer                                  _faceBuffer;
    cl::Buffer                                  _verticeBuffer;
    cl::Buffer                                  _triangleIdBuffer;

    // tempoary vectors
    std::vector<GpuBvhNode>                     _nodes;
    std::vector<vec3i>                          _faces;
    std::vector<vec3f>                          _vertices;
    std::vector<vec2u>                          _triangleIds;

    std::vector<AcceleratorSurface*>            _surfaces;
    range3f                                     _worldBound;

    GpuBvhAcceleratorOpts                       _opts;

    // statistic
    mutable cl_ulong                            _ifTotalElased = 0;
    mutable cl_ulong                            _iaTotalElased = 0;

    mutable cl_ulong                            _ifExeElased = 0;
    mutable cl_ulong                            _iaExeElased = 0;

    mutable size_t                              _ifRayCount = 0;
    mutable size_t                              _iaRayCount = 0;

    mutable size_t                              _h2dBytes = 0;
    mutable cl_ulong                            _h2dElasped = 0;
    mutable size_t                              _d2hBytes = 0;
    mutable cl_ulong                            _d2hElasped = 0;
};

#endif
