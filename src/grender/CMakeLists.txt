SET(GRENDER_HDR
gaccelerator_bvh.h
gpath.h
gray.h
)

SET(GRENDER_CPP
gaccelerator_bvh.cpp
gaccelerator_bvh_build.cpp
gpath.cpp
)

SET(GRENDER_CL
kernels/speculative_while_while.cl
kernels/intersection.cl
kernels/simple.cl
)

add_library(grender
${GRENDER_CPP}
${GRENDER_HDR}
${GRENDER_CL}
)

SOURCE_GROUP("kernels" FILES ${GRENDER_CL})

target_link_libraries(grender render clutil scene accelerator)
