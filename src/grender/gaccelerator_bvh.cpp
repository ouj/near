#include "gaccelerator_bvh.h"

#include <scene/transform.h>
#include <scene/shape.h>
#include <algorithm>
#include <tbb/tbb.h>
#include <tuple>

using namespace std;

#define WHILE_WHILE

#ifdef WHILE_WHILE
static const char traceSource[] = ""
#include "kernels/speculative_while_while.cl"
;
#else
static const char traceSource[] = ""
#include "kernels/simple.cl"
;
#endif

GpuBvhAcceleratorOpts::GpuBvhAcceleratorOpts() : AcceleratorOpts(GpuBvhAccelerator::TYPEID) {}

jsonObject GpuBvhAcceleratorOpts::print() const {
    jsonObject json;
    json["maxLeafItems"] = maxLeafItems;
    json["profiling"] = true;
    return json;
}

void GpuBvhAcceleratorOpts::parse(const jsonObject &json) {
    if (jsonHas(json, "maxLeafItems")) maxLeafItems = jsonGet(json, "maxLeafItems");
    if (jsonHas(json, "profiling")) profiling = jsonGet(json, "profiling");
}

GpuBvhAccelerator::GpuBvhAccelerator() {
    cl_int err = CL_SUCCESS;

    std::vector<cl::Platform> platforms;
    std::vector<cl::Device> devices;
    err = cl::Platform::get(&platforms);
    RETURN_IF_CL_ERROR(err, "cl::Platform::get()");
    if (platforms.empty()) {
        error("OpenCL cannot initialzie platform");
        return;
    }

    // Try to get a GPU device
    const cl::Platform &platform = platforms[0];
    platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
    if (devices.empty()) {
        warning("not available gpu, fall back to all devices");
        err = platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
        RETURN_IF_CL_ERROR(err, "getDevices");
        if (devices.empty()) {
            error("OpenCL cannot initialzie devices");
            return;
        }
    }

    // create cl context
    cl_context_properties properties[3] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)platform(),
        0
    };
    _context = cl::Context(devices, properties, nullptr, nullptr, &err);
    RETURN_IF_CL_ERROR(err, "construct context");

    cl_command_queue_properties cq_properties = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE;
    if (_opts.profiling) {
        cq_properties |= CL_QUEUE_PROFILING_ENABLE;
    }
    _commandQueue = cl::CommandQueue(_context, devices[0], cq_properties, &err);
    RETURN_IF_CL_ERROR(err, "construct command queue");
}


GpuBvhAccelerator::GpuBvhAccelerator(const cl::Context &context,
                                     const cl::CommandQueue &commandQueue)
        : _context(context), _commandQueue(commandQueue) {}

GpuBvhAccelerator::~GpuBvhAccelerator() { printProfilingData(); }

bool GpuBvhAccelerator::buildAccelerator(const std::vector<Surface*>& surfaces,
                                         const std::vector<Light*>& lights) {
    _surfaces = AcceleratorCommon::buildSurfaces(surfaces, decltype(lights){}, true);
    if (_surfaces.size() == 0) {
        _worldBound = range3f(zero3f, zero3f);
        return true;
    }
    if (!buildBvhAccelerator()) {
        error("Failed to build cpu bvh");
        return false;
    }
    size_t bytes = size_in_bytes(_nodes) + size_in_bytes(_faces) +
                   size_in_bytes(_vertices) + size_in_bytes(_triangleIds);

    message_va("CPU BVH memory size: %zu", bytes);
    // init buffers
    cl_int err = CL_SUCCESS;
    _nodeBuffer = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                             _nodes.size() * sizeof(GpuBvhNode),
                             _nodes.data(), &err);
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer (nodeBuffer");
    _faceBuffer = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                             _faces.size() * sizeof(vec3i),
                             _faces.data(), &err);
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer (faceBuffer)");
    _verticeBuffer = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                _vertices.size() * sizeof(vec3f),
                                _vertices.data(), &err);
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer (verticeBuffer");
    assert(sizeof(vec2u) == sizeof(cl_uint2));
    _triangleIdBuffer = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                   _triangleIds.size() * sizeof(vec2u),
                                   _triangleIds.data(), &err);
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer (triangleIds");

    // init kernels
    vector<cl::Device> devices = _context.getInfo<CL_CONTEXT_DEVICES>();

    cl::Program::Sources sources = { { traceSource, sizeof(traceSource) } };
    cl::Program program { _context, sources, &err };

    RETURN_FALSE_IF_CL_ERROR(err, "Program::Program()");
    err = program.build(devices);
    if (err != CL_SUCCESS) {
        std::string log =
            program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices.front());
        error_va("Compile error %s", log.c_str());
        return false;
    }
    _firstKernel = cl::Kernel(program, "intersectFirst", &err);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::Kernel()");
    _anyKernel = cl::Kernel(program, "intersectAny", &err);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::Kernel()");
    return true;
}

void GpuBvhAccelerator::clearAccelerator() {}

bool GpuBvhAccelerator::intersectFirst(const vector<ray3f> &rays,
                                       vector<intersection3f> &intersections) const {
    if (rays.empty()) return true;
    cl_int err = CL_SUCCESS;

    // create gpu buffer
    cl::Buffer rayBuffer { _context, CL_MEM_READ_ONLY,
                           size_in_bytes(rays), nullptr, &err };
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer()");
    cl::Buffer isectBuffer { _context, CL_MEM_WRITE_ONLY,
                             rays.size() * sizeof(gintersection), nullptr, &err };
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer()");

    // host to device data transfer
    cl::Event writeEvent;
    err = _commandQueue.enqueueWriteBuffer(rayBuffer, CL_NON_BLOCKING, 0,
                                           rays.size() * sizeof(ray3f),
                                           rays.data(), NULL, &writeEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueWriteBuffer()");

    // set kernel arguments
    err = _firstKernel.setArg(0, rayBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(1, _nodeBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(2, _faceBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(3, _verticeBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(4, isectBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");

    // execute kernel
    vector<cl::Event> kernelWaitList = { writeEvent };
    cl::Event kernelEvent;
    err = _commandQueue.enqueueNDRangeKernel(_firstKernel, 0, rays.size(),
                                             cl::NullRange, &kernelWaitList,
                                             &kernelEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueNDRangeKernel()");

    // device to host data transfer
    vector<gintersection> gintersections { rays.size() };
    vector<cl::Event> readWaitList = { kernelEvent };
    cl::Event readEvent;
    err = _commandQueue.enqueueReadBuffer(isectBuffer, CL_BLOCKING, 0,
                                          gintersections.size() * sizeof(gintersection),
                                          gintersections.data(), &readWaitList,
                                          &readEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueReadBuffer()");
    readEvent.wait();
    RETURN_FALSE_IF_CL_ERROR(err, "Event::wait()");
    resolveIntersections(rays, gintersections, intersections);
    collectProfilingDataIF(writeEvent, kernelEvent, readEvent, rays.size());
    return true;
}

static void CL_CALLBACK intersectFirstCallBack(cl_event event, cl_int status, void *data) {
    std::function<void()> *fn = (std::function<void()>*)data;
    (*fn)();
    delete fn;
}

bool GpuBvhAccelerator::intersectFirst(std::shared_ptr<IntersectFirstTask> task) const {
    const vector<ray3f> &rays = task->getRays();
    if (rays.empty()) return true;
    cl_int err = CL_SUCCESS;

    // create gpu buffer
    cl::Buffer rayBuffer { _context, CL_MEM_READ_ONLY, size_in_bytes(rays), nullptr, &err };
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer()");
    cl::Buffer isectBuffer { _context, CL_MEM_WRITE_ONLY,
                             rays.size() * sizeof(gintersection), nullptr, &err };
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer()");

    // host to device data transfer
    cl::Event writeEvent;
    err = _commandQueue.enqueueWriteBuffer(rayBuffer, CL_NON_BLOCKING, 0,
                                           rays.size() * sizeof(ray3f),
                                           rays.data(), NULL, &writeEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueWriteBuffer()");

    // set kernel arguments
    err = _firstKernel.setArg(0, rayBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(1, _nodeBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(2, _faceBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(3, _verticeBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _firstKernel.setArg(4, isectBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");

    // execute kernel
    vector<cl::Event> kernelWaitList = { writeEvent };
    cl::Event kernelEvent;
    err = _commandQueue.enqueueNDRangeKernel(_firstKernel, 0, rays.size(),
                                             cl::NullRange, &kernelWaitList,
                                             &kernelEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueNDRangeKernel()");

    shared_ptr<vector<gintersection>> gintersections { new vector<gintersection> { rays.size() } };
    // device to host data transfer
    vector<cl::Event> readWaitList = { kernelEvent };
    cl::Event readEvent;
    err = _commandQueue.enqueueReadBuffer(isectBuffer, CL_NON_BLOCKING, 0,
                                          gintersections->size() * sizeof(gintersection),
                                          gintersections->data(), &readWaitList,
                                          &readEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueReadBuffer()");

    auto fn = new std::function<void()>([this, gintersections, task]()->void {
        vector<intersection3f> intersections;
        resolveIntersections(task->getRays(), *gintersections, intersections);
        task->intersectionHandler(intersections);
    });
    err = readEvent.setCallback(CL_COMPLETE, intersectFirstCallBack, fn);
    RETURN_FALSE_IF_CL_ERROR(err, "Event::setCallback()");
    return true;
}

void GpuBvhAccelerator::resolveIntersections(const vector<ray3f> &rays,
                                             const vector<gintersection> &gintersections,
                                             vector<intersection3f> &intersections) const {
    assert(rays.size() == gintersections.size());

    intersections.resize(gintersections.size());
    tbb::parallel_for(tbb::blocked_range<int>(0, gintersections.size()),
                      [&](tbb::blocked_range<int> &range)->void{
        for (int i = range.begin(); i != range.end(); i++) {
            const gintersection &gisect = gintersections[i];
            int index = __as_int(gisect.s[0]);
            if (index < 0) { // missed
                intersections[i] = intersection3f();
                continue;
            }

            float t = gisect.s[1];
            vec2f uv = { gisect.s[2], gisect.s[3] };
            const ray3f &ray = rays[i];

            vec2u triId = _triangleIds[index];
            AcceleratorSurface *surf = _surfaces[triId.x];
            assert(surf->shape->type == TriangleMeshShape::TYPEID);
            TriangleMeshShape *mesh = (TriangleMeshShape*)surf->shape;

            const vec3i& f = mesh->face[triId.y];
            const vec3f& v0 = mesh->pos[f.x];
            const vec3f& v1 = mesh->pos[f.y];
            const vec3f& v2 = mesh->pos[f.z];

            intersection3f isect;
            isect.f.o = ray.eval(t);
            if (mesh->norm.empty()) {
                isect.f.z = triangleNormal(mesh->pos[f.x],mesh->pos[f.y],mesh->pos[f.z]);
            } else {
                isect.f.z = normalize(lerp(mesh->norm[f.x], mesh->norm[f.y], mesh->norm[f.z], uv));
            }
            isect.f.z = transformNormal(surf->transform, isect.f.z);
            if (dot(isect.f.z, ray.d) > 0) {
                isect.f.z = -isect.f.z;
            }
            xyFromZ(isect.f.z, isect.f.x, isect.f.y);
            if(mesh->uv.empty()) isect.st = uv;
            else isect.st =lerp(mesh->uv[f.x],mesh->uv[f.y],mesh->uv[f.z],uv);
            isect.m = surf->material;
            intersections[i] = isect;
        }
    });
}

bool GpuBvhAccelerator::intersectAny(const std::vector<ray3f> &rays,
                                     std::vector<cl_uint> &hits) const {
   if (rays.empty()) return true;
    cl_int err = CL_SUCCESS;

    // create gpu buffer
    cl::Buffer rayBuffer { _context, CL_MEM_READ_ONLY, size_in_bytes(rays), nullptr, &err };
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer()");
    cl::Buffer hitBuffer { _context, CL_MEM_WRITE_ONLY, rays.size() * sizeof(cl_uint), nullptr, &err };
    RETURN_FALSE_IF_CL_ERROR(err, "Buffer::Buffer()");

    // host to device data transfer
    cl::Event writeEvent;
    err = _commandQueue.enqueueWriteBuffer(rayBuffer, CL_NON_BLOCKING, 0,
                                           rays.size() * sizeof(ray3f),
                                           rays.data(), NULL, &writeEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueWriteBuffer()");

    // set kernel arguments
    err = _anyKernel.setArg(0, rayBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _anyKernel.setArg(1, _nodeBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _anyKernel.setArg(2, _faceBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _anyKernel.setArg(3, _verticeBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");
    err = _anyKernel.setArg(4, hitBuffer);
    RETURN_FALSE_IF_CL_ERROR(err, "Kernel::setArg()");

    // execute kernel
    vector<cl::Event> kernelWaitList = { writeEvent };
    cl::Event kernelEvent;
    err = _commandQueue.enqueueNDRangeKernel(_anyKernel, 0, rays.size(),
                                             cl::NullRange, &kernelWaitList,
                                             &kernelEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueNDRangeKernel()");

    // device to host data transfer
    hits.resize(rays.size(), 0);
    vector<cl::Event> readWaitList = { kernelEvent };
    cl::Event readEvent;
    err = _commandQueue.enqueueReadBuffer(hitBuffer, CL_BLOCKING, 0,
                                          hits.size() * sizeof(uint),
                                          hits.data(), &readWaitList,
                                          &readEvent);
    RETURN_FALSE_IF_CL_ERROR(err, "CommandQueue::enqueueReadBuffer()");
    readEvent.wait();
    RETURN_FALSE_IF_CL_ERROR(err, "Event::wait()");
    collectProfilingDataIA(writeEvent, kernelEvent, readEvent, rays.size());
    return true;
}

static void printEvent(const char* msg, const cl::Event &event) {
    cl_ulong qtime = event.getProfilingInfo<CL_PROFILING_COMMAND_QUEUED>();
    cl_ulong stime = event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    cl_ulong etime = event.getProfilingInfo<CL_PROFILING_COMMAND_END>();
    message_va("%s Timestamp-> [Queued: %.2fms | Start: %.2fms | End: %.2fms]", msg,
               nanoToMilli((double)qtime), nanoToMilli((double)stime),
               nanoToMilli((double)etime));
    message_va("%s Times-> [Wait: %.2fms | Execute: %.2fms | Total: %.2fms]", msg,
               nanoToMilli((double)stime - qtime), nanoToMilli((double)etime - stime),
               nanoToMilli((double)etime - qtime));
}

void GpuBvhAccelerator::collectProfilingDataIF(const cl::Event &writeEvent, const cl::Event &kernelEvent,
                                               const cl::Event &readEvent, size_t rayNum) const {
    _ifTotalElased += readEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                      writeEvent.getProfilingInfo<CL_PROFILING_COMMAND_QUEUED>();
    _ifExeElased += kernelEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                    kernelEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    _h2dElasped += writeEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                   writeEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    _d2hElasped += readEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                   readEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    _ifRayCount += rayNum;
    _h2dBytes += sizeof(ray3f) * rayNum;
    _d2hBytes += sizeof(gintersection) * rayNum;
}

void GpuBvhAccelerator::collectProfilingDataIA(const cl::Event &writeEvent, const cl::Event &kernelEvent,
                                               const cl::Event &readEvent, size_t rayNum) const {
    _iaTotalElased += readEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                      writeEvent.getProfilingInfo<CL_PROFILING_COMMAND_QUEUED>();
    _iaExeElased += kernelEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                    kernelEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    _h2dElasped += writeEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                   writeEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    _d2hElasped += readEvent.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                   readEvent.getProfilingInfo<CL_PROFILING_COMMAND_START>();
    _iaRayCount += rayNum;
    _h2dBytes += sizeof(ray3f) * rayNum;
    _d2hBytes += sizeof(gintersection) * rayNum;
}

void GpuBvhAccelerator::printProfilingData() {
    message("GPU profiling data");
    message("====================================");
    message("Data Transfer:");
    message_va("Host to Device: \t%.2f MB/s",
               byteToMB((double)_h2dBytes) / nanoToSecond((double)_h2dElasped));
    message_va("Device to Host: \t%.2f MB/s",
               byteToMB((double)_d2hBytes) / nanoToSecond((double)_d2hElasped));
    message("-------------");
    message("Quary Rays");
    message_va("Trace: \t%.2f Million Rays/s",
               _ifRayCount / consts<double>::million /  nanoToSecond((double)_ifExeElased));
    message_va("Total: \t%.2f Million Rays/s",
               _ifRayCount / consts<double>::million /  nanoToSecond((double)_ifTotalElased));
    message("-------------");
    message("Shadow Rays");
    message_va("Trace: \t%.2f Million Rays/s",
               _iaRayCount / consts<double>::million / nanoToSecond((double)_iaExeElased));
    message_va("Total: \t%.2f Million Rays/s",
               _iaRayCount / consts<double>::million /  nanoToSecond((double)_iaTotalElased));
    message("-------------");
}
