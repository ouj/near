#include "gpath.h"

#include <integration/opts.h>
#include <integration/utils.h>
#include <tbb/mutex.h>

namespace options {
    namespace gpath {
        constexpr bool russianroulette = false;
    }
}
    
using namespace std;
using namespace tbb;

GpuPathRenderOpts::GpuPathRenderOpts(): RenderOpts(GpuPathRender::TYPEID) {}

jsonObject GpuPathRenderOpts::print() const {
    jsonObject json;
    json["type"] = "gpupathrenderopts";
    json["general"] = printGeneralOpts(general);
    json["imagesampling"] = printImageSamplingOpts(imageSampling);
    json["maxdepth"] = maxdepth;
    return json;
}

void GpuPathRenderOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("gpupathrenderopts");
    if (jsonHas(json, "general")) general = parseGeneralOpts(jsonGet(json,"general"));
    if (jsonHas(json, "imagesampling"))
        imageSampling = parseImageSamplingOpts(jsonGet(json, "imagesampling"));
    if (jsonHas(json, "maxdepth")) maxdepth = jsonGet(json, "maxdepth");
}

GpuPathRender::GpuPathRender(const GpuPathRenderOpts &opts) : _opts(opts) {
    assert(opts.type == TYPEID);
}

GpuPathRender::GpuPathRender() {}

GpuPathRender::~GpuPathRender() { clearState(); }

bool GpuPathRender::initState(const Scene* scene, const GpuBvhAccelerator* accelerator) {
    assert(scene && accelerator);
    _scene = scene;
    _accelerator = accelerator;
    ray3f::epsilon = _scene->settings.rayepsilon;
    initLightSampling(_scene->lights);
    _opts.general.height = adjustedHeight(scene->cameras[0], _opts.general.width);

    _opts.imageSampling.blocksize = 256;
    _opts.imageSampling.batchsamples = 1u;
    initImageSamplingState(_imageSampling, _opts.imageSampling,
                           _opts.general.width, _opts.general.height);
    _blockDim.x = ceil((float)_opts.general.width / _opts.imageSampling.blocksize);
    _blockDim.y = ceil((float)_opts.general.height / _opts.imageSampling.blocksize);
    _blockNum = _blockDim.x * _blockDim.y;
    return true;
}

void GpuPathRender::clearState() {}

Image<vec3f> GpuPathRender::traceImage() {
    return _imageSampling.image;
}

bool resolveRay(const ray3f& ray, const intersection3f &intersection,
                const std::vector<Light*> &lights,
                frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs) {
    // init
    le = zero3f;
    rs = invalidReflectanceSample;

    // intersect
    if (intersection.m == nullptr) { // missed
        le = evalEnvironment(ray, lights);
        return false;
    }

    // shading frame
    f = intersection.f;
    wo = -ray.d;

    // emission
    EmissionSample es = sampleEmission(intersection.m,intersection.st);
    if(isvalid(es)) le = evalEmission(es,f,-ray.d);

    // reflectance
    rs = sampleReflectance(intersection.m,intersection.st);

    // done
    return true;
}

tuple<vec3f, ray3f> evalDirect(const ReflectanceSample& rs, const frame3f& f,
                               const vec3f& wo, const vec2f& ls, const Light* light) {
    // sample shadow
    ShadowSample ss = sampleShadow(light, f.o, ls);
    if(ss.le.iszero()) return make_tuple(zero3f, defaultray3f);

    // accumulate direct
    vec3f brdf = evalReflectance(rs,f,ss.ray.d,wo);
    float projection = dot(ss.ray.d, f.z);
    vec3f ld = ss.le * brdf * projection / ss.pdf;

    if(ld.iszero()) return make_tuple(zero3f, defaultray3f);
    else return make_tuple(ld, ss.ray);
}

tuple<vec3f, ray3f> evalDirect(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                               SampleSequence& seq, const std::vector<Light*>& lights) {
    LightSample ls = sampleLight(lights, next1f(seq));
    ray3f ray; vec3f ld;
    tie(ld, ray) = evalDirect(rs, f, wo, next2f(seq), ls.light);
    return make_tuple(ld / ls.pdf, ray);
}

struct CameraTraceTask : IntersectFirstTask {
    CameraTraceTask(GpuPathRender *renderer, vector<ray3f> &&rays, vector<PSample> &&samples)
        : renderer(renderer), rays(rays), samples(samples) {}
    CameraTraceTask(const CameraTraceTask &task) = delete;
    CameraTraceTask(CameraTraceTask &&task) noexcept
        : renderer(renderer), rays(std::move(rays)), samples(std::move(samples)) {}

    virtual const std::vector<ray3f> getRays() const { return rays; };
    virtual void intersectionHandler(std::vector<intersection3f> &intersections) {
        concurrent_vector<SSample>      shadowSamples;
        concurrent_vector<ray3f>        shadowRays;
        const Scene *scene = renderer->_scene;
        parallel_for(blocked_range<size_t>(0, intersections.size()),
                     [&](blocked_range<size_t> &r)->void {
            for (auto s = r.begin(); s != r.end(); s++) {
                const ray3f &ray = rays[s];
                const intersection3f &isect = intersections[s];
                PSample &ps = samples[s];
                vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
                if (!::resolveRay(ray, isect, scene->lights, f, wo, le, rs) || !isvalid(rs)) {
                    // renderer->accumulate(ps.pixel, le);
                    continue;
                }
                vec3f ld; ray3f shadowRay;
                tie(ld, shadowRay) = ::evalDirect(rs, f, wo, ps.seq, scene->lights);
                if (!ld.iszero()) {
                    shadowRays.push_back(shadowRay);
                    shadowSamples.push_back(SSample {ld, ps.pixel});
                }
            }
        });
        rays.reserve(0);
        samples.reserve(0);
    }
    GpuPathRender *renderer;
    vector<ray3f> rays;
    vector<PSample> samples;
};

void GpuPathRender::traceCamera(const Camera* camera, vector<PSample> &samples) {
    vector<ray3f> rays(samples.size());
    parallel_for(blocked_range<size_t>(0, samples.size()),
                 [&rays, &samples, camera, this](const blocked_range<size_t> &r)->void{
        for (auto s = r.begin(); s != r.end(); s++) {
            PSample &ps = samples[s];
            vec2f rs = next2f(ps.seq);
            uint i, j;
            tie(i, j) = ps.pixel.unpack();
            vec2f is = vec2f((i + rs.x) / _opts.general.width, (j + rs.y) / _opts.general.height);
            rays[s] = ::sampleCamera(camera, is);
        }
    });

    shared_ptr<CameraTraceTask> task;
    task.reset(new CameraTraceTask {this, std::move(rays), std::move(samples)});
    if (!_accelerator->intersectFirst(task)) {
        error("Failed trace camera rays");
    }
}

bool GpuPathRender::tracePass() {
//     for (int i = 0; i < 1; i++) {
//         vector<PSample> samples = emitBatch(_blockIndex);
//         traceCamera(_scene->cameras[0], samples);
//         _blockIndex++;
//         if (_blockIndex == _blockNum) _blockIndex = 0;
//     }
//     sleep(1);
//     return true;
    return !sampleFrame(_scene->cameras[0]);;
}

vector<PSample> GpuPathRender::emitBatch(uint blockIndex) {
    uint width = _opts.general.width;
    uint height = _opts.general.height;
    uint blockSize = _opts.imageSampling.blocksize;
    vec2u b = to2D(_blockIndex, _blockDim.x);
    vec2u min = vec2u(b.x * blockSize, b.y * blockSize);
    vec2u max = vec2u((b.x + 1) * blockSize, (b.y + 1) * blockSize);
    max = min_component(max, {width, height});

    vector<PSample> batch;
    for (uint j = min.y; j < max.y; j++) {
        for (uint i = min.x; i < max.x; i++) {
            SampleSequence seq;
            initSequence(seq, _imageSampling.generator, _imageSampling.rngs.at(i, j),
                         i, j, _imageSampling.samples.at(i, j), _opts.imageSampling.batchsamples);
            for (int s = 0; s < _opts.imageSampling.batchsamples; s++) {
                batch.emplace_back(seq, vec2u { i, j });
                _imageSampling.samples.at(i, j) += 1;
                nextSample(seq);
            }
        }
    }
    return batch;
}

bool GpuPathRender::sampleFrame(const Camera* camera) {
    uint totalSamples =
        _opts.general.width * _opts.general.height * _opts.imageSampling.batchsamples;
    
    vector<PSample>        pathSamples;
    vector<ray3f>          rays;
    pathSamples.reserve(totalSamples);
    rays.reserve(totalSamples);
    
    // sample camera
    for (auto j = 0; j < _opts.general.height; j++) {
        for (auto i = 0; i < _opts.general.width; i++) {
            vec2u pixel = vec2u(i, j);
            SampleSequence seq;
            initSequence(seq, _imageSampling.generator, _imageSampling.rngs.at(i, j),
                         i, j, _imageSampling.samples.at(i, j), _opts.imageSampling.batchsamples);
            for (int s = 0; s < _opts.imageSampling.batchsamples; s++) {
                PSample ps { seq, pixel };
                vec2f rs = next2f(ps.seq);
                vec2f is = vec2f((i + rs.x) / _opts.general.width, (j + rs.y) / _opts.general.height);
                ray3f ray = ::sampleCamera(camera, is);
                pathSamples.push_back(ps);
                rays.push_back(ray);
                _imageSampling.samples.at(pixel.x, pixel.y) += 1;
                nextSample(seq);
            }
        }
    }
    return traceRadiance(rays, pathSamples);
}

bool GpuPathRender::traceRadiance(std::vector<ray3f> &rays, std::vector<PSample> &samples) {
    if (rays.empty()) return true;
    
    vector<intersection3f> intersections;
    if (!_accelerator->intersectFirst(rays, intersections)) {
        return false;
    }
    
    vector<SSample>      shadowSamples;
    vector<ray3f>        shadowRays;
    vector<PSample>      secondarySamples;
    vector<ray3f>        secondaryRays;
    mutex                shadowMutex;
    mutex                secondaryMutex;
    tbb::parallel_for(tbb::blocked_range<int>(0, intersections.size()),
                    [&](tbb::blocked_range<int> &range)->void {
        for (uint s = range.begin(); s != range.end(); s++) {
            const ray3f &ray = rays[s];
            const intersection3f &isect = intersections[s];
            PSample &ps = samples[s];
            SampleSequence &seq = ps.seq;
            vec3f le = zero3f; vec3f wo; frame3f f; ReflectanceSample rs;
            if (!::resolveRay(ray, isect, _scene->lights, f, wo, le, rs) || !isvalid(rs)) {
                accumulate(ps.pixel, le * ps.throughput);
                continue;
            }
            vec3f ld; ray3f shadowRay;
            tie(ld, shadowRay) = ::evalDirect(rs, f, wo, seq, _scene->lights);
            if (!ld.iszero()) {
                mutex::scoped_lock lock(shadowMutex);
                shadowRays.push_back(shadowRay);
                shadowSamples.emplace_back(ld * ps.throughput, ps.pixel);
            }
            
            if(ps.depth < _opts.maxdepth-1) {
                ScatteringSample ss = sampleScattering(rs,f,wo,next1f(seq),next2f(seq));
                if(!ss.bsdf.iszero()) {
                    vec3f scale = ss.bsdf * dot(ss.ray.d,f.z) / ss.pdf;
                    if (options::gpath::russianroulette) {
                        float continueProb = min(1.0f, mean(scale));
                        if (seq.rng->next1f() > continueProb) {
                            continue;
                        } else {
                            scale /= continueProb;
                        }
                    }
                    {
                        mutex::scoped_lock lock(secondaryMutex);
                        secondaryRays.push_back(ss.ray);
                        secondarySamples.emplace_back(seq, ps.pixel, ps.depth+1, 
                                                      ps.throughput * scale);
                    }
                }
            }
        }
    });

    // unload memory
    samples.reserve(0);
    rays.reserve(0);
    
    if (!traceShadow(shadowRays, shadowSamples)) return false;
    if (!traceRadiance(secondaryRays, secondarySamples)) return false;
    return true;
}

bool GpuPathRender::traceShadow(std::vector<ray3f> &rays, std::vector<SSample> &samples) {
    vector<uint> hits;
    if (!_accelerator->intersectAny(rays, hits)) {
        return false;
    }
    tbb::parallel_for(tbb::blocked_range<int>(0, hits.size()),
                      [&](tbb::blocked_range<int> &range)->void {
        for (uint s = range.begin(); s != range.end(); s++) {
            const uint &hit = hits[s];
            const SSample &ss = samples[s];
            accumulate(ss.pixel, hit > 0 ? zero3f : ss.ld);
        }
    });
    rays.reserve(0);
    samples.reserve(0);
    return true;
}

void GpuPathRender::accumulate(const vec2u &pixel, const vec3f l) {
    _imageSampling.accimage.at(pixel.x, pixel.y) += l;
    float s = _imageSampling.samples.at(pixel.x, pixel.y);
    _imageSampling.image.at(pixel.x, pixel.y) = _imageSampling.accimage.at(pixel.x, pixel.y) / s;
}

vec2i GpuPathRender::getImageSize() const {
    return vec2i(_opts.general.width, _opts.general.height);
}

const Image<vec3f>* GpuPathRender::getImage() const {
    return &(_imageSampling.image);
}
