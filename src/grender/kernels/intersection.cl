"#define STACK_SIZE 64\n"
"#define ENTRYPOINT_SENTINEL -1\n"
OPENCL_PROGRAM_SOURCE(
    float fmax4(float a, float b, float c, float d) {
        return fmax(fmax(a, b), fmax(c, d));
    }
    float fmin4(float a, float b, float c, float d) {
        return fmin(fmin(a, b), fmin(c, d));
    }

    bool intersectBoundingBox(float3 bmin, float3 bmax, float3 o, float3 invDir,
                                float tmin, float tmax) {
        // branch-less intersection test:http: //goo.gl/L6TbDV
        float3 a = (bmin - o) * invDir;
        float3 b = (bmax - o) * invDir;
        float2 span;
        span.lo = fmax4(fmin(a.x, b.x), fmin(a.y, b.y), fmin(a.z, b.z), tmin);
        span.hi = fmin4(fmax(a.x, b.x), fmax(a.y, b.y), fmax(a.z, b.z), tmax);
        return span.hi >= span.lo;
    }

    // x > 0 means hit, otherwise missed
    float4 intersectTriangle(float3 v0, float3 v1, float3 v2, float3 orin, float3 dir,
                             float tmin, float tmax) {
        float t; float2 uv; bool th;
        const float4 miss = (float4)(-1.0, 0.0, 0.0, 0.0);

        float3 e1 = v1 - v0;
        float3 e2 = v2 - v0;
        float3 s1 = cross(dir, e2);
        float divisor = dot(s1, e1);
        if(divisor == 0.0) return miss;

        float invDivisor = 1.0 / divisor;
        float3 d = orin - v0;
        uv.x = dot(d, s1) * invDivisor;
        if(uv.x < 0 || uv.x > 1) return miss;

        float3 s2 = cross(d, e1);
        uv.y = dot(dir, s2) * invDivisor;
        if(uv.y < 0 || uv.x + uv.y > 1) return miss;

        t = dot(e2, s2) * invDivisor;
        if(t < tmin || t > tmax) return miss;

        return (float4)(1.0, t, uv);
    }
)
