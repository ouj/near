"#define STACK_SIZE 64\n"
"#define ENTRYPOINT_SENTINEL -1\n"
#include "intersection.cl"
OPENCL_PROGRAM_SOURCE(
    __kernel void intersectFirst(__global float8 *rays, __global float8 *nodes,
                                 __global int *faces, __global float *vertices,
                                 __global float4 *intersections) {
        // initialization
        int index = get_global_id(0);

        float8 rawRay = rays[index];
        float3 o = rawRay.s012; // ray origin
        float3 d = rawRay.s345; // ray direction.
        float tmin = rawRay.s6;
        float tmax = rawRay.s7;

        float3  invDir = 1.0f / d; // inverted ray direction
        float   invDir_[3];
        vstore3(invDir, 0, invDir_);

        int     traversalStack[STACK_SIZE];
        int     nodeIndex = 0;  // Start from the root.
        int     leafIndex = -1;  // No postponed leaf.

        traversalStack[0] = ENTRYPOINT_SENTINEL; // Bottom-most entry.
        int *stackPtr = &traversalStack[0];

        float t = MAXFLOAT;
        int id = -1;
        float2 uv;

        __local int searchingLeaf;
        while (nodeIndex != ENTRYPOINT_SENTINEL) {
            searchingLeaf = 0;

            // tracing branches
            while (leafIndex < 0 && nodeIndex != ENTRYPOINT_SENTINEL) {
                float8 node = nodes[nodeIndex];
                float3 bmin = node.s012;
                float3 bmax = node.s345;
                uint secondChild = as_uint(node.s6);
                int aux = as_int(node.s7);

                if (intersectBoundingBox(bmin, bmax, o, invDir, tmin, tmax)) { // interseted
                    if (aux <= 0) {  // branch node
                        int dim = -aux;
                        if (invDir_[dim] < 0) {
                            stackPtr++;
                            *stackPtr = nodeIndex + 1;
                            nodeIndex = secondChild;
                        } else {
                            stackPtr++;
                            *stackPtr = secondChild;
                            nodeIndex = nodeIndex + 1;
                        }
                    } else if (aux > 0 && leafIndex < 0) {
                        atomic_inc(&searchingLeaf);
                        leafIndex = nodeIndex;
                        nodeIndex = *stackPtr;
                        stackPtr--;
                    }
                } else {
                    nodeIndex = *stackPtr;
                    stackPtr--;
                }
                if (searchingLeaf == get_local_size(0))
                    break;
            }

            // tracing leaves
            while (leafIndex >= 0) {
                float8 leaf = nodes[leafIndex];
                uint offset = as_uint(leaf.s6);
                int nitem = as_int(leaf.s7);
                for (int s = 0; s < nitem; s++) {
                    uint ti = offset + s;
                    __global int *f = &faces[ti * 3];

                    float3 v0 = vload3(f[0], vertices);
                    float3 v1 = vload3(f[1], vertices);
                    float3 v2 = vload3(f[2], vertices);

                    // intersect triangle
                    float4 th = intersectTriangle(v0, v1, v2, o, d, tmin, tmax);
                    if (th.x > 0 && t > th.y) {
                        id = ti;
                        t = th.y;
                        uv = th.zw;
                    }
                }
                leafIndex = -1;
            }
        }
        intersections[index] = (float4)(as_float(id), t, uv);
    }
    
    __kernel void intersectAny(__global float8 *rays, __global float8 *nodes, __global int *faces, 
                               __global float *vertices, __global uint *hits) {
        int index = get_global_id(0);
        
        float8 rawRay = rays[index];
        float3 o = rawRay.s012; // ray origin
        float3 d = rawRay.s345; // ray direction.
        float tmin = rawRay.s6;
        float tmax = rawRay.s7;
        
        float3  invDir = 1.0f / d; // inverted ray direction
        float   invDir_[3];
        vstore3(invDir, 0, invDir_);
        
        int     traversalStack[STACK_SIZE];
        int     nodeIndex = 0;  // Start from the root.
        int     leafIndex = -1;  // No postponed leaf.
        
        traversalStack[0] = ENTRYPOINT_SENTINEL; // Bottom-most entry.
        int *stackPtr = &traversalStack[0];

        int id = -1;
        float2 uv;
        
        __local int searchingLeaf;
        while (nodeIndex != ENTRYPOINT_SENTINEL) {
            searchingLeaf = 0;
            
            // tracing branches
            while (leafIndex < 0 && nodeIndex != ENTRYPOINT_SENTINEL) {
                float8 node = nodes[nodeIndex];
                float3 bmin = node.s012;
                float3 bmax = node.s345;
                uint secondChild = as_uint(node.s6);
                int aux = as_int(node.s7);
                
                if (intersectBoundingBox(bmin, bmax, o, invDir, tmin, tmax)) { // interseted
                    if (aux <= 0) {  // branch node
                        int dim = -aux;
                        if (invDir_[dim] < 0) {
                            stackPtr++;
                            *stackPtr = nodeIndex + 1;
                            nodeIndex = secondChild;
                        } else {
                            stackPtr++;
                            *stackPtr = secondChild;
                            nodeIndex = nodeIndex + 1;
                        }
                    } else if (aux > 0 && leafIndex < 0) {
                        atomic_inc(&searchingLeaf);
                        leafIndex = nodeIndex;
                        nodeIndex = *stackPtr;
                        stackPtr--;
                    }
                } else {
                    nodeIndex = *stackPtr;
                    stackPtr--;
                }
                if (searchingLeaf == get_local_size(0))
                    break;
            }
            
            // tracing leaves
            while (leafIndex >= 0) {
                float8 leaf = nodes[leafIndex];
                uint offset = as_uint(leaf.s6);
                int nitem = as_int(leaf.s7);
                for (int s = 0; s < nitem; s++) {
                    uint ti = offset + s;
                    __global int *f = &faces[ti * 3];
                    
                    float3 v0 = vload3(f[0], vertices);
                    float3 v1 = vload3(f[1], vertices);
                    float3 v2 = vload3(f[2], vertices);
                    
                    // intersect triangle
                    float4 th = intersectTriangle(v0, v1, v2, o, d, tmin, tmax);
                    if (th.x > 0) {
                        hits[index] = 1;
                        return;
                    }
                }
                leafIndex = -1;
            }
        }
        hits[index] = 0;
    }
)
