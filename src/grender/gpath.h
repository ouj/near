#ifndef GPU_PATH_RENDER_H
#define GPU_PATH_RENDER_H

#include <integration/opts.h>
#include <integration/sequence.h>
#include "gaccelerator_bvh.h"
#include <render/render.h>

struct GpuPathRenderOpts : RenderOpts {
    GpuPathRenderOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    GeneralOpts         general;
    ImageSamplingOpts   imageSampling;
    uint                maxdepth = 2;
};

struct PSample {
    PSample(const SampleSequence &seq, const vec2u &pixel)
        : seq(seq), pixel(pixel) {}
    PSample(const SampleSequence &seq, const vec2u &pixel, int depth, const vec3f &throughput)
        : seq(seq), pixel(pixel), depth(depth), throughput(throughput) {}
    SampleSequence      seq;
    vec2u               pixel;
    int                 depth = 0;
    vec3f               throughput = one3f;
};

struct SSample {
    SSample(const vec3f &ld, const vec2u &pixel) : ld(ld), pixel(pixel) {}
    vec3f               ld;
    vec2u               pixel;
};

class GpuPathRender {
public:
    const static int TYPEID = 900901;
    GpuPathRender();
    GpuPathRender(const GpuPathRenderOpts &opts);
    virtual ~GpuPathRender();

    virtual bool                initState(const Scene *scene, const GpuBvhAccelerator *accelerator);
    virtual void                clearState();
    virtual Image<vec3f>        traceImage();
    virtual bool                tracePass();
    virtual vec2i               getImageSize() const;
    virtual const Image<vec3f>* getImage() const;
private:
    bool                        sampleFrame(const Camera *camera);
    void                        accumulate(const vec2u &pixel, const vec3f l);
    std::vector<PSample>        emitBatch(uint blockIndex);
    void                        traceCamera(const Camera* camera, std::vector<PSample> &samples);
    bool                        traceRadiance(std::vector<ray3f> &rays,
                                              std::vector<PSample> &samples);
    bool                        traceShadow(std::vector<ray3f> &rays,
                                            std::vector<SSample> &samples);

    const Scene                *_scene;
    const GpuBvhAccelerator    *_accelerator;

    uint                        _blockIndex = 0;
    uint                        _blockNum;
    vec2u                       _blockDim;
    GpuPathRenderOpts           _opts;
    ImageSamplingState          _imageSampling;

    friend struct CameraTraceTask;
};

#endif
