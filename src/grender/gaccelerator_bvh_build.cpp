#include "gaccelerator_bvh.h"

#include <scene/transform.h>
#include <scene/shape.h>
#include <algorithm>
#include <tbb/tbb.h>
#include <tuple>

using namespace std;

struct BvhBucket {
    BvhBucket() : count(0), bbox(invalidrange3f){}
    int         count;
    range3f     bbox;
};

struct BvhTriBuildItem {
    range3f     bbox;
    vec3f       centroid;
    int         index;
};

static void flattenScene(const vector<AcceleratorSurface*> &surfaces,
                         vector<vec3i> &faces, vector<vec3f> &pos, vector<vec2u> &triangleIds) {
    // allocate memory
    int totalVertexCount = 0;
    int totalTriangleCount = 0;

    for_each(surfaces.begin(), surfaces.end(),
             [&totalTriangleCount, &totalVertexCount](const AcceleratorSurface *s)->void {
        error_if_not(s->shape->type == TriangleMeshShape::TYPEID,
                     "only triangle meshes supported");
        TriangleMeshShape* mesh = (TriangleMeshShape*)s->shape;
        totalVertexCount += mesh->pos.size();
        totalTriangleCount += mesh->face.size();
    });

    pos.reserve(totalVertexCount);
    faces.reserve(totalTriangleCount);
    triangleIds.reserve(totalTriangleCount);

    int pIndex = 0;
    for (uint i = 0; i < surfaces.size(); i ++) {
        error_if_not(surfaces[i]->shape->type == TriangleMeshShape::TYPEID,
                     "supports only triangle meshes");
        const TriangleMeshShape* mesh =  (TriangleMeshShape*)surfaces[i]->shape;
        for(int j = 0; j < mesh->pos.size(); j ++) {
            pos.push_back(transformPoint(surfaces[i]->transform, mesh->pos[j]));
        }

        // Translate mesh indices
        vec3i baseIndex { pIndex, pIndex, pIndex };
        for (uint j = 0; j < mesh->face.size(); j++) {
            faces.push_back(mesh->face[j] + baseIndex);
            triangleIds.push_back({ i, j });
        }
        pIndex += mesh->pos.size();
    }
}

static GpuBvhNode makeBvhLeaf(vector<BvhTriBuildItem>::iterator begin,
                              vector<BvhTriBuildItem>::iterator end,
                              const range3f &bbox, vector<uint> &orderedFaces) {
    assert(end - begin < 256); // make sure it fit in a byte.
    GpuBvhNode leaf;
    leaf.s[0] = bbox.min.x;
    leaf.s[1] = bbox.min.y;
    leaf.s[2] = bbox.min.z;
    leaf.s[3] = bbox.max.x;
    leaf.s[4] = bbox.max.y;
    leaf.s[5] = bbox.max.z;
    leaf.s[6] = __as_float((cl_uint)orderedFaces.size()); // offset
    leaf.s[7] = __as_float((cl_int)(end - begin));
    for_each(begin, end, [&orderedFaces](const BvhTriBuildItem& item)->void {
        orderedFaces.push_back(item.index);
    });
    return leaf;
}

static GpuBvhNode makeBvhBranch(const range3f &bbox, int8_t dim, uint32_t left, uint32_t right) {
    GpuBvhNode node;
    node.s[0] = bbox.min.x;
    node.s[1] = bbox.min.y;
    node.s[2] = bbox.min.z;
    node.s[3] = bbox.max.x;
    node.s[4] = bbox.max.y;
    node.s[5] = bbox.max.z;
    node.s[6] = __as_float((cl_uint)right); // offset
    node.s[7] = __as_float((cl_int)-dim);
    return node;
}

inline float bboxArea(const range3f &b) {
    vec3f d = b.extent();
    return 2.f * (d.x * d.y + d.x * d.z + d.y * d.z);
}

uint32_t recursiveBvhBuild(vector<BvhTriBuildItem>::iterator begin,
                           vector<BvhTriBuildItem>::iterator end,
                           vector<GpuBvhNode> &nodes, vector<uint> &orderedFaces, int maxLeafItems) {
    int size = end - begin;
    error_if(size <= 0, "invalid size");
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;

    for_each(begin, end, [&bbox, &cbbox](const BvhTriBuildItem& item)->void {
        bbox.grow(item.bbox);
        cbbox.grow(item.centroid);
    });

    int8_t dim = cbbox.extent().max_component_index();
    if (cbbox.max[dim] == cbbox.min[dim] || size <= maxLeafItems) {
        nodes.push_back(makeBvhLeaf(begin, end, bbox, orderedFaces));
        return nodes.size() - 1;
    }

    // Use SAH heuristic
    const int bucketNum = 32;
    BvhBucket buckets[bucketNum];
    float range = cbbox.max[dim] - cbbox.min[dim];
    for_each(begin, end, [&](const BvhTriBuildItem& item)->void {
        float offset = item.centroid[dim] - cbbox.min[dim];
        int b = min(bucketNum - 1, (int)(bucketNum * (offset / range)));
        error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
        buckets[b].count += 1;
        buckets[b].bbox.grow(item.bbox);
    });

    float minCost = consts<float>::max;
    int splitIndex = 0;
    float totalArea = bboxArea(bbox);
    for (int i = 0; i < bucketNum - 1; ++i) {
        range3f b0 = invalidrange3f;
        range3f b1 = invalidrange3f;
        int count0 = 0;
        int count1 = 0;
        for (int j = 0; j <= i; ++j) {
            b0.grow(buckets[j].bbox);
            count0 += buckets[j].count;
        }
        for (int j = i+1; j < bucketNum; ++j) {
            b1.grow(buckets[j].bbox);
            count1 += buckets[j].count;
        }
        float cost = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
        if (cost < minCost) {
            minCost = cost;
            splitIndex = i;
        }
    }

    assert(size > maxLeafItems);
    decltype(begin) mid;
    if (minCost < size) {
        mid = partition(begin, end,
                        [splitIndex, bucketNum, dim, &cbbox](const BvhTriBuildItem &t)->bool {
            float r = cbbox.max[dim] - cbbox.min[dim];
            int b = static_cast<int>(bucketNum *
                                    ((t.centroid[dim] - cbbox.min[dim]) / r));
            b = clamp(b, 0, bucketNum - 1);
            return b <= splitIndex;
        });
        if (mid == begin || mid == end) {
            warning("Invalid SAH split");
            nodes.push_back(makeBvhLeaf(begin, end, bbox, orderedFaces));
            return nodes.size() - 1;
        }
    } else {
        mid = begin + (end - begin) * 0.5f;
    }
    assert(mid != begin && mid != end);
    uint32_t nodeIndex = nodes.size();
    nodes.emplace_back();
    uint32_t left = recursiveBvhBuild(begin, mid, nodes, orderedFaces, maxLeafItems);
    uint32_t right = recursiveBvhBuild(mid, end, nodes, orderedFaces, maxLeafItems);
    assert(left == nodeIndex + 1);
    GpuBvhNode& node = nodes[nodeIndex];
    node = makeBvhBranch(bbox, dim, left, right);
    return nodeIndex;
}

bool GpuBvhAccelerator::buildBvhAccelerator() {
    ::flattenScene(_surfaces, _faces, _vertices, _triangleIds);
    if (_faces.empty() || _vertices.empty()) return true;
    vector<BvhTriBuildItem> items;
    items.reserve(_faces.size());
    _worldBound = invalidrange3f;
    for (int i = 0; i < _faces.size(); i++) {
        const vec3i &f = _faces[i];
        const vec3f &v0 = _vertices[f.x];
        const vec3f &v1 = _vertices[f.y];
        const vec3f &v2 = _vertices[f.z];
        range3f bbox = triangleBoundingBox(v0, v1, v2);
        _worldBound.grow(bbox);
        items.push_back({ bbox, bbox.center(), i});
    }

    vector<uint> orderedFaces;
    orderedFaces.reserve(_faces.size());
    uint32_t nodeNum = recursiveBvhBuild(items.begin(), items.end(), _nodes, orderedFaces,
                                         _opts.maxLeafItems);
    assert(nodeNum == 0);
    decltype(_faces)            nFaces;
    decltype(_triangleIds)      nTriangleIds;
    nFaces.reserve(_faces.size());
    nTriangleIds.reserve(_triangleIds.size());
    for_each(orderedFaces.begin(), orderedFaces.end(),
        [this, &nFaces, &nTriangleIds](int index)->void {
        nFaces.push_back(_faces[index]);
        nTriangleIds.push_back(_triangleIds[index]);
    });
    _faces = std::move(nFaces);
    _triangleIds = std::move(nTriangleIds);

    return true;
}