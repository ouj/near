#include "cldevice.h"

using std::string;
using std::vector;

string CLDeviceTypeString(cl_device_type type) {
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            return "CPU Device";
        case CL_DEVICE_TYPE_GPU:
            return "GPU Device";
        case CL_DEVICE_TYPE_ACCELERATOR:
            return "Accelerator Device";
        case CL_DEVICE_TYPE_DEFAULT:
            return "Default Device";
        default:
            return "Unknown Device";
    }
}

string CLDeviceLocalMemoryTypeString(cl_device_local_mem_type type) {
    switch (type) {
        case CL_LOCAL:
            return "Local";
        case CL_GLOBAL:
            return "Global";
        case CL_NONE:
            return "None";
        default:
            return "Unknown";
    }
}

string CLDeviceMemCacheTypeString(cl_device_mem_cache_type type) {
    switch (type) {
        case CL_READ_ONLY_CACHE:
            return "Read Only";
        case CL_READ_WRITE_CACHE:
            return "Read and Write";
        case CL_NONE:
            return "None";
        default:
            return "Unknown";
    }
}

vector<string> CLDeviceExecutionCapabilityList(cl_device_exec_capabilities cap) {
    vector<string> list;
    if (cap | CL_EXEC_KERNEL)
        list.emplace_back("Execute Kernel");
    if (cap | CL_EXEC_NATIVE_KERNEL)
        list.emplace_back("Execute Native Kernel");
    return list;
}

vector<string> CLDeviceCommandQueuePropertiesList(cl_command_queue_properties prop) {
    vector<string> list;
    if (prop | CL_QUEUE_PROFILING_ENABLE)
        list.emplace_back("Queue Out of Order Execution Mode Enable");
    if (prop | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE )
        list.emplace_back("Queue Profiling Enable");
    return list;
}

vector<string> CLDeviceFpConfigList(cl_device_fp_config config) {
    vector<string> list;
    if (config | CL_FP_DENORM)
        list.emplace_back("denorms");
    if (config | CL_FP_INF_NAN)
        list.emplace_back("INF and quiet NaNs");
    if (config | CL_FP_ROUND_TO_NEAREST)
        list.emplace_back("round to nearest even rounding mode");
    if (config | CL_FP_ROUND_TO_ZERO)
        list.emplace_back("round to zero rounding mode");
    if (config | CL_FP_ROUND_TO_INF)
        list.emplace_back("round to +ve and -ve infinity rounding modes");
    if (config | CL_FP_FMA)
        list.emplace_back("IEEE754-2008 fused multiply-add");
    if (config | CL_FP_CORRECTLY_ROUNDED_DIVIDE_SQRT)
        list.emplace_back("divide and sqrt are correctly rounded as defined by the IEEE754 specification");
    if (config | CL_FP_SOFT_FLOAT)
        list.emplace_back("Basic floating-point operations are implemented in software");
    return list;
}
