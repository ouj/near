#include "clenvironment.h"
#include <vector>

#include "cldevice.h"

using namespace cl;

void clLogMessagesToStdout(const char *errStr, const void *privateInfo, ::size_t cb,
                           void *userData) {
    message_va("Context error: %s", errStr);
}

bool CLEnvironment::initialize() {
    cl_int err = CL_SUCCESS;

    std::vector<Platform> platforms;
    err = Platform::get(&platforms);
    RETURN_FALSE_IF_CL_ERROR(err, "cl::Platform::get()");
    if (platforms.empty()) return false;

    _platform = platforms[0];
    // Try to get a GPU device
    _platform.getDevices(CL_DEVICE_TYPE_GPU, &_devices);
    if (_devices.empty()) {
        warning("not available gpu, fall back to all devices");
        err = _platform.getDevices(CL_DEVICE_TYPE_ALL, &_devices);
        RETURN_FALSE_IF_CL_ERROR(err, "getDevices");
        if (_devices.empty()) {
            return false;
        }
    }

    // create cl context
    cl_context_properties properties[3] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)_platform(),
        0
    };
    _context = Context(_devices, properties, &clLogMessagesToStdout, nullptr, &err);
    RETURN_FALSE_IF_CL_ERROR(err, "construct context");

    cl_command_queue_properties cq_properties = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE;
    _commandQueue = CommandQueue(_context, _devices[0], cq_properties, &err);
    RETURN_FALSE_IF_CL_ERROR(err, "construct command queue");

    return true;
}

void CLEnvironment::shutdown() {
    _commandQueue.finish();
}
