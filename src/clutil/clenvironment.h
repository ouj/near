#ifndef CL_ENVIRONMENT_H
#define CL_ENVIRONMENT_H

#include "clutil.h"

struct CLEnvironment {
    CLEnvironment() {};
    ~CLEnvironment() {};

    bool initialize();
    void shutdown();

    CLEnvironment(const CLEnvironment &env) = delete;
    CLEnvironment& operator=(const CLEnvironment &env) = delete;

    cl::Context _context;
    cl::CommandQueue _commandQueue;
    cl::Platform _platform;
    std::vector<cl::Device> _devices;
};

#endif