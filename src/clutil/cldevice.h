#ifndef CL_DEVICE_H
#define CL_DEVICE_H

#include "clutil.h"
#include <vector>
#include <string>

std::string CLDeviceTypeString(cl_device_type type);
std::string CLDeviceLocalMemoryTypeString(cl_device_local_mem_type type);
std::string CLDeviceMemCacheTypeString(cl_device_mem_cache_type type);
std::vector<std::string> CLDeviceExecutionCapabilityList(cl_device_exec_capabilities cap);
std::vector<std::string> CLDeviceCommandQueuePropertiesList(cl_command_queue_properties prop);
std::vector<std::string> CLDeviceFpConfigList(cl_device_fp_config config);

#endif