#ifndef CL_UTIL_H
#define CL_UTIL_H

#define USE_CL_DEVICE_FISSION
#if defined(__APPLE__) || defined(__MACOSX)
#include "OpenCL_1_2/cl.hpp"
#else
#include "OpenCL_1_1/cl.hpp"
#endif

#include "clerror.h"

#define OPENCL_PROGRAM_SOURCE(...) #__VA_ARGS__

inline cl_float __as_float(int32_t i) { return *(cl_float*)&i; }
inline cl_float __as_float(uint32_t i) { return *(cl_float*)&i; }
inline cl_float __as_float(cl_short2 s2) { return *(cl_float*)&s2; }

inline cl_int __as_int(cl_float f) { return *(cl_int*)&f; }
inline cl_uint __as_uint(cl_float f) { return *(cl_uint*)&f; }
inline cl_short2 __as_short2(cl_float f) { return *(cl_short2*)&f; }

#endif
