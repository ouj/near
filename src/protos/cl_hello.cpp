#include <clutil/clutil.h>
#include <clutil/clenvironment.h>
#include <vector>
#include <string>
#include <map>
#include <common/debug.h>

const char source[] = OPENCL_PROGRAM_SOURCE(
__constant char hw[] = "Hello World\n";
__kernel void hello(__global char * out)
{
    size_t tid = get_global_id(0);
    out[tid] = hw[tid];
}
);

int main(int argc, char** argv) {
    CLEnvironment clenv;
    if (!clenv.initialize()) {
        error("Failed to initialize OpenCL");
        clenv.shutdown();
    }

    cl_int err = CL_SUCCESS;

    const size_t bufferLen = 512;
    char *outH = new char[bufferLen];
    cl::Buffer outCL(clenv._context,
                     CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
                     bufferLen,
                     outH,
                     &err);
    CHECK_CL_ERROR(err, "Buffer::Buffer()");

    cl::Program::Sources sources = { { source, sizeof(source) } };
    cl::Program program(clenv._context, sources, &err);
    CHECK_CL_ERROR(err, "Program::Program()");
    err = program.build(clenv._devices);
    if (err != CL_SUCCESS) {
        std::string log =
            program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(clenv._devices.front());
        error_va("Compile error %s", log.c_str());
    }
    cl::Kernel kernel(program, "hello", &err);
    CHECK_CL_ERROR(err, "Kernel::Kernel()");
    err = kernel.setArg(0, outCL);
    CHECK_CL_ERROR(err, "Kernel::setArg()");

    cl::Event event;
    err = clenv._commandQueue.enqueueNDRangeKernel(kernel,
                                                   cl::NullRange,
                                                   cl::NDRange(bufferLen),
                                                   cl::NDRange(1, 1),
                                                   NULL,
                                                   &event);
    CHECK_CL_ERROR(err, "ComamndQueue::enqueueNDRangeKernel()");

    event.wait();
    err = clenv._commandQueue.enqueueReadBuffer(outCL,
                                                CL_TRUE,
                                                0,
                                                bufferLen,
                                                outH);
    CHECK_CL_ERROR(err, "ComamndQueue::enqueueReadBuffer()");
    message(outH);

    clenv.shutdown();
    return 0;
}
