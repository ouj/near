#include <clutil/clutil.h>
#include <clutil/cldevice.h>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <common/debug.h>

std::string listToString(const std::vector<std::string> &list) {
    std::string str;
    for (auto &s : list) {
        str += "\n\t" + s;
    }
    return str;
}

struct DeviceInfoTable {
public:

    template<typename T>
    void add(const std::string &name, const T &content) {
        device_infos.push_back({ name, std::to_string(content) });
    }
    template<typename T>
    void add(const std::string &name, const std::vector<T> &content) {
        std::string contentStr;
        std::for_each(content.begin(), content.end(), [&contentStr] (const T &t)->void {
            contentStr += std::to_string(t);
            contentStr += ',';
        });
        device_infos.push_back({ name, contentStr });
    };
    void clear() { device_infos.clear(); }
    void print() {
        for (const auto &item : device_infos) {
            message_va("%s: %s", item.first.c_str(), item.second.c_str());
        }
    }
private:
    std::vector<std::pair<std::string, std::string>> device_infos;
};

template<>
void DeviceInfoTable::add(const std::string &name, const std::string &content) {
    device_infos.push_back({name, content});
}

template<>
void DeviceInfoTable::add(const std::string &name, const bool &content) {
    device_infos.push_back({name, content ? "True" : "False"});
}

template<>
void DeviceInfoTable::add(const std::string &name, const std::vector<std::string> &content) {
    std::string contentStr;
    std::for_each(content.begin(), content.end(), [&contentStr] (const std::string &t)->void {
        contentStr += t;
        contentStr += ',';
    });
    device_infos.push_back({ name, contentStr });
}

int main(int argc, char** argv) {
    cl_int err = CL_SUCCESS;

    std::vector<cl::Platform> platformList;
    err = cl::Platform::get(&platformList);
    CHECK_CL_ERROR(err, "cl::Platform::get()");

    for (cl_uint i = 0; i < platformList.size(); i++) {
        const cl::Platform &platform = platformList[i];
        // Get platform info
        message_va("Platform %d\n---------------------- ", i + 1);
        message_va("Name: %s", platform.getInfo<CL_PLATFORM_NAME>().c_str());
        message_va("Version: %s", platform.getInfo<CL_PLATFORM_VERSION>().c_str());
        message_va("Vendor: %s", platform.getInfo<CL_PLATFORM_VENDOR>().c_str());
        message_va("Profile: %s", platform.getInfo<CL_PLATFORM_PROFILE>().c_str());
        message("");

        // Get device info
        std::vector<cl::Device> deviceList;
        err = platformList[i].getDevices(CL_DEVICE_TYPE_ALL, &deviceList);
        CHECK_CL_ERROR(err, "getDevices");

        for (cl_uint j = 0; j < deviceList.size(); j++) {
            message_va("* Device %d *", j + 1);
            const cl::Device &device = deviceList[j];
            DeviceInfoTable table;

            table.add("Device Name", device.getInfo<CL_DEVICE_NAME>());
            table.add("Device Type", CLDeviceTypeString(device.getInfo<CL_DEVICE_TYPE>()));
            table.add("Vendor", device.getInfo<CL_DEVICE_VENDOR>());
            table.add("Vendor ID", device.getInfo<CL_DEVICE_VENDOR_ID>());
            table.add("Device Version", device.getInfo<CL_DEVICE_VERSION>());
            table.add("Drive Version", device.getInfo<CL_DRIVER_VERSION>());
            table.add("Profile", device.getInfo<CL_DEVICE_PROFILE>());

            table.add("Max Compute Units", device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>());
            table.add("Max Work Item Dimensions", device.getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>());
            table.add("Max Work Group Size", device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());
            table.add("Max Work Item Size", device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>());

            table.add("Max Clock Frequency", device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>());
            table.add("Address Bits", device.getInfo<CL_DEVICE_ADDRESS_BITS>());
            table.add("Max Read Image Arguments", device.getInfo<CL_DEVICE_MAX_READ_IMAGE_ARGS>());
            table.add("Max Write Image Arguments", device.getInfo<CL_DEVICE_MAX_WRITE_IMAGE_ARGS>());
            table.add("Max Memory Allocation Size", device.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>());
            table.add("Native Vector Width (Char)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR>());
            table.add("Native Vector Width (Short)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT>());
            table.add("Native Vector Width (Int)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_INT>());
            table.add("Native Vector Width (Long)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG>());
            table.add("Native Vector Width (Half)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF>());
            table.add("Native Vector Width (Float)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT>());
            table.add("Native Vector Width (Double)", device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE>());
            table.add("Preferred Vector Width (Char)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR>());
            table.add("Preferred Vector Width (Short)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT>());
            table.add("Preferred Vector Width (Int)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT>());
            table.add("Preferred Vector Width (Long)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG>());
            table.add("Preferred Vector Width (Half)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF>());
            table.add("Preferred Vector Width (Float)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT>());
            table.add("Preferred Vector Width (Double)", device.getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE>());
            table.add("Has Image Support", device.getInfo<CL_DEVICE_IMAGE_SUPPORT>());

            table.add("2D Image Max Width", device.getInfo<CL_DEVICE_IMAGE2D_MAX_WIDTH>());
            table.add("2D Image Max Height", device.getInfo<CL_DEVICE_IMAGE2D_MAX_HEIGHT>());
            table.add("3D Image Max Width", device.getInfo<CL_DEVICE_IMAGE3D_MAX_WIDTH>());
            table.add("3D Image Max Height", device.getInfo<CL_DEVICE_IMAGE3D_MAX_HEIGHT>());
            table.add("3D Image Max Depth", device.getInfo<CL_DEVICE_IMAGE3D_MAX_DEPTH>());
            table.add("Max Parameter Size", device.getInfo<CL_DEVICE_MAX_PARAMETER_SIZE>());
            table.add("Max Samplers", device.getInfo<CL_DEVICE_MAX_SAMPLERS>());

            table.add("Memory Base Address Alignment", device.getInfo<CL_DEVICE_MEM_BASE_ADDR_ALIGN>());
            table.add("Minimal Data Type Alignment Size", device.getInfo<CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE>());
            table.add("Global Memory Type", CLDeviceMemCacheTypeString(device.getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_TYPE>()));
            table.add("Global Memory Cacheline Size", device.getInfo<CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE>());
            table.add("Global Memory Cache Size", device.getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_SIZE>());
            table.add("Global Memory Size", device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>());
            table.add("Max Constant Buffer Size", device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>());
            table.add("Max Constant Arguments", device.getInfo<CL_DEVICE_MAX_CONSTANT_ARGS>());

            table.add("Local Memory Type", CLDeviceLocalMemoryTypeString(device.getInfo<CL_DEVICE_LOCAL_MEM_TYPE>()));
            table.add("Local Memory Size", device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>());
            table.add("Error Correction Support", device.getInfo<CL_DEVICE_ERROR_CORRECTION_SUPPORT>());
            table.add("Profiling Timer Resolution", device.getInfo<CL_DEVICE_PROFILING_TIMER_RESOLUTION>());
            table.add("Use Little Endian", device.getInfo<CL_DEVICE_ENDIAN_LITTLE>());
            table.add("Execution Capability", CLDeviceExecutionCapabilityList(device.getInfo<CL_DEVICE_EXECUTION_CAPABILITIES>()));
            table.add("Execution Capability", CLDeviceCommandQueuePropertiesList(device.getInfo<CL_DEVICE_QUEUE_PROPERTIES>()));
            table.add("Single FP Config", listToString(CLDeviceFpConfigList(device.getInfo<CL_DEVICE_SINGLE_FP_CONFIG>())));
            table.add("Double FP Config", listToString(CLDeviceFpConfigList(device.getInfo<CL_DEVICE_DOUBLE_FP_CONFIG>())));
            table.add("Host Unified Memory", device.getInfo<CL_DEVICE_HOST_UNIFIED_MEMORY>());
            table.add("Device Available", device.getInfo<CL_DEVICE_AVAILABLE>());
            table.add("Compiler Available", device.getInfo<CL_DEVICE_COMPILER_AVAILABLE>());

#if defined(__APPLE__) || defined(__MACOSX)
            table.add("OpenCL C Version", device.getInfo<CL_DEVICE_OPENCL_C_VERSION>());
            table.add("Built-in Kernels", device.getInfo<CL_DEVICE_BUILT_IN_KERNELS>());
            table.add("Reference Count", device.getInfo<CL_DEVICE_REFERENCE_COUNT>());
            table.add("Preferred Interop User Synce", device.getInfo<CL_DEVICE_PREFERRED_INTEROP_USER_SYNC>());
            table.add("Device Partition Properties", device.getInfo<CL_DEVICE_PARTITION_PROPERTIES>());
            table.add("Device Partition Affinity Domain", device.getInfo<CL_DEVICE_PARTITION_AFFINITY_DOMAIN>());
            table.add("Device Partition Type", device.getInfo<CL_DEVICE_PARTITION_TYPE>());
#endif
            table.print();
        }
    }
    return 0;
}
