#!/usr/bin/python

import subprocess
import os
import sys
import glob

def main(argv = None):
    if argv == None or len(argv) <= 1:
        print ('usage: brender.py scenename [traceopts.json] [accopts.json]')
        return
    
    scenename = os.path.splitext(argv[1])[0]
    opts = None
    aopts = None
    if len(argv) > 2:
        opts = argv[2]
    if len(argv) > 3:
        aopts = argv[3]

    list = glob.glob(scenename + '*.json')
    for s in list:
        imagename = os.path.splitext(s)[0]+'.pfm'
        cmd = ['strace', s, imagename]
        if opts != None: cmd.append(opts)
        if aopts != None: cmd.append(aopts)
        print cmd
        subprocess.call(cmd)
    

if __name__ == '__main__':
    main(sys.argv)
