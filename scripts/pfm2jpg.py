#!/usr/bin/python

import os
import glob
import sys
import subprocess

def convert_images(path):
    path = os.path.normpath(path)

    img_path = os.path.normpath(os.path.join(path, '*.[Pp][Ff][Mm]'))
    gp = glob.glob(img_path)

    for fname in gp:
        if os.path.isfile(fname):
            (base, extension) = os.path.splitext(fname)
            outputname = base + ".jpg"
            cmd = ["convert", "-gamma", "2.2", fname, outputname]
            print(cmd)
            subprocess.call(cmd)

def main(argv = None):
    if argv == None or len(argv) <= 1:
        path = ''
    else:
        path = sys.argv[1]
    convert_images(path)

if __name__ == '__main__':
    main(sys.argv)