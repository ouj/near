#!/usr/bin/python
from array import array
import os

width = 0
height = 0
channels = 0

def skipws(file):
    c = file.read(1)
    while c.isspace():
        c = file.read(1)
    file.seek(-1,os.SEEK_CUR)
    
def skippastnewline(file):
    c = file.read(1)
    while c != '\n':
        c = file.read(1)

def nextword(file):
    skipws(file)
    ret = ''
    c = file.read(1)
    while not c.isspace():
        ret += c
        c = file.read(1)
    file.seek(-1,os.SEEK_CUR)
    return ret

def load(filename):
    # file open
    file = open(filename, 'rb')
    
    # magic
    magic = nextword(file)
    
    # width, height
    width  = int(nextword(file))
    height = int(nextword(file))
    
    # scale
    scale = float(nextword(file))
    if scale < 0: scale = -scale;
    
    # skip till a newline
    skippastnewline(file)
    
    # data type
    if magic == 'PF': channels, byte, ascii = 3, False, False
    elif magic == 'PE': channels, byte, ascii = 3, False, True
    elif magic == 'Pf': channels, byte, ascii = 1, False, False
    elif magic == 'Pe': channels, byte, ascii = 1, False, True
    elif magic == 'P6': channels, byte, ascii = 3, True, False
    elif magic == 'P3': channels, byte, ascii = 3, True, True
    elif magic == 'P5': channels, byte, ascii = 1, True, False
    elif magic == 'P2': channels, byte, ascii = 1, True, True
    else: raise Error()
    
    # load data
    if ascii:
        text = ' '.join(file.readlines()).rstrip(' \n').lstrip(' \n')
        image = [float(t) for t in text.split(' ')]
    else:
        if byte:
            data = array('B')
            data.fromfile(file, width*height*channels)
            image = [float(b) for b in data]
        else:
            data = array('f')
            data.fromfile(file,width*height*channels)
            image = data.tolist()
    if width*height*channels != len(image): raise Error()
    
    # scale values
    image = [i / scale for i in image]
    
    # flip y for floats
    if not byte:
        for y in range(0,height/2):
            for x in range(0,width):
                for c in range(0,channels):
                    idx1 = ((y*width)+x)*channels+c
                    idx2 = (((height-1-y)*width)+x)*channels+c
                    image[idx1], image[idx2] = image[idx2], image[idx1]
    
    # ensure all values are good
    lz = 0
    for i in range(len(image)):
        if(image[i] < 0): lz += 1; image[i] = 0
    if lz  >0: print("less than zero: " + str(lz))
    
    # done
    return image, width, height, channels

def save(image, width, height, channels):
    return
