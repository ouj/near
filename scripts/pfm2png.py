#!/usr/bin/python

import os
import glob
import sys
import subprocess



def convert_images(path, exposure, gamma):
    path = os.path.normpath(path)

    img_path = os.path.normpath(os.path.join(path, '*.[Pp][Ff][Mm]'))
    gp = glob.glob(img_path)

    for fname in gp:
        if os.path.isfile(fname):
            (base, extension) = os.path.splitext(fname)
            outputname = base + ".png"
            if exposure == 0.0:
                cmd = ["convert", "-gamma", str(gamma), fname, outputname]
            elif exposure > 0.0:
                cmd = ['convert', '-evaluate LeftShift', str(exposure), "-gamma", str(gamma), fname, outputname]
            else:
                cmd = ['convert', '-evaluate RightShift', str(-exposure), "-gamma", str(gamma), fname, outputname]
            print(fname)
            subprocess.call(cmd)

def main(argv = None):
    exposure = 0.0
    gamma = 2.2
    if argv == None or len(argv) <= 1:
        path = ''
        gamma = 2.2
        exposure = 0.0
    else:
        path = ''
        for a in argv[1:]:
            if a[0] == '-':
                if a[1] == 'e':
                    exposure = float(a[2:])
                elif a[1] == 'g':
                    gamma = float(a[2:])
            else:
                path = a
    #print (path, exposure, gamma)
    convert_images(path, exposure, gamma)

if __name__ == '__main__':
    main(sys.argv)