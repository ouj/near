#!/usr/bin/python

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from array import array
import imageio

USE_BUFFERS = True

gamma = 2.2
exposure = 0.0

width = 0
height = 0
channels = 0

flip = False
windowWidth = 0
windowHeight = 0
zoom = 1

image = []
displayImage = []

filename = ''

# from gl utils
def drawPixels(ww, wh, zoom, x, y, w, h, nc, d, flipY):
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0,ww,wh,0)
    
    glDisable(GL_DEPTH_TEST)
    glDisable(GL_CULL_FACE)

    glColor3f(1,1,1)

    if flipY:
        glRasterPos2d(x,zoom*y)
        glPixelZoom(zoom,-zoom)
        glDrawPixels(w,h,GL_RGB if nc==3 else GL_RGBA,GL_FLOAT,d)
        glPixelZoom(1,1);
    else:
        glPixelZoom(zoom,zoom)
        glRasterPos2d(x,zoom*(h-y))
        glDrawPixels(w,h,GL_RGB if nc==3 else GL_RGBA,GL_FLOAT,d)
        glPixelZoom(1,1)

def tonemap(image):
    scale = pow(2,exposure)
    exponent = 1.0 / gamma
    img = [pow(i * scale, exponent) for i in image]
    if USE_BUFFERS:
        buffer = array('f')
        buffer.fromlist(img)
        buffer = buffer.tostring()
        return buffer
    else:
        return img

def display():
    global displayImage, width, height
    
    glClearColor(0,0,0,0)
    glClear(GL_COLOR_BUFFER_BIT)

    drawPixels(windowWidth,windowHeight,zoom,0,0,width,height,channels,displayImage,True)
    
    glutSwapBuffers()

def passiveMotion(x,y):
    if 0 <= x < width and 0 <= y < height:
        r, g, b = image[(y*width+x)*3+0], image[(y*width+x)*3+1], image[(y*width+x)*3+2]
    else:
        r, g, b = 0, 0, 0
    glutSetWindowTitle('imageview | {f} | {w}x{h} | {x:4},{y:4} -> {r:3.2g},{g:3.2g},{b:3.2g}'.format(
        f=filename,x=x,y=y,r=r,g=g,b=b,w=width,h=height))
        
def keyboard(c,x,y):
    global exposure, gamma, image, displayImage, zoom
    if c == '1':
        exposure = 0.0
        gamma = 1
        displayImage = tonemap(image)
    elif c == '2':
        exposure = 0.0
        gamma = 2.2
        displayImage = tonemap(image)
    elif c == '[':
        exposure -= 0.5
        displayImage = tonemap(image)
    elif c == ']':
        exposure += 0.5
        displayImage = tonemap(image)
    elif c == '{':
        gamma -= 0.1
        displayImage = tonemap(image)
    elif c == '}':
        gamma += 0.1
        displayImage = tonemap(image)
    elif c == '-':
        zoom -= 1
        if zoom < 1: zoom = 1
    elif c == '=':
        zoom += 1
        if zoom < 1: zoom = 1
    elif c == '0':
        zoom = 1
    else: print("unsupported key")
    print("gamma:    " + str(gamma))
    print("exposure: " + str(exposure))
    print("zoom:     " + str(zoom))
    glutPostRedisplay()

def reshape(w,h):
    global windowWidth, windowHeight
    windowWidth = w
    windowHeight = h
    glViewport(0,0,windowWidth,windowHeight)

def run():
    glutInit([])
    glutInitWindowPosition(0,0)
    glutInitWindowSize(width,height)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB)
    glutCreateWindow('imageview')
    
    glutDisplayFunc(display)
    glutPassiveMotionFunc(passiveMotion)
    glutReshapeFunc(reshape)
    glutKeyboardFunc(keyboard)
    
    glutMainLoop()

def main():
    global image, displayImage, width, height, channels, filename
    filename = sys.argv[1]
    image, width, height, channels = imageio.load(filename)
    displayImage = tonemap(image)
    run()

main()