#!/usr/bin/python -B

import sys
import os
from array import array
from copy import copy

class Image(object):
    def __init__(self, width, height, channels, pixels = None): 
        self.width = width
        self.height = height
        self.channels = channels
        if pixels == None: self.pixels = [float(0)]*(width*height*channels)
        else: self.pixels = copy(pixels)
        
    def copy(self, image, ox, oy):
        for i in range(0,image.width):
            for j in range(0,image.height):
                for c in range(0,image.channels):
                    self.set(i+ox,j+oy,c,image.get(i,j,c))
        
    def get(self,i,j,c): return self.pixels[(j*self.width+i)*self.channels+c]
    def set(self,i,j,c,v): self.pixels[(j*self.width+i)*self.channels+c] = v

def skipws(file):
    c = file.read(1)
    while c.isspace():
        c = file.read(1)
    file.seek(-1,os.SEEK_CUR)
    
def skippastnewline(file):
    c = file.read(1)
    while c != '\n':
        c = file.read(1)

def nextword(file):
    skipws(file)
    ret = ''
    c = file.read(1)
    while not c.isspace():
        ret += c
        c = file.read(1)
    file.seek(-1,os.SEEK_CUR)
    return ret

def load(filename):
    # file open
    file = open(filename, 'rb')
    
    # magic
    magic = nextword(file)
    
    # width, height
    width  = int(nextword(file))
    height = int(nextword(file))
    
    # scale
    scale = float(nextword(file))
    if scale < 0: scale = -scale;
    
    # skip till a newline
    skippastnewline(file)
    
    # data type
    if magic == 'PF': channels, byte, ascii = 3, False, False
    elif magic == 'PE': channels, byte, ascii = 3, False, True
    elif magic == 'Pf': channels, byte, ascii = 1, False, False
    elif magic == 'Pe': channels, byte, ascii = 1, False, True
    elif magic == 'P6': channels, byte, ascii = 3, True, False
    elif magic == 'P3': channels, byte, ascii = 3, True, True
    elif magic == 'P5': channels, byte, ascii = 1, True, False
    elif magic == 'P2': channels, byte, ascii = 1, True, True
    else: raise Error()
    
    # load data
    if ascii:
        text = ' '.join(file.readlines()).rstrip(' \n').lstrip(' \n')
        pixels = [float(t) for t in text.split(' ')]
    else:
        data = array('f')
        data.fromfile(file,width*height*channels)
        pixels = data.tolist()
    if width*height*channels != len(pixels): raise Error()
    
    # scale values
    pixels = [i / scale for i in pixels]
    
    # flip y for floats
    if not byte:
        for y in range(0,height/2):
            for x in range(0,width):
                for c in range(0,channels):
                    idx1 = ((y*width)+x)*channels+c
                    idx2 = (((height-1-y)*width)+x)*channels+c
                    pixels[idx1], pixels[idx2] = pixels[idx2], pixels[idx1]
    
    # ensure all values are good
    lz = 0
    for i in range(len(pixels)):
        if(pixels[i] < 0): lz += 1; pixels[i] = 0
    if lz  >0: print("less than zero: " + str(lz))
    
    # done
    return Image(width, height, channels, pixels)
   
def savePfm(filename, image):
    # file open
    file = open(filename, 'wb')
    
    # magic
    if image.channels == 3: file.write("PF\n")
    else: file.write("Pf\n")
    
    # width, height
    file.write(str(image.width) + " " + str(image.height) + "\n")
    
    # scale
    scale = -1
    file.write(str(scale) + "\n")
    
    # flip y for floats
    pixels = copy(image.pixels)
    for y in range(0,image.height/2):
        for x in range(0,image.width):
            for c in range(0,image.channels):
                idx1 = ((y*image.width)+x)*image.channels+c
                idx2 = (((image.height-1-y)*image.width)+x)*image.channels+c
                pixels[idx1], pixels[idx2] = pixels[idx2], pixels[idx1]

    # write data
    data = array('f')
    data.fromlist(pixels)
    data.tofile(file)
    
    # done
    file.close()    
   
def comp(xtiles, images):
    if len(images) % xtiles != 0: raise Exception("bad number of images")
    xt = xtiles
    yt = len(images) / xt
    w = images[0].width
    h = images[0].height
    c = images[0].channels
    for image in images:
        if image.width != w: raise Exception("bad width")
        if image.height != h: raise Exception("bad height")
        if image.channels != c: raise Exception("bad channels")
    ret  = Image(w*xt,h*yt,c)
    for j in range(0,yt):
        for i in range(0,xt):
            ret.copy(images[j*xt+i],i*w,j*h)
    return ret

def main():
    xtiles = int(sys.argv[1])
    filenames = sys.argv[2:-1]
    filename_comp = sys.argv[-1]
    images = [load(filename) for filename in filenames]
    image_comp = comp(xtiles,images)
    savePfm(filename_comp, image_comp);

main()
