#!/usr/bin/python

import os
import sys
import json
import shutil

usage = 'usage: procscene scene.json mod.json'

def replace_list_object(objectlist, object):
    name = object['name']
    for i in range(len(objectlist)):
        if objectlist[i]['name'] == name:
            print ('replace object ' + name)
            objectlist[i] = object
    
def insert_list_object(objectlist, object):
    name = object['name']
    found = False
    for i in range(len(objectlist)):
        if objectlist[i]['name'] == name:
            found = True
            print ('found object ' + name + ', replace it')
            objectlist[i] = object
    if not found:
        print ('insert object ' + object['name'])
        objectlist.append(object)
    
def delete_list_object(objectlist, object):
    name = object['name']
    for obj in objectlist:
        if obj['name'] == name:
            print ('remove object ' + name)
            objectlist.remove(obj)
            return

def replace_object(type, object, scene):
    if type == 'reflectance':
        replace_list_object(scene['reflectances'], object)
    elif type == 'material':
        replace_list_object(scene['materials'], object)
    elif type == 'texture':
        replace_list_object(scene['textures'], object)
    elif type == 'shape':
        replace_list_object(scene['shapes'], object)
    elif type == 'surface':
        replace_list_object(scene['surfaces'], object)
    elif type == 'transform':
        replace_list_object(scene['transforms'], object)
    elif type == 'light':
        replace_list_object(scene['lights'], object)
    elif type == 'material':
        replace_list_object(scene['cameras'], object)
    else: print ('error')

def insert_object(type, object, scene):
    if type == 'reflectance':
        insert_list_object(scene['reflectances'], object)
    elif type == 'material':
        insert_list_object(scene['materials'], object)
    elif type == 'texture':
        insert_list_object(scene['textures'], object)
    elif type == 'shape':
        insert_list_object(scene['shapes'], object)
    elif type == 'surface':
        insert_list_object(scene['surfaces'], object)
    elif type == 'transform':
        insert_list_object(scene['transforms'], object)
    elif type == 'light':
        insert_list_object(scene['lights'], object)
    elif type == 'camera':
        insert_list_object(scene['cameras'], object)
    else: print ('error')

def delete_object(type, object, scene):
    if type == 'reflectance':
        delete_list_object(scene['reflectances'], object)
    elif type == 'material':
        delete_list_object(scene['materials'], object)
    elif type == 'texture':
        delete_list_object(scene['textures'], object)
    elif type == 'shape':
        delete_list_object(scene['shapes'], object)
    elif type == 'surface':
        delete_list_object(scene['surfaces'], object)
    elif type == 'transform':
        delete_list_object(scene['transforms'], object)
    elif type == 'light':
        delete_list_object(scene['lights'], object)
    elif type == 'camera':
        delete_list_object(scene['cameras'], object)
    else: print ('error')

def copy_file(src, dst):
    shutil.copyfile(src, dst)

def proc(mod, scene):
    if mod['operation'] == 'replace':
        replace_object(mod['type'], mod['sceneobject'], scene)
    elif mod['operation'] == 'insert':
        insert_object(mod['type'], mod['sceneobject'], scene)
    elif mod['operation'] == 'delete':
        delete_object(mod['type'], mod['sceneobject'], scene)
    elif mod['operation'] == 'copy' and mod['type'] == 'file':
        print ('copy from ' + mod['src'] + ' to ' + mod['dst'])
        copy_file(mod['src'], mod['dst'])
    else: print ('unknow modifier operation')

def main(argv = None):
    if len(argv) < 3:
        print (usage)
        
    scenepath = sys.argv[1]
    modpath = sys.argv[2]
    replace = None
    if len(argv) > 3:
        replace = sys.argv[3]
    
    sf = open(scenepath, 'rb')
    scene = json.load(sf)
    sf.close()
    
    mf = open(modpath, 'rb')
    mfstr = mf.read()
    if replace != None:
        mfstr = mfstr.replace('@@@', replace)
    modlist = json.loads(mfstr)
    mf.close()
    
    for mod in modlist:
        proc(mod, scene)
        
    #print scene
    scene_string = json.dumps(scene, separators=(',',':'), indent=4)
    nf = open(scenepath, 'wb')
    nf.write(scene_string)
    nf.close()

if __name__ == '__main__':
    main(sys.argv)

