#!/usr/bin/python
import os
import sys
import json
import imageio
import gc

def compute_image_average(image, width, height, channels):
    
    size = width * height;
    inv_size = 1.0 / size
    if channels == 1:
        color = 0.0
        for i in range(size):
            c = image[i] * inv_size
            color += c
        return color
    elif channels == 3:
        color = [0.0, 0.0, 0.0]
        for i in range(size):
            for c in range(channels):
                color[c] += (image[i] * 3 + c) * inv_size
        return color
    else:
        print 'incorrect channel number'
        return None

def remove_diffuse_texture(reflectance, textures):
    if reflectance.has_key('rhod'):
        new_color = reflectance['rhod']
    else:
        new_color = [1.0, 1.0, 1.0]
    if reflectance.has_key('rhodtxt_ref'):
        tex_ref = reflectance['rhodtxt_ref']
        match_textures = [t for t in textures if t['name'] == tex_ref]
        if len(match_textures) >= 1:
            texture = match_textures[0]
            tex_filename = texture['image_filename']
            (image, width, height, channels) = imageio.load(tex_filename)
            tex_color = compute_image_average(image, width, height, channels)
            for c in range(3):
                new_color[c] = new_color[c] * tex_color[c]
            del image
        reflectance.pop('rhodtxt_ref')
        reflectance['rhod'] = new_color

def remove_specular_texture(reflectance, textures):
    if reflectance.has_key('rhos'):
        new_color = reflectance['rhos']
    else:
        new_color = [1.0, 1.0, 1.0]
    if reflectance.has_key('rhostxt_ref'):
        tex_ref = reflectance['rhostxt_ref']
        match_textures = [t for t in textures if t['name'] == tex_ref]
        if len(match_textures) >= 1:
            texture = match_textures[0]
            tex_filename = texture['image_filename']
            (image, width, height, channels) = imageio.load(tex_filename)
            tex_color = compute_image_average(image, width, height, channels)
            for c in range(3):
                new_color[c] = new_color[c] * tex_color[c]
            del image
        reflectance.pop('rhostxt_ref')
        reflectance['rhos'] = new_color

def remove_roughness_texture(reflectance, textures):
    if reflectance.has_key('n'):
        new_color = reflectance['n']
    else:
        new_color = 1
    if reflectance.has_key('ntxt_ref'):
        tex_ref = reflectance['ntxt_ref']
        match_textures = [t for t in textures if t['name'] == tex_ref]
        if len(match_textures) >= 1:
            texture = match_textures[0]
            tex_filename = texture['image_filename']
            (image, width, height, channels) = imageio.load(tex_filename)
            tex_color = compute_image_average(image, width, height, channels)
            new_color = new_color * tex_color
            del image
        reflectance.pop('ntxt_ref')
        reflectance['n'] = new_color

def remove_textures(scene):    
    reflectances = scene['reflectances']
    textures = scene['textures']
    sources = scene['sources']
    emissions = scene['emissions']
    for reflectance in reflectances:
        if reflectance['type'] == 'lambertreflectance':
            remove_diffuse_texture(reflectance, textures)
        elif reflectance['type'] == 'microfacetreflectance':
            remove_diffuse_texture(reflectance, textures)
            remove_specular_texture(reflectance, textures)
            remove_roughness_texture(reflectance, textures)
        gc.collect()
    
   
    new_textures = []
    for emission in emissions:
        if emission.has_key('letxt_ref'):
            tex_ref = emission['letxt_ref']
            texture = [t for t in textures if t['name'] == tex_ref][0]
            new_textures.append(texture)
    
    for source in sources:
        if source['type'] == 'environmentsource' and source.has_key('letxt_ref'):
            tex_ref = source['letxt_ref']
            texture = [t for t in textures if t['name'] == tex_ref][0]
            new_textures.append(texture)
            
    scene['textures'] = new_textures
    
    
def main(argv = None):
    if argv == None or len(argv) <= 1:
        print ('usage: rmtex <input_scene>')
        return
        
    path = sys.argv[1]
    
    (folder, filename) = os.path.split(path)
    
    if folder == None or len(folder) <= 0:
        folder = '.'
        
    os.chdir(folder)
    print ('current directory: ' + os.getcwd())
    
    fp = open(filename, 'rb')
    scene = json.load(fp)
    fp.close()
    
    remove_textures(scene)
    
    output_filename = 'notex_' + filename
    print ('save to file: ' + output_filename)
    
    scene_string = json.dumps(scene, indent=4, sort_keys=True)
    fp = open(output_filename, "w")
    fp.write(scene_string)
    fp.close()
    
if __name__ == '__main__':
    main(sys.argv)
