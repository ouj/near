#!/usr/bin/python

import subprocess
import os
import sys
import glob

def make_video(scenename):
    cmd = ['ffmpeg', '-qscale', '1', '-r', '20', '-b', '5000000', '-y', '-i', scenename+'%d.png', scenename + '.mp4']
    print cmd
    subprocess.call(cmd)

def convert_images(scenename):
    img_path = os.path.normpath(scenename+'*.[Pp][Ff][Mm]')
    gp = glob.glob(img_path)
    for fname in gp:
        if os.path.isfile(fname):
            (base, extension) = os.path.splitext(fname)
            outputname = base + ".png"
            cmd = "convert -gamma 2.2 " + fname + " " + outputname
            print cmd
            os.system(cmd)
            
def remove_images(scenename):
    img_path = os.path.normpath(scenename+'*.[Pp][Nn][Gg]')
    gp = glob.glob(img_path)
    for fname in gp:
        if os.path.isfile(fname):
            os.remove(fname)
            
def main(argv = None):
    if argv == None or len(argv) <= 1:
        print ('usage: makevideo.py imagename')
        return
        
    scenename = os.path.splitext(argv[1])[0]
    print ('convert pfm images to png')
    convert_images(scenename)
    print ('make video')
    make_video(scenename)
    print ('delete pngs')
    remove_images(scenename)

if __name__ == '__main__':
    main(sys.argv)
