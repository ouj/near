#! /usr/bin/env python -B

import json
import sys

def filter(scene):
    reflectances = scene["reflectances"]
    for refl in reflectances:
        if refl["type"] == "microfacetreflectance":
            refl["type"] = "lambertreflectance"
            del refl["rhos"]
            del refl["specular"]
            del refl["diffuse"]
            del refl["n"]
            if "rhosTxt" in refl: del refl["rhosTxt"]
            if "nTxt" in refl: del refl["nTxt"]
            print("changed reflectance: " + refl["name"])
    return scene

def main():
    if len(sys.argv) != 3: print ("usage: tolambert filenamein.json filenameout.json"); return
    scene = json.load(open(sys.argv[1], "rt"))
    scene = filter(scene)
    json.dump(scene, open(sys.argv[2], "wt"), indent=4)
    
if __name__ == '__main__':
    main()
