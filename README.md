Overview
=======
near is a research-oriented rendering system written in C++, and highly parallelized using Intel TBB. It includes implementation of many famous rendering algorithms, including 

###unbiased methods
* _path tracing_, 
* _bidrectional path tracing_
* _metropolis light transport_
* _energy redistributed path tracing_
* ...

### biased methods
* _photon mapping_
* _stochastic progressive photon mapping_
* _instant radiosity_
* _lightcut_
* …

**GPU raytracing is coming, ** Check out [my blog](http://jiaweiou.blogspot.com/) for latest development updates.

Dependency
=======
near depends on four submodule repositories:

* __common__ a common library that defines basic types (vector, frame, array, transform), and basic math functions that frequently used by the rendering system.
* __scene__ scene graph that used by the rendering system
* __accelerator__ acceleration structures such as bounding box hierarchy.
* __trace__ based ray tracing and light transport integration functions.

download source
===============

    git clone "https://ouj@bitbucket.org/ouj/near.git"
    cd near
    git submodule init
    git submodule update

build
===============
__Xcode (Mac OSX)__

    cd near
    ./cmake-gen.py xcode

__Code::Blocks (Linux)__

    cd near
    ./cmake-gen.py cbs
    
__Makefile (Both Linux and Mac OSX)__

    cd near
    ./cmake-gen.py make
    
scene datasets and 
======================================
sample scenes are located under `testscenes`folder. scenes can also be exported from blender using the [blender-exporter](https://bitbucket.org/ouj/blend-exporter) for near.
Or from PBRT using the [pbrt-exporter](https://bitbucket.org/ouj/pbrt-exporter).

configuration files
===================
sample configuration files are located under `configs` folder. You can create your own base on the sample ones.

    
execution
===============
__interaction trace__ (with display window)

    itrace scene.json scene.pfm [renderopts.json] [accelratoropts.json]

__static trace__ (without display window)

    strace scene.json scene.pfm [renderopts.json] [accelratoropts.json]