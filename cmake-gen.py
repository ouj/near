#!/usr/bin/env python

import os
import sys
import platform
import subprocess
import glob
import shutil

def cmake_generate(opt, build_mode='release'):
    gui = False
    cmake_executable = 'cmake' if not gui else 'ccmake'
    build_mode_float = get_build_mode_flat(build_mode)
    if opt == 'xcode':  # xcode project
        if not os.path.exists('build/obj/xcode'):
            os.makedirs('build/obj/xcode')
        os.chdir('build/obj/xcode')
        subprocess.call((cmake_executable, build_mode_float, '-Wdev', '-G', 'Xcode', '../../../src'))
        projects = glob.glob('*.xcodeproj')
        assert(len(projects) > 0)
        for p in projects:
            subprocess.call(('open', p))
    elif opt == 'make':  # unix/linux makefile
        if not os.path.exists('build/obj/make'):
            os.makedirs('build/obj/make')
        os.chdir('build/obj/make')
        subprocess.call((cmake_executable, build_mode_float, '-Wdev', '-G', 'Unix Makefiles', '../../../src'))
        assert(os.path.exists('Makefile'))
        subprocess.call(('make', 'all'))
    elif opt == 'cbs':
        if not os.path.exists('build/obj/cbs'):
            os.makedirs('build/obj/cbs')
        os.chdir('build/obj/cbs')
        subprocess.call((cmake_executable, build_mode_float, '-Wdev', '-G', 'CodeBlocks - Unix Makefiles', '../../../src'))
        projects = glob.glob('*.cbp')
        assert(len(projects) > 0)
        for p in projects:
            subprocess.call(('gnome-open', p), stdout=open(os.devnull, 'w'), stderr=subprocess.STDOUT)
    elif opt == 'kde':
        if not os.path.exists('build/obj/kde'):
            os.makedirs('build/obj/kde')
        os.chdir('build/obj/kde')
        subprocess.call((cmake_executable, build_mode_float, '-Wdev', '-G', 'KDevelop3', '../../../src'))
    elif opt == 'subl':
        if not os.path.exists('build/obj/subl'):
            os.makedirs('build/obj/subl')
        os.chdir('build/obj/subl')
        subprocess.call((cmake_executable, build_mode_float, '-Wdev', '-G', 'Sublime Text 2 - Unix Makefiles', '../../../src'))
    elif opt == 'clean':  # clean cmake files
        if os.path.exists('build/obj'):
            shutil.rmtree('build/obj')
        if os.path.exists('bin'):
            shutil.rmtree('bin')
        assert(not os.path.exists('build/obj'))
        assert(not os.path.exists('bin'))

def get_build_mode_flat(build_mode):
    if build_mode == 'release':
        return '-DCMAKE_BUILD_TYPE=Release'
    elif build_mode == 'debug':
        return '-DCMAKE_BUILD_TYPE=Debug'
    elif build_mode == 'reldbg':
        return '-DCMAKE_BUILD_TYPE=RelWithDebInfo'

def print_usage():
    desc = {
        'xcode' : 'Generate Max OSX Xcode project',
        'make'  : 'Generate Unix/Linux makefile',
        'cbs'   : 'Generate Linux Code::Blocks project',
        'kde'   : 'Generate Linux KDeveloper project',
        'subl'  : 'Generate Sublime Text 2 project',
        'clean' : 'Clean up cmake objs'
    }
    buildmodedesc = {
        'release' : 'Release build (Default)',
        'debug'   : 'Debug build',
        'reldbg'  : 'Release build with debug info'
    }
    print 'Usage: cmake_gen.py [BUILD TYPE] (BUILD MODE)'
    print '----------'
    print 'Build types for %s platform:' % platform.system()
    for opt in cmake_generators():
        print '%s - %s' % (opt.rjust(10), desc[opt])

    print 'Build Mode:'
    for opt in build_modes():
        print '%s - %s' % (opt.rjust(10), buildmodedesc[opt])

def cmake_generators():
    if platform.system() == 'Darwin':
        return ['xcode', 'make', 'clean']
    elif platform.system() == 'Linux':
        return ['cbs', 'kde', 'make', 'clean']

def build_modes():
    return ['release', 'debug', 'reldgb']

def available_options():
    return cmake_generators() + build_modes()

def main(argv):
    if len(argv) < 2:
        print_usage()
        return
    opt = argv[1]
    assert(opt in available_options())

    build_mode = 'release'
    if len(argv) == 3:
        if argv[2] in build_modes():
            build_mode = argv[2]
        else:
            print_usage()
            return
    cmake_generate(opt, build_mode)


if __name__ == '__main__':
    main(sys.argv)



