This folder contains the cmake build related files and directories.

* __modules__ - contains the cmake module for find_package() methods.
* __obj__ - contains the cmake generated files and directories. It is generate d by the _cmake-gen.py_ script, and it can be removed and regenerated. 